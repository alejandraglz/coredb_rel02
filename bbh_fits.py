import numpy as np

def bbh_UIBfits_setup(m1, m2, chi1, chi2):
    """
    Common setup function for UIB final-state and luminosity fit functions
    """
    # Vectorize the function if arrays are provided as input
    m1   = np.vectorize(float)(np.array(m1))
    m2   = np.vectorize(float)(np.array(m2))
    chi1 = np.vectorize(float)(np.array(chi1))
    chi2 = np.vectorize(float)(np.array(chi2))
    if np.any(m1<0):
      raise ValueError("m1 must not be negative")
    if np.any(m2<0):
      raise ValueError("m2 must not be negative")
    if np.any(abs(chi1)>1):
      raise ValueError("chi1 has to be in [-1, 1]")
    if np.any(abs(chi2)>1):
      raise ValueError("chi2 has to be in [-1, 1]")
    # binary masses
    m    = m1+m2
    if np.any(m<=0):
      raise ValueError("m1+m2 must be positive")
    msq  = m*m
    m1sq = m1*m1
    m2sq = m2*m2
    # symmetric mass ratio
    eta  = m1*m2/msq
    if np.any(eta>0.25):
      print( "Truncating eta from above to 0.25. This should only be necessary in some rounding corner cases, but better check your m1 and m2 inputs...")
      eta = np.minimum(eta,0.25)
    if np.any(eta<0.0):
      print("Truncating negative eta to 0.0. This should only be necessary in some rounding corner cases, but better check your m1 and m2 inputs...") 
      eta = np.maximum(eta,0.0)
    eta2 = eta*eta
    eta3 = eta2*eta
    eta4 = eta2*eta2
    # spin variables (in m = 1 units)
    S1    = chi1*m1sq/msq # spin angular momentum 1
    S2    = chi2*m2sq/msq # spin angular momentum 2
    Stot  = S1+S2         # total spin
    Shat  = (chi1*m1sq+chi2*m2sq)/(m1sq+m2sq) # effective spin, = msq*Stot/(m1sq+m2sq)
    Shat2 = Shat*Shat
    Shat3 = Shat2*Shat
    Shat4 = Shat2*Shat2
    # spin difference, assuming m1>m2
    chidiff  = chi1 - chi2
    if np.any(m2>m1): # fit assumes m1>m2
      chidiff = np.sign(m1-m2)*chidiff
    chidiff2 = chidiff*chidiff
    # typical squareroots and functions of eta
    sqrt2 = 2.**0.5
    sqrt3 = 3.**0.5
    sqrt1m4eta = (1. - 4.*eta)**0.5
    return m, eta, eta2, eta3, eta4, Stot, Shat, Shat2, Shat3, Shat4, chidiff, chidiff2, sqrt2, sqrt3, sqrt1m4eta

"""
Peak luminosity fitting formula calibrated to numerical relativity and
perturbative results from https://arxiv.org/abs/1612.09566
(c) David Keitel, Xisco Jimenez Forteza, Sascha Husa, Lionel London, Alessandro Nagar, Sebastiano Bernuzzi, Enno Harms, Mark Hannam, Sebastian Khan, Michael Puerrer, Vivek Chaurasia, Geraint Pratten

This python implementation is equivalent to code contributed by the authors to the nrutils.py package of LALInference,
under GNU General Public License as published by the Free Software Foundation,
original package authors Archisman Ghosh, Nathan K. Johnson-McDaniel, P. Ajith, 2015-04-09
Additional thanks for code comments to Nathan Johnson-McDaniel, Ofek Birnholtz and Aaron Zimmerman.
"""


def bbh_UIBfits_setup_2(m1, m2, chi1, chi2):
    """
    Common setup function for UIB final-state and luminosity fit functions
    """
    # Vectorize the function if arrays are provided as input
    m1   = np.vectorize(float)(np.array(m1))
    m2   = np.vectorize(float)(np.array(m2))
    chi1 = np.vectorize(float)(np.array(chi1))
    chi2 = np.vectorize(float)(np.array(chi2))
    if np.any(m1<0):
      raise ValueError("m1 must not be negative")
    if np.any(m2<0):
      raise ValueError("m2 must not be negative")
    if np.any(abs(chi1)>1):
      raise ValueError("chi1 has to be in [-1, 1]")
    if np.any(abs(chi2)>1):
      raise ValueError("chi2 has to be in [-1, 1]")
    # binary masses
    m    = m1+m2
    if np.any(m<=0):
      raise ValueError("m1+m2 must be positive")
    msq  = m*m
    m1sq = m1*m1
    m2sq = m2*m2
    # symmetric mass ratio
    eta  = m1*m2/msq
    if np.any(eta>0.25):
      print("Truncating eta from above to 0.25. This should only be necessary in some rounding corner cases, but better check your m1 and m2 inputs...") 
      eta = np.minimum(eta,0.25)
    if np.any(eta<0.0):
      print("Truncating negative eta to 0.0. This should only be necessary in some rounding corner cases, but better check your m1 and m2 inputs...") 
      eta = np.maximum(eta,0.0)
    eta2 = eta*eta
    eta3 = eta2*eta
    eta4 = eta2*eta2
    eta5 = eta3*eta2
    # spin variables (in m = 1 units)
    S1    = chi1*m1sq/msq # spin angular momentum 1
    S2    = chi2*m2sq/msq # spin angular momentum 2
    Stot  = S1+S2         # total spin
    Shat  = msq*Stot/(m1sq+m2sq) # effective spin
    Shat2 = Shat*Shat
    Shat3 = Shat2*Shat
    Shat4 = Shat2*Shat2
    # spin difference, assuming m1>m2
    chidiff  = chi1 - chi2
    if np.any(m2>m1): # fit assumes m1>m2
      chidiff = np.sign(m1-m2)*chidiff
    chidiff2 = chidiff*chidiff
    # typical squareroots and functions of eta
    sqrt2 = 2.**0.5
    sqrt3 = 3.**0.5
    sqrt1m4eta = (1. - 4.*eta)**0.5
    return m, eta, eta2, eta3, eta4, eta5, Stot, Shat, Shat2, Shat3, Shat4, chidiff, chidiff2, sqrt2, sqrt3, sqrt1m4eta


def LpeakUIB2016(m1, m2, chi1, chi2):
    """
    Peak luminosity fit function by Keitel, Jimenez-Forteza, Husa, London et al (2016)
    m1, m2: component masses
    chi1, chi2: dimensionless spins of two BHs
    Note: Here it is assumed that m1>m2.
    """

    m, eta, eta2, eta3, eta4, eta5, Stot, Shat, Shat2, Shat3, Shat4, chidiff, chidiff2, sqrt2, sqrt3, sqrt1m4eta = bbh_UIBfits_setup_2(m1, m2, chi1, chi2)

    # fit coefficients from Tables
    a0 = 0.8742169580717333
    a1 = -2.111792574893241
    a2 = 35.214103272783646
    a3 = -244.94930678226913
    a4 = 877.1061892200927
    a5 = -1172.549896493467
    b1 = 0.9800204548606681
    b2 = -0.1779843936224084
    b4 = 1.7859209418791981
    f71 = 0.
    d10 = 3.789116271213293
    d20 = 0.40214125006660567
    d30 = 4.273116678713487
    f10 = 1.6281049269810424
    f11 = -3.6322940180721037
    f20 = 31.710537408279116
    f21 = -273.84758785648336
    f30 = -0.23470852321351202
    f31 = 6.961626779884965
    f40 = 0.21139341988062182
    f41 = 1.5255885529750841
    f60 = 3.0901740789623453
    f61 = -16.66465705511997
    f70 = 0.8362061463375388
    # calculate
    Lpeak = a0 + a1*eta + a2*eta2 + a3*eta3 + a4*eta4 + a5*eta5 + (0.456*Shat*(f10 + f11*eta + (16. - 16.*f10 - 4.*f11)*eta2) - 0.019*Shat2*(f20 + f21*eta + (16. - 16.*f20 - 4.*f21)*eta2) + 1.*Shat3*(f30 + f31*eta + (-16.*f30 - 4.*f31)*eta2) + 1.*Shat4*(f40 + f41*eta + (-16.*f40 - 4.*f41)*eta2))/(1. - 0.586*Shat*(f60 + f61*eta + (16. - 16.*f60 - 4.*f61)*eta2) + 1.*Shat2*(f70 + f71*eta + (-16.*f70 - 4.*f71)*eta2)) + d10*sqrt1m4eta*eta3*chidiff + d30*Shat*sqrt1m4eta*eta3*chidiff + d20*eta3*chidiff2
    L0 = 0.01637919720310354
    # Convert to actual luminosity
    Lpeak = Lpeak*eta2*L0
    # Convert to 10^56 ergs/s units
    # We first define the "Planck luminosity" of c^5/G in 10^56 ergs/s units. Note: 10^56 ergs/s = 10^49 J/s
    LAL_LUMPL_SI = 3.628504984913064522721519179529402840e52
    LumPl_ergs_per_sec = LAL_LUMPL_SI*1e-49
    ##return LumPl_ergs_per_sec*Lpeak
    return Lpeak, LumPl_ergs_per_sec*Lpeak