from cmath import nan
import matplotlib
import matplotlib.pyplot as plt
import numpy as np 
import json
import h5py 
from matplotlib.ticker import (MultipleLocator, AutoMinorLocator)
import csv
import glob

from watpy.coredb.coredb import *
from watpy.wave.wave import *
from watpy.utils.coreh5 import *
from watpy.utils.ioutils import *

import sys
import os
sys.path.append('../')
from mismatch import *
from handy_functions import *

P = pd.read_csv("../data/database.csv")
sim_path    = '/data/numrel/DATABASE/CoRe_DB_clone/'
rel2_path   = '/data/numrel/DATABASE/Release02/'
#cdb = CoRe_db(sim_path)
#idb = cdb.idb
#rel2_path = '/home/agonzalez/Documents/git/watpy/scripts'


#to_correct = ['BAM:0132', 'BAM:0133', 'BAM:0134', 'BAM:0135', 'BAM:0138', 'BAM:0139', 'BAM:0140', 'BAM:0141', 'THC:0040']
to_correct = ['BAM_0097_EFL','BAM_0100_EFL']

for entry in to_correct:
    print("Working with: ",entry)
    flag = False
    if entry not in to_correct:#in idb.dbkeys:  ## CURRENT RELEASE
        #sim = cdb.sim[entry]
        continue

    else:                    ## NEW RELEASE
        this_path = os.path.join(rel2_path,entry.replace(':','_'))
        metamain = this_path + '/metadata_main.txt'
        sim = CoRe_sim(this_path)

        for res in list(sim.run.keys()):
            print('In :', res)
            simrun = sim.run[res]
            meta_dict = simrun.md.data
            datah5 = simrun.data
            dseth5 = datah5.read_dset()
            for group in dseth5.keys():
                if group == 'energy':
                    continue
                else:
                    for datei in dseth5[group]:
                        if group=='rh_22':
                            dset = dseth5[group][datei]
                            t         = dset[:,0]
                            Re        = dset[:,1]
                            Im        = dset[:,2]
                            ampl = dset[:,4]
                            psi       = Re + 1j*Im
                            amp       = np.abs(psi)
                            amp_mrg   = np.max(amp)
                            if amp_mrg>100:
                                print("bad strain")
                            elif np.max(ampl)>100:
                                print('bad amplitude: ',datei)
                                #plt.plot(t,Re)
                                #plt.plot(t,ampl)
                                #plt.show()
                            elif amp_mrg>100 and np.max(ampl)>100:
                                print('all bad!!')
                            else:
                                print('alles gut!')
                                plt.plot(t,Re)
                                plt.plot(t,ampl)
                                plt.show()
                        else:
                            continue
'''
#to_correct = ['THC:0040']#['BAM:0132', 'BAM:0133', 'BAM:0134', 'BAM:0135', 'BAM:0138', 'BAM:0139', 'BAM:0140', 'BAM:0141', 'THC:0040']
#rel2_path = '/home/agonzalez/Documents/git/watpy/scripts'

for entry in to_correct:
    print('Now with: ',entry)
    this_path = os.path.join(rel2_path,entry.replace(':','_'))
    sim = CoRe_sim(this_path)
    
    for res in list(sim.run.keys()):
        simrun = sim.run[res]
        datah5 = simrun.data
        datah5.write_strain_to_txt_old()
        datah5.write_EJ_to_txt()
        run_path = this_path+'/'+res
        filename = [x for x in os.listdir(run_path) if x.startswith('Rh_l2_m2_r')][0]
        print(this_path+'/'+res+'/'+filename)
        u, re, im, mom, amp, phi, t = np.loadtxt(this_path+'/'+res+'/'+filename, usecols=(0,1,2,3,4,5,6),unpack=True)
        
        if np.max(np.abs(re + 1j*im))>100:
            print('Change: ', res)
            #plt.plot(u,re)
            #plt.plot(u,amp)
            #plt.show()

            # Unpack h5
            #fileh5 = h5py.File(run_path+'/data.h5')
            lm_list = []
            dh5 = datah5.read_dset()
            for group in dh5.keys():
                if group.startswith('rh_'):
                    l, m = datah5.lm_from_group(group)
                    lm_list.append((l,m))
                else:
                    continue
            ## here print with write to strain using lm_list (dont forget to modify the functions in watpy)
            for l,m in lm_list:
                datah5.write_strain_to_txt([(l,m)])
                datah5.write_psi4_to_txt([(l,m)])

            dfiles = {}
            dfiles['energy'] = [os.path.split(x)[1] for x in glob.glob('{}/EJ_r*.txt'.format(run_path))]
            for (l,m) in lm_list:
                dfiles['rh_{}{}'.format(l,m)] = [os.path.split(x)[1] for x in glob.glob('{}/Rh_l{}_m{}_r*.txt'.format(run_path,l,m))] 
                dfiles['rpsi4_{}{}'.format(l,m)] = [os.path.split(x)[1] for x in glob.glob('{}/Rpsi4_l{}_m{}_r*.txt'.format(run_path,l,m))] 
            ch5 = CoRe_h5(run_path) 
            ch5.create_dset(dfiles)
            ch5.dump()

        ## below only for when only the amplitude is bad

        elif np.max(amp)>90:
            print('bad amp here: ',res)
            #plt.plot(u,re)
            #plt.plot(u,amp)
            #plt.show()

            lm_list = []
            dh5 = datah5.read_dset()
            for group in dh5.keys():
                if group.startswith('rh_'):
                    l, m = datah5.lm_from_group(group)
                    lm_list.append((l,m))
                else:
                    continue
            print(lm_list)
            ## here print with write to strain using lm_list
            for l,m in lm_list:
                datah5.write_strain_to_txt([(l,m)])
                datah5.write_psi4_to_txt([(l,m)])

            dfiles = {}
            dfiles['energy'] = [os.path.split(x)[1] for x in glob.glob('{}/EJ_r*.txt'.format(run_path))]
            for (l,m) in lm_list:
                dfiles['rh_{}{}'.format(l,m)] = [os.path.split(x)[1] for x in glob.glob('{}/Rh_l{}_m{}_r*.txt'.format(run_path,l,m))] 
                dfiles['rpsi4_{}{}'.format(l,m)] = [os.path.split(x)[1] for x in glob.glob('{}/Rpsi4_l{}_m{}_r*.txt'.format(run_path,l,m))] 
            ch5 = CoRe_h5(run_path) 
            ch5.create_dset(dfiles)
            ch5.dump()
'''
