from watpy.coredb.metadata import *
from watpy.utils.ioutils import *
from watpy.coredb.coredb import *
import os 
import numpy as np
from pathlib import Path


sim_folder = "/data/numrel/DATABASE/Release02/Sim/"
sim_folders = os.listdir(sim_folder)

k = 0
ref= "Ujevic:2022qle"
for sim_dir in sim_folders:
    if sim_dir.startswith("BAM_PRECESSION") or sim_dir.startswith("BAM_SLY_1.350_1.350_0.00_0.00_0.038_") or sim_dir.startswith("BAM_H4"):
    #if not sim_dir=="BAM_SLY_1.350_1.350_0.00_0.00_0.038_vac":   
        continue
    else:
        metamain_dir = sim_folder + sim_dir
        path = Path(metamain_dir)
        owner = path.owner()

        if not owner=="georgios.doulis":
            print(sim_dir," by ",owner)
            run_folders = os.listdir(sim_folder+sim_dir)
            
            # Run folders loop
            for run_folder in run_folders:
                if os.path.isdir(sim_folder + sim_dir + '/'  + run_folder):
                    if sim_dir.startswith("BAM_"):
                        cdb_folder = sim_folder + sim_dir + "/" + run_folder + "/CoReDB"
                    else:
                        cdb_folder = sim_folder + sim_dir + "/" + run_folder
                    
                    meta_filename = cdb_folder + '/metadata.txt'

                    dict = read_txt_into_dict(meta_filename)
                    print(dict["reference_bibkeys"])
                    if dict["reference_bibkeys"]==ref:
                        k = k+1
                    m1 = float(dict["id_mass_starA"])
                    m2 = float(dict["id_mass_starB"])
                    mchirp = ((m1*m2)**(3/5)) / ((m1+m2)**(1/5))
                    qq = m1/m2
                    for key, value in dict.items():
                        if key=="reference_bibkeys" and len(value)==0:
                            print("Missing bibkey!")
                            #run_md = CoRe_md(path=cdb_folder,metadata=meta_filename)
                            #run_md.add(key='reference_bibkeys',val='Ujevic:2022qle')
                            #run_md.write(path=cdb_folder)
                        #if key=="id_mass_ratio" and str(mchirp).startswith("1.18"):
                        #    print("{:.6e}".format(qq))
                        #    run_md = CoRe_md(path=cdb_folder,metadata=meta_filename)
                        #    run_md.add(key='reference_bibkeys',val='Nedora:2020hxc')
                        #    run_md.add(key=key,val="{:.6e}".format(qq))
                        #    run_md.write(path=cdb_folder)
                        else:
                            continue
        else:
            continue
print("From: ",ref,", # of sims: ",k)
                
