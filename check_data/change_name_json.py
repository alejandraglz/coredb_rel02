from watpy.coredb.metadata import *
from watpy.utils.ioutils import *
from watpy.coredb.coredb import *
import os 
import shutil
import numpy as np
from pathlib import Path
import natsort

def str_format(id):
    digits = len(str(id))
    if digits==2:
        banana = '00'+str(id)
    elif digits==3:
        banana = '0'+str(id)
    return banana

json_file = "../breschi_fits/results/fits_results.json"
jdict_list = read_json_into_dict(json_file)

names = []
for dict in jdict_list:
    if dict is not None:
        names.append(dict['database_key'])
#thc_names= [i for i in name if (i.split(':')[0]=='THC' and float(i.split(':')[1])>36)] 

runs=[]
k = 0
for run_name in names:
    code = run_name.split(':')[0]
    num = run_name.split(':')[1]
    run = run_name.split(':')[2]
    if code=='THC' and num in ['0052','0056','0069']:
        names.remove(run_name)
#print(names) #names you want in your json

new_dictlist = []
for dict in jdict_list:
    if (dict is not None) and (dict['database_key'] in names):
        new_dictlist.append(dict)
with open('fits_results.json', 'w') as f:         
        json.dump(new_dictlist, f, indent = 6)



'''
base = '/data/numrel/DATABASE/Release02/'
sim_folders = natsort.natsorted(os.listdir(base))
sim_folders.remove('Sim')
sim_folders.remove('README')
sim_folders= [i for i in sim_folders if i.split('_')[0]== 'THC'] 
print(sim_folders)
i = 37
for j,folder in enumerate(sim_folders):
    code = folder.split('_')[0]
    num = float(folder.split('_')[1])
    if num==i:
        print(folder)
        i = i+1
    else:
        current_name = 'THC_'+str_format(i) #name we should have
        path2sim = base + current_name
        if os.path.exists(path2sim):
            print("oh, it does exist?? ",path2sim)
        else:
            old_name = sim_folders[j]
            if os.path.exists(base+old_name):
                #runcmd(['pwd'],base, None,True)
                #print("Change ",old_name," to ",current_name)
                sim_folders[j] = current_name # change folder name, DO AT THE END
                
                ### change metadata_main
                meta_filename = base + old_name  + '/metadata_main.txt'
                dict = read_txt_into_dict(meta_filename)
                sim_md = CoRe_md(path=base+old_name,metadata=dict)
                new_key = current_name.replace('_',':')
                #print(sim_md.data['database_key']," change to ",new_key)
                sim_md.add(key='database_key',val=new_key)
                sim_md.write(base+old_name,'metadata_main.txt',TXT_MAIN)
                
                ### change metadata
                run_folders = os.listdir(base+old_name)
                num_runs = 1
                for run_folder in run_folders:
                    if os.path.isdir(base + old_name + '/'  + run_folder):
                        path2run = base+old_name + '/'+ run_folder
                        metapath = path2run + '/metadata.txt'
                        dictrun = read_txt_into_dict(metapath)
                        run_md = CoRe_md(path=path2run,metadata=dictrun)
                        #print(run_md.data['database_key']," change to ",new_key+':'+run_folder)
                        run_md.add(key='database_key',val=new_key+':'+run_folder)
                        run_md.write(path2run)
                        num_runs = num_runs + 1

                ###  NOW CHANGE FOLDER NAME:
                runcmd(['mv',old_name,current_name],base,None,True)
            else:
                break
            i = i+1
        
print(sim_folders)
        if sim_dir.startswith("BAM_"):
            new_folder = "BAM_"+str_format(bam_id)
            bam_id = bam_id + 1
        else:
            new_folder = "THC_"+str_format(thc_id)
            thc_id = thc_id + 1

        path2sim = base+new_folder
        if not os.path.exists(path2sim):
            os.makedirs(path2sim)

        metamain_dir = sim_folder + sim_dir
        run_folders = os.listdir(sim_folder+sim_dir)

        # Run folders loop
        num_runs = 1
        for run_folder in run_folders:
            if os.path.isdir(sim_folder + sim_dir + '/'  + run_folder):
                #make the run folder
                run_name = 'R0'+str(num_runs)
                path2run = path2sim + '/'+ run_name
                if not os.path.exists(path2run):
                    os.makedirs(path2run)
                
                dbkey_run = new_folder.replace('_',':') + ":" + run_name

                ##copy the files
                
                #correct simname in meta
                if sim_dir.startswith("BAM_"):
                    try:
                        sim_name = run_folder.rsplit('BAM_',1)[1]
                    except:
                        sim_name = run_folder
                    cdb_folder = sim_folder + sim_dir + "/" + run_folder + "/CoReDB"
                else:
                    sim_name = run_folder.rsplit('THC_',1)[1]
                    cdb_folder = sim_folder + sim_dir + "/" + run_folder

                #files from Sim/ folder
                meta_filename = cdb_folder + '/metadata.txt'
                h5_filename = cdb_folder + '/data.h5'
                
                dict = read_txt_into_dict(meta_filename)
                
                run_md = CoRe_md(path=cdb_folder,metadata=dict)
                run_md.add(key='database_key',val=dbkey_run)
                run_md.add(key='binary_type',val='BNS')
                run_md.add(key='simulation_name',val=sim_name)
                run_md.write(path=path2run)
                
                path2meta = path2run + '/metadata.txt'
                path2h5 = path2run + '/data.h5'

                #shutil.copyfile(meta_filename, path2meta)
                shutil.copyfile(h5_filename, path2h5)
                num_runs = num_runs + 1

        # added missing metadata_main
        dbkey = new_folder.replace('_',':')

        if sim_dir.startswith("BAM_"):
            sim_name_main = sim_dir.rsplit('BAM_',1)[1]
        else:
            sim_name_main = sim_dir.rsplit('THC_',1)[1]

        runs_in_sim = ""
        for i in range(1,num_runs):
            run_code ='R0'+str(i)
            if i == 1:
                runs_in_sim = run_code
            else:
                runs_in_sim = runs_in_sim + ", " + run_code
        
        sim_md = CoRe_md(path=cdb_folder,metadata=dict)
        sim_md.add(key='database_key',val=dbkey)
        sim_md.add(key='simulation_name',val=sim_name_main)
        sim_md.add(key='available_runs',val=runs_in_sim)
        sim_md.add(key='binary_type',val='BNS')
        sim_md.write(path=path2sim,fname='metadata_main.txt',templ=TXT_MAIN)
'''
