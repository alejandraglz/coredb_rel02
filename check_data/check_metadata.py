from watpy.coredb.metadata import *
from watpy.utils.ioutils import *
from watpy.coredb.coredb import *
import os 
import numpy as np
from pathlib import Path

new_release_folder = "new_release"
if not os.path.exists(new_release_folder):
    os.makedirs(new_release_folder)

sim_folder = "/data/numrel/DATABASE/Release02/Sim/"
sim_folders = os.listdir(sim_folder)

for sim_dir in sim_folders:
    #if not sim_dir.startswith("THC_") or sim_dir.startswith("BAM_PRECESSION"):
    if not sim_dir=="BAM_SLY_1.350_1.350_0.00_0.00_0.038_vac":   
        continue
    else:
        metamain_dir = sim_folder + sim_dir
        path = Path(metamain_dir)
        owner = path.owner()
        #print(sim_dir," by ",owner)
        
        if owner =="georgios.doulis":
            run_folders = os.listdir(sim_folder+sim_dir)

            # Run folders loop
            num_runs = 0
            for run_folder in run_folders:
                if os.path.isdir(sim_folder + sim_dir + '/'  + run_folder):
                    cdb_folder = sim_folder + sim_dir + "/" + run_folder + "/CoReDB"
                    meta_filename = cdb_folder + '/metadata.txt'
                    #meta_corr = cdb_folder + '/metadata_corrected.txt'

                    dict = read_txt_into_dict(meta_filename)
                    print(sim_dir," by ",owner)
                    for key, value in dict.items():
                        if key.startswith("id_") and len(value)==0:
                            print("Problem with empty key: ",key)
                        
                
                #num_runs = num_runs + 1
                
                # remove () in metadata, can't write on folder, do locally!
                #dest_meta = open(meta_corr,'w')
                #source_meta = open(meta_filename,'r')
                #for line in source_meta:
                #    line1 = line.replace("(","")
                #    line2 = line1.replace(")","")
                #    dest_meta.write(line2)
                #dest_meta.close()
                #source_meta.close()
                #print(meta_corr)
           #break
        # end of runs loop
    #break
    # added missing metadata_main
    #runs_in_sim = ""
    #for i in range(1,num_runs+1):
    #    run_code ='R0'+str(i)
    #    if i == 1:
    #        runs_in_sim = run_code
    #    else:
    #        runs_in_sim = runs_in_sim + ", " + run_code
    #print(runs_in_sim)
    #    
    #sim_in_new = new_release_folder + '/' + sim_dir
    #if not os.path.exists(sim_in_new):
    #    os.makedirs(sim_in_new)
    #    temp_dir = "./"+sim_in_new
    #
    #sim_md = CoRe_md(path='.',metadata=dict)
    #sim_md.add(key='simulation_name',val=sim_name_main)
    #sim_md.add(key='available_runs',val=runs_in_sim)
    #sim_md.write(path=temp_dir,fname='metadata_main.txt',templ=TXT_MAIN)

    
                
                # Add key to metadata
                #sim_name = run_folder.rsplit('BAM_',1)[1]
                #dict = read_txt_into_dict(meta_filename)
                #if dict['simulation_name']:
                #    continue
                #else:
                #    run_md = CoRe_md(path='.',metadata=dict)
                #    run_md.add(key='simulation_name',val=sim_name)
                #    run_md.write(path=cdb_folder)'''
