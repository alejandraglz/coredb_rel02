import os
import numpy as np
import h5py
from watpy.utils.viz import wplot
from watpy.coredb.coredb import *
from watpy.coredb.metadata import *
from watpy.wave.wave import *
from watpy.utils.ioutils import *
from watpy.utils.coreh5 import *
import json
import re

import sys
sys.path.append('../')

def write_three(chain):
    result = None
    s = [r'(\d+), (\d+), (\d+)',r'(\d+),(\d+),(\d+)',r'(\w+), (\w+), (\w+)']
    for sm in s:
        nums = re.match(sm,chain)
        if nums is not None:
            num1 = "{:.2f}".format(float(nums.group(1)))
            num2 = "{:.2f}".format(float(nums.group(2)))
            num3 = "{:.2f}".format(float(nums.group(3)))
            result = str(num1) + ', ' + str(num2) + ', ' + str(num3)
    if (chain=='0.000000e+00, 0.000000e+00, 0.000000e+00') or result=='0.00, 0.00, 0.00':
        result = '0, 0, 0'
    elif result==None:
        result = chain

    return result

db_path = '/data/numrel/DATABASE/CoRe_DB_clone'
#db_path = '../../git/watpy/msc/CoRe_DB_clone'
cdb = CoRe_db(db_path)
#cdb.sync()
idb = cdb.idb

# read metadata_main in a list
mdlist = []
print(cdb.idb.dbkeys)

for key in idb.dbkeys:
    print('Working with: ',key)
    sim_md = cdb.sim[key].md.data
    resos = []
    for res in cdb.sim[key].run.keys():
        run_md = cdb.sim[key].run[res].md.data
        resos.append(float(run_md['grid_spacing_min']))
    cdb.sim[key].md.data['id_mass_starA'] = run_md['id_mass_starA'][0]+run_md['id_mass_starA'][1]+run_md['id_mass_starA'][2]#"{:.2f}".format(float(run_md['id_mass_starA']))
    cdb.sim[key].md.data['id_mass_starB'] = run_md['id_mass_starB'][0]+run_md['id_mass_starA'][1]+run_md['id_mass_starA'][2]#"{:.2f}".format(float(run_md['id_mass_starB']))
    cdb.sim[key].md.data['id_spin_starA'] = write_three(run_md['id_spin_starA'])
    cdb.sim[key].md.data['id_spin_starB'] = write_three(run_md['id_spin_starB'])
        #print(cdb.sim[key].md.data['id_spin_starA'])
    cdb.sim[key].md.data['id_gw_frequency_Momega22'] = "{:.3f}".format(float(run_md['id_gw_frequency_Momega22']))
    cdb.sim[key].md.data['grid_spacing_min'] = min(resos)
    mdlist.append(cdb.sim[key].md)
    
# update the index 
idb.update_from_mdlist(mdlist)

# write the index to JSON with the appropriate template 
#idb.to_json_tmplk()
idb.to_json_tmplk()
