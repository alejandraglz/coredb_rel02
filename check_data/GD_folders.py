from watpy.coredb.metadata import *
from watpy.utils.ioutils import *
from watpy.coredb.coredb import *
import os 
import shutil
import numpy as np
from pathlib import Path


base = '/data/numrel/DATABASE/Release02/'

sim_folder = base + 'Sim/'
sim_folders = os.listdir(sim_folder)


for sim_dir in sim_folders:
    metamain_dir = sim_folder + sim_dir
    path = Path(metamain_dir)
    owner = path.owner()
    if not owner=="georgios.doulis":
        continue
    else:
        if sim_dir=="BAM_SLy_1.350_1.350_0.00_0.00_0.038_EFL":
            new_folder = "BAM_0097_EFL"
        else:
            new_folder = "BAM_0100_EFL"

        path2sim = base+new_folder
        if not os.path.exists(path2sim):
            os.makedirs(path2sim)

        run_folders = os.listdir(sim_folder+sim_dir)

        # Run folders loop
        num_runs = 1
        for run_folder in run_folders:
            if os.path.isdir(sim_folder + sim_dir + '/'  + run_folder):
                #make the run folder
                run_name = 'R0'+str(num_runs)
                path2run = path2sim + '/'+ run_name
                if not os.path.exists(path2run):
                    os.makedirs(path2run)
                
                dbkey_run = new_folder.replace('_',':') + ":" + run_name

                ##copy the files
                
                #correct simname in meta
                try:
                    sim_name = run_folder.rsplit('BAM_',1)[1]
                except:
                    sim_name = run_folder
                cdb_folder = sim_folder + sim_dir + "/" + run_folder 
                
                #files from Sim/ folder
                meta_filename = cdb_folder + '/metadata.txt'
                h5_filename = cdb_folder + '/data.h5'
                
                dict = read_txt_into_dict(meta_filename)
                
                run_md = CoRe_md(path=cdb_folder,metadata=dict)
                run_md.add(key='database_key',val=dbkey_run)
                run_md.add(key='binary_type',val='BNS')
                run_md.add(key='simulation_name',val=sim_name)
                run_md.write(path=path2run)
                
                path2meta = path2run + '/metadata.txt'
                path2h5 = path2run + '/data.h5'

                #shutil.copyfile(meta_filename, path2meta)
                shutil.copyfile(h5_filename, path2h5)
                num_runs = num_runs + 1

        # added missing metadata_main
        dbkey = new_folder.replace('_',':')

        sim_name_main = sim_dir.rsplit('BAM_',1)[1]
        
        runs_in_sim = ""
        for i in range(1,num_runs):
            run_code ='R0'+str(i)
            if i == 1:
                runs_in_sim = run_code
            else:
                runs_in_sim = runs_in_sim + ", " + run_code
        
        sim_md = CoRe_md(path=cdb_folder,metadata=dict)
        sim_md.add(key='database_key',val=dbkey)
        sim_md.add(key='simulation_name',val=sim_name_main)
        sim_md.add(key='available_runs',val=runs_in_sim)
        sim_md.add(key='binary_type',val='BNS')
        sim_md.write(path=path2sim,fname='metadata_main.txt',templ=TXT_MAIN)
