from watpy.coredb.metadata import *
from watpy.utils.ioutils import *
from watpy.coredb.coredb import *
from watpy.utils.coreh5 import *
import os 
import numpy as np
from pathlib import Path

sim_folder = "/data/numrel/DATABASE/Release02/Sim/"
sim_folders = os.listdir(sim_folder)
kk = 0
for sim_dir in sim_folders:
    metamain_dir = sim_folder + sim_dir
    path = Path(metamain_dir)
    owner = path.owner()
    #if not sim_dir.startswith("THC_") or sim_dir.startswith("BAM_PRECESSION"):
    if not owner=="georgios.doulis":
        continue
    else:
        #print(sim_dir)
        #sim_name_main = sim_dir.rsplit('BAM_',1)[1]
        print(sim_dir," by ",owner)

        run_folders = os.listdir(sim_folder+sim_dir)

        # Run folders loop
        num_runs = 0
        for run_folder in run_folders:
            kk=kk+1
            if os.path.isdir(sim_folder + sim_dir + '/'  + run_folder):
                cdb_folder = sim_folder + sim_dir + "/" + run_folder #+ "/CoReDB"
                h5_filename = cdb_folder + '/data.h5'
                flag = 0
                
                try:
                    h5_file = CoRe_h5(path=cdb_folder,dfile=h5_filename)
                    dset = h5_file.read_dset()
                    #h5_file.write_to_txt()
                    #h5_file.dump()
                    #break

#### check if h5 is well grouped
                    for k in dset.keys():
                        if k=='energy':
                            continue
                        else:
                            lg,mg = h5_file.lm_from_group(k)
                            lm_g = str(lg)+str(mg)
                            for f in dset[k].keys():
                                vlmr = wfile_parse_name(f)
                                var,lf,mf,r,c = vlmr
                                lm_f = str(lf) + str(mf)
                                if lm_g==lm_f:
                                    continue
                                else:
#                                    print("group: ",lm_g,", file: ",lm_f)
                                    flag = 1
                    if flag>0:
                        print("Problem with groups and their files!")
                    else:
                        print("Alles gut!")
####
                    
                except FileNotFoundError as e:
                    print("No existing h5 file in: ",cdb_folder)
                    continue
                        
print(kk)
