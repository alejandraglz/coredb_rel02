from watpy.coredb.metadata import *
from watpy.utils.ioutils import *
from watpy.coredb.coredb import *
import os 
import numpy as np
from pathlib import Path


sim_folder = "/data/numrel/DATABASE/Release02/Sim/"
sim_folders = os.listdir(sim_folder)

sim2mod = ['BAM_MPA1_1.852_1.481_0.00_0.00_0.059','BAM_MPA1_1.744_1.570_0.00_0.00_0.059','BAM_MPA1_1.655_1.655_0.00_0.00_0.059','BAM_SLy_1.350_1.350_0.00_0.00_0.038']
add_res = {
'MPA1_1.852_1.481_0.00_0.00_0.059_R01_dxyz4.93_nxyz384_nmxyz192_l6' : '0.0770833333333333',
'MPA1_1.852_1.481_0.00_0.00_0.059_R02_dxyz6.58_nxyz288_nmxyz144_l6' : '0.102777777777778',
'MPA1_1.852_1.481_0.00_0.00_0.059_R03_dxyz9.87_nxyz192_nmxyz96_l6'  : '0.154166666666667',

'MPA1_1.744_1.570_0.00_0.00_0.059_R01_dxyz4.87_nxyz384_nmxyz192_l6' : '0.0760416666666667',
'MPA1_1.744_1.570_0.00_0.00_0.059_R02_dxyz6.49_nxyz288_nmxyz144_l6' : '0.101388888888889',
'MPA1_1.744_1.570_0.00_0.00_0.059_R03_dxyz9.73_nxyz192_nmxyz96_l6'  : '0.152083333333333',

'MPA1_1.655_1.655_0.00_0.00_0.059_R01_dxyz4.83_nxyz384_nmxyz192_l6' : '0.0755208333333333',
'MPA1_1.655_1.655_0.00_0.00_0.059_R02_dxyz6.44_nxyz288_nmxyz144_l6' : '0.100694444444444',
'MPA1_1.655_1.655_0.00_0.00_0.059_R03_dxyz9.67_nxyz192_nmxyz96_l6'  : '0.151041666666667',

'SLY_1.350_1.350_0.00_0.00_0.038_R01_xyz6.02_nxyz320_nmxyz160_l6_atm'  : '0.0940000000000000',
'SLY_1.350_1.350_0.00_0.00_0.038_R02_dxyz7.52_nxyz256_nmxyz128_l6_atm' : '0.117500000000000',
'SLY_1.350_1.350_0.00_0.00_0.038_R03_dxyz10.0_nxyz192_nmxyz96_l6_atm'  : '0.156666666666667',
'SLY_1.350_1.350_0.00_0.00_0.038_R04_dxyz15.0_nxyz128_nmxyz64_l6_atm'  : '0.235000000000000',

'SLY_1.350_1.350_0.00_0.00_0.038_R01_dxyz6.02_nxyz320_nmxyz160_l6_vac' : '0.0940000000000000',
'SLY_1.350_1.350_0.00_0.00_0.038_R02_dxyz7.52_nxyz256_nmxyz128_l6_vac' : '0.117500000000000',
'SLY_1.350_1.350_0.00_0.00_0.038_R03_dxyz10.0_nxyz192_nmxyz96_l6_vac'  : '0.156666666666667',
'SLY_1.350_1.350_0.00_0.00_0.038_R04_dxyz15.0_nxyz128_nmxyz64_l6_vac'  : '0.235000000000000'
}

for sim_dir in sim2mod:
    if sim_dir=="BAM_PRECESSION":
    #if not sim_dir=="BAM_SLY_1.350_1.350_0.00_0.00_0.038_vac":   
        continue
    else:
        metamain_dir = sim_folder + sim_dir
        path = Path(metamain_dir)
        owner = path.owner()
        print(sim_dir," by ",owner)

        run_folders = os.listdir(sim_folder+sim_dir)

        # Run folders loop
        num_runs = 0
        for run_folder in run_folders:
            if os.path.isdir(sim_folder + sim_dir + '/'  + run_folder):
                cdb_folder = sim_folder + sim_dir + "/" + run_folder + "/CoReDB"
                meta_filename = cdb_folder + '/metadata.txt'
                meta_corr = cdb_folder + '/metadata_corrected.txt'

                if run_folder in add_res.keys():
                    dict = read_txt_into_dict(meta_filename)
                    run_md = CoRe_md(path=cdb_folder,metadata=meta_filename)
                    run_md.add(key='grid_spacing_min',val=add_res[run_folder])
                    run_md.write(path=cdb_folder)

                
                #num_runs = num_runs + 1
                
                # remove () in metadata, can't write on folder, do locally!
                #dest_meta = open(meta_corr,'w')
                #source_meta = open(meta_filename,'r')
                #for line in source_meta:
                #    line1 = line.replace("(","")
                #    line2 = line1.replace(")","")
                #    dest_meta.write(line2)
                #dest_meta.close()
                #source_meta.close()
                #print(meta_corr)
           #break
        # end of runs loop
    #break
    # added missing metadata_main
    #runs_in_sim = ""
    #for i in range(1,num_runs+1):
    #    run_code ='R0'+str(i)
    #    if i == 1:
    #        runs_in_sim = run_code
    #    else:
    #        runs_in_sim = runs_in_sim + ", " + run_code
    #print(runs_in_sim)
    #    
    #sim_in_new = new_release_folder + '/' + sim_dir
    #if not os.path.exists(sim_in_new):
    #    os.makedirs(sim_in_new)
    #    temp_dir = "./"+sim_in_new
    #
    #sim_md = CoRe_md(path='.',metadata=dict)
    #sim_md.add(key='simulation_name',val=sim_name_main)
    #sim_md.add(key='available_runs',val=runs_in_sim)
    #sim_md.write(path=temp_dir,fname='metadata_main.txt',templ=TXT_MAIN)

    
                
                # Add key to metadata
                #sim_name = run_folder.rsplit('BAM_',1)[1]
                #dict = read_txt_into_dict(meta_filename)
                #if dict['simulation_name']:
                #    continue
                #else:
                #    run_md = CoRe_md(path='.',metadata=dict)
                #    run_md.add(key='simulation_name',val=sim_name)
                #    run_md.write(path=cdb_folder)'''
