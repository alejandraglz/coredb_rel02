from cmath import nan
import matplotlib
import matplotlib.pyplot as plt
import numpy as np 
import json
import h5py 
from matplotlib.ticker import (MultipleLocator, AutoMinorLocator)
import csv
import glob

from watpy.coredb.coredb import *
from watpy.wave.wave import *
from watpy.utils.coreh5 import *
from watpy.utils.ioutils import *

import sys
import os
sys.path.append('../')
from mismatch import *
from handy_functions import *

P = pd.read_csv("../data/database.csv")
#sim_path    = '/data/numrel/DATABASE/CoRe_DB_clone/'
rel2_path   = '/data/numrel/DATABASE/Release02/'
#cdb = CoRe_db(sim_path)
#idb = cdb.idb
#rel2_path = '/home/agonzalez/Documents/git/watpy/scripts'


#to_correct = ['BAM:0132', 'BAM:0133', 'BAM:0134', 'BAM:0135', 'BAM:0138', 'BAM:0139', 'BAM:0140', 'BAM:0141', 'THC:0040']
to_correct = ['BAM_0097_EFL','BAM_0100_EFL']
#rel2_path = '/home/agonzalez/Documents/git/watpy/scripts'

for entry in to_correct:
    print('Now with: ',entry)
    this_path = os.path.join(rel2_path,entry.replace(':','_'))
    sim = CoRe_sim(this_path)
    
    for res in list(sim.run.keys()):
        run_path = this_path+'/'+res
        out, err = runcmd(['cp','data.h5','data_old.h5'],run_path,True)
        out, err = runcmd(['rm','R*.txt'],run_path,True)
        out, err = runcmd(['rm','EJ*.txt'],run_path,True)
        
