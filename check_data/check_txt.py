from watpy.coredb.metadata import *
from watpy.utils.ioutils import *
from watpy.coredb.coredb import *
import os 
import numpy as np
from pathlib import Path

new_release_folder = "new_release"
if not os.path.exists(new_release_folder):
    os.makedirs(new_release_folder)

sim_folder = "/data/numrel/DATABASE/Release02/Sim/"
sim_folders = os.listdir(sim_folder)

for sim_dir in sim_folders:
    #if not sim_dir.startswith("BAM_") or sim_dir.startswith("BAM_PRECESSION"):
    if not sim_dir=="BAM_SLY_1.350_1.350_0.00_0.00_0.038_vac":   
        continue
    else:
        metamain_dir = sim_folder + sim_dir
        path = Path(metamain_dir)
        owner = path.owner()
        print(sim_dir," by ",owner)

        run_folders = os.listdir(sim_folder+sim_dir)

        # Run folders loop
        num_runs = 0
        for run_folder in run_folders:
            if os.path.isdir(sim_folder + sim_dir + '/'  + run_folder):
                cdb_folder = sim_folder + sim_dir + "/" + run_folder + "/CoReDB"
                meta_filename = cdb_folder + '/metadata.txt'
                run_files = os.listdir(cdb_folder)
                
                # Files in run folder loop
                for txt_file in run_files:
                    path_txt = cdb_folder + '/' + txt_file
                    size = os.path.getsize(path_txt)

                    #check if empty
                    if size>0:
                        continue
                    else:
                        print(path_txt," is empty")
                    
                    #check for nans
                    txt = open(path_txt,'r')
                    nan_str = ["NaN","NAN","nan"]
                    flag = 0
                    for line in txt:
                        for s in nan_str:
                            if s in line:
                                flag = 1
                    if flag==0:
                        continue
                    else:
                        print("NaN value in: ",path_txt)
