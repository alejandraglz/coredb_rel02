import os
import sys
import numpy as np
import h5py
from watpy.utils.viz import wplot
from watpy.coredb.coredb import *
from watpy.coredb.metadata import *
from watpy.wave.wave import *
from watpy.utils.ioutils import *
from watpy.utils.coreh5 import *
import json

#git config --global user.email "you@example.com"
#git config --global user.name "Your Name"

def git_push(dir,entry,verbose=True):
    print("Working in {}",dir)
    #email = 'alejandra.gonzalez@uni-jena.de'
    #username='agonzalez'
    #out, err =runcmd(['git','config','--global','user.email',email],dir,True)
    #print(out,err)
    #out, err =runcmd(['git','config','--global','user.name',username],dir,True)
    #print(out,err)
    out, err = runcmd(['git','init'],dir,True)
    print(out,err)
    out, err = runcmd(['git','add','.'],dir,True)
    print(out,err)
    commit = 'Initial commit'
    out = runcmd(['git','commit','-m',commit], dir, True)
    print(out,err) 
    #url = 'https://core-gitlfs.tpi.uni-jena.de:core_database/'+entry+'.git'
    url = 'git@core-gitlfs.tpi.uni-jena.de:core_database/'+entry+'.git'
    out = runcmd(['git','push','--set-upstream',url,'master'],dir,True)
    print(out,err)
    print("done!")

def correct_meta(meta_main):
    simname = meta_main['simulation_name'].split('_')
    simlen = len(simname)
    if simname[0]=='sly' or simname[0]=='SLY':
        simname[0] = 'SLy'
        meta_main['simulation_name'] = '_'.join(simname)
        meta_main['id_eos'] = simname[0]
    elif simname[0] == 'MS1B':
        simname[0] = 'MS1b'
        meta_main['simulation_name'] = '_'.join(simname)
        meta_main['id_eos'] = simname[0]

    if meta_main['id_eos']=='SLY':
        meta_main['id_eos']='SLy'
    elif meta_main['id_eos']=='MS1B':
        meta_main['id_eos']='MS1b'
    
    return meta_main

db_path = '/data/numrel/DATABASE/CoRe_DB_clone'
rel2_path   = '/data/numrel/DATABASE/Release02/'
cdb = CoRe_db(db_path)
idb = cdb.idb

direcs = os.listdir(rel2_path)
direcs.remove('Sim')
direcs.remove('README')
direcs.remove('BAM_0097_EFL')
direcs.remove('BAM_0100_EFL')
direcs.sort()

'''
### ADD SIM DIRECTORY AND TO DBKEYS
for entry in direcs:
    dbkey = entry.replace('_',':')
    code = entry.split('_')[0]
    this_path = rel2_path + entry
    metamain = this_path + '/metadata_main.txt'
    sim = CoRe_sim(this_path)
    run_list = list(sim.run.keys())
    run_list.sort()
    meta_main = sim.md.data
    meta_main = correct_meta(meta_main)

    newdbkey = cdb.add_simulation(code,meta_main['simulation_name'], metadata = meta_main)

    for res in run_list:
        print('In :', res)
        run_path = this_path+'/'+res
        simrun = sim.run[res]
        meta_dict = simrun.md.data
        meta_dict = correct_meta(meta_dict)
        cdb.sim[newdbkey].add_run(path = run_path , metadata = meta_dict)

#for entry in direcs:
#    out, err = runcmd(['rm','-r',entry],db_path,True)

### UPDATE CORE INDEX
# read metadata_main in a list
mdlist = []
print(cdb.idb.dbkeys)
for key in idb.dbkeys:
    mdlist.append(cdb.sim[key].md)

# update the index 
idb.update_from_mdlist(mdlist)

# write the index to JSON with the appropriate template 
idb.to_json_tmplk()


print(direcs[3:])


for entry in direcs[3:]:
    print('In: ', entry)
    git_path = db_path + '/' + entry
    sim = CoRe_sim(git_path)
    run_list = list(sim.run.keys())
    run_list.sort()
    
    ## ADD .GITATTRIBUTES FILE
    file_path = git_path + '/.gitattributes'
    if os.path.exists(file_path):
        print('File exists!')
    else:
        git_file = open(file_path, "w")
        L = []
        for res in run_list:
            git_string = res+'/data.h5 filter=lfs diff=lfs merge=lfs -text \n'
            L.append(git_string)
        git_file.writelines(L)
        git_file.close()
        print('Wrote file!')

    ## CREATE REPO AND PUSH
    print('Initialize git repo ..')
    git_push(git_path,entry)
'''

##  ADD RUNS TO EXISTING SIM DBKEY
add_simruns = ['BAM_0097_EFL','BAM_0100_EFL']

for entry in add_simruns:
    dbname = entry.split('_')[0]+':'+entry.split('_')[1]
    sim = cdb.sim[dbname]
    
    this_path = rel2_path + entry
    sim_old = CoRe_sim(this_path)
    runs_in_sim = list(sim_old.run.keys())
    runs_in_sim.sort()
    for res in runs_in_sim:
        run_path = this_path+'/'+res
        simrun = sim_old.run[res]
        meta_dict = simrun.md.data
        meta_dict = correct_meta(meta_dict)
        cdb.sim[dbname].add_run(path = run_path , metadata = meta_dict)
