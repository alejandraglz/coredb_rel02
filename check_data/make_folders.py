from watpy.coredb.metadata import *
from watpy.utils.ioutils import *
from watpy.coredb.coredb import *
import os 
import shutil
import numpy as np
from pathlib import Path

def str_format(id):
    digits = len(str(id))
    if digits==2:
        banana = '00'+str(id)
    elif digits==3:
        banana = '0'+str(id)
    return banana

base = '/data/numrel/DATABASE/Release02/'

sim_folder = base + 'Sim/'
sim_folders = os.listdir(sim_folder)

bam_id = 129
thc_id = 37

for sim_dir in sim_folders:
    if sim_dir.startswith("BAM_PRECESSION") or sim_dir.startswith("BAM_SLY_1.350_1.350_0.00_0.00_0.038_") or sim_dir.startswith("BAM_H4_"):
        continue
    else:
        if sim_dir.startswith("BAM_"):
            new_folder = "BAM_"+str_format(bam_id)
            bam_id = bam_id + 1
        else:
            new_folder = "THC_"+str_format(thc_id)
            thc_id = thc_id + 1

        path2sim = base+new_folder
        if not os.path.exists(path2sim):
            os.makedirs(path2sim)

        metamain_dir = sim_folder + sim_dir
        run_folders = os.listdir(sim_folder+sim_dir)

        # Run folders loop
        num_runs = 1
        for run_folder in run_folders:
            if os.path.isdir(sim_folder + sim_dir + '/'  + run_folder):
                #make the run folder
                run_name = 'R0'+str(num_runs)
                path2run = path2sim + '/'+ run_name
                if not os.path.exists(path2run):
                    os.makedirs(path2run)
                
                dbkey_run = new_folder.replace('_',':') + ":" + run_name

                ##copy the files
                
                #correct simname in meta
                if sim_dir.startswith("BAM_"):
                    try:
                        sim_name = run_folder.rsplit('BAM_',1)[1]
                    except:
                        sim_name = run_folder
                    cdb_folder = sim_folder + sim_dir + "/" + run_folder + "/CoReDB"
                else:
                    sim_name = run_folder.rsplit('THC_',1)[1]
                    cdb_folder = sim_folder + sim_dir + "/" + run_folder

                #files from Sim/ folder
                meta_filename = cdb_folder + '/metadata.txt'
                h5_filename = cdb_folder + '/data.h5'
                
                dict = read_txt_into_dict(meta_filename)
                
                run_md = CoRe_md(path=cdb_folder,metadata=dict)
                run_md.add(key='database_key',val=dbkey_run)
                run_md.add(key='binary_type',val='BNS')
                run_md.add(key='simulation_name',val=sim_name)
                run_md.write(path=path2run)
                
                path2meta = path2run + '/metadata.txt'
                path2h5 = path2run + '/data.h5'

                #shutil.copyfile(meta_filename, path2meta)
                shutil.copyfile(h5_filename, path2h5)
                num_runs = num_runs + 1

        # added missing metadata_main
        dbkey = new_folder.replace('_',':')

        if sim_dir.startswith("BAM_"):
            sim_name_main = sim_dir.rsplit('BAM_',1)[1]
        else:
            sim_name_main = sim_dir.rsplit('THC_',1)[1]

        runs_in_sim = ""
        for i in range(1,num_runs):
            run_code ='R0'+str(i)
            if i == 1:
                runs_in_sim = run_code
            else:
                runs_in_sim = runs_in_sim + ", " + run_code
        
        sim_md = CoRe_md(path=cdb_folder,metadata=dict)
        sim_md.add(key='database_key',val=dbkey)
        sim_md.add(key='simulation_name',val=sim_name_main)
        sim_md.add(key='available_runs',val=runs_in_sim)
        sim_md.add(key='binary_type',val='BNS')
        sim_md.write(path=path2sim,fname='metadata_main.txt',templ=TXT_MAIN)
