#########################################
# Alignment functions from watpy
#########################################
import numpy as np
from scipy.signal import tukey

def CreateDict_dev(lmax, m1, m2, chi1, chi2, l1, l2, iota, f0, srate, df, interp, domain):
    """
    Create the dictionary of parameters for EOBRunPy (for 22)
    """
    M = m1+m2
    q = m1/m2

    x = [lmax, lmax]
    kmax = int(x[0]*(x[0]-1)/2 + x[1]-2) + 1
    mode_list = list(range(0, kmax))

    pardic = {
    'M'                  : M,
    'q'                  : q,
    'chi1'               : chi1,
    'chi2'               : chi2,
    'Lambda1'            : l1,
    'Lambda2'            : l2,
    'use_spins'          : 1,
    'use_flm'            : "HM",
    'nqc'                : "auto",
    'nqc_coefs_flx'      : "nrfit_spin202002",
    'nqc_coefs_hlm'      : "compute",
    'tides'              : "TEOBRESUM",
    'tides_gravitomagnetic' : "yes",
    'use_lambda234_fits' : "YAGI13",
    'distance'           : 410., #1
    'initial_frequency'  : 0.005, #f0
    'use_geometric_units': "yes",
    'interp_uniform_grid': "yes",
    'domain'             : domain,
    'srate_interp'       : srate,
    'inclination'        : 0.0,
    'output_multipoles'  : "yes",
    'output_lm'          : [0,1,3,4,8,13],#[0,1,3,4,8,13],#[1],
    'use_mode_lm'        : [0,1,3,4,8,13],#[0,1,3,4,8,13],#[1],      # List of modes to use/output through EOBRunPy
    'compute_LR'         : "no",
    'compute_LSO'        : "no",
    'dt'                 : 0.5,
    'arg_out'            : 1,      #Output hlm/hflm. Default = 0
    }
    return pardic

def CreateDict(lmax, m1, m2, chi1, chi2, l1, l2, iota, f0, srate, df, interp, domain,mode):
    """
    Create the dictionary of parameters for EOBRunPy (for 22)
    """
    M = m1+m2
    q = m1/m2

    x = [lmax, lmax]
    kmax = int(x[0]*(x[0]-1)/2 + x[1]-2) + 1
    mode_list = list(range(0, kmax))

    pardic = {
    'M'                  : M,
    'q'                  : q,
    'chi1'               : chi1,
    'chi2'               : chi2,
    'Lambda1'            : l1,
    'Lambda2'            : l2,
    'use_flm'            : 3,
    #'use_tidal'          : 2,
    'distance'           : 410, #1
    'initial_frequency'  : 0.005, #f0
    'use_geometric_units': 1,
    'interp_uniform_grid': interp,
    'domain'             : domain,
    'srate_interp'       : srate,
    'inclination'        : iota,
    'use_mode_lm'        : [mode],      # List of modes to use/output through EOBRunPy
    'output_hpc'         : 0,
    'output_multipoles'  : 0, 
    'arg_out'            : 1,
    'output_dynamics'    : 0,
    'dt'                 : 0.5,
    }
    return pardic


def CreateDict_HM(lmax, m1, m2, chi1, chi2, l1, l2, iota, f0, srate, df, interp, domain):
    """
    Create the dictionary of parameters for EOBRunPy (for 22)
    """
    M = m1+m2
    q = m1/m2

    x = [lmax, lmax]
    kmax = int(x[0]*(x[0]-1)/2 + x[1]-2) + 1
    mode_list = list(range(0, kmax))

    pardic = {
    'M'                  : M,
    'q'                  : q,
    'chi1'               : chi1,
    'chi2'               : chi2,
    'Lambda1'            : l1,
    'Lambda2'            : l2,
    'use_flm'            : 3,
    'use_tidal'          : 2,
    'distance'           : 410, #1
    'initial_frequency'  : 0.005, #f0
    'use_geometric_units': 1,
    'interp_uniform_grid': interp,
    'domain'             : domain,
    'srate_interp'       : srate,
    'inclination'        : iota,
    'use_mode_lm'        : [0,1,3,4,8,13],      # List of modes to use/output through EOBRunPy
    'output_hpc'         : 0,
    'output_multipoles'  : 0, 
    'arg_out'            : 1,
    'output_dynamics'    : 0,
    'dt'                 : 0.5,
    }
    return pardic

def CreateDict_phi(lmax, m1, m2, chi1, chi2, l1, l2, iota, phi_c, f0, srate, df, interp, domain):
    """
    Create the dictionary of parameters for EOBRunPy with phi_c and HM
    """
    M = m1+m2
    q = m1/m2

    x = [lmax, lmax]
    kmax = int(x[0]*(x[0]-1)/2 + x[1]-2) + 1
    mode_list = list(range(0, kmax))

    pardic = {
    'M'                  : M,
    'q'                  : q,
    'chi1'               : chi1,
    'chi2'               : chi2,
    'Lambda1'            : l1,
    'Lambda2'            : l2,
    'use_flm'            : 3,
    'use_tidal'          : 2,
    'distance'           : 410, #1
    'initial_frequency'  : 0.005, #f0
    'use_geometric_units': 1,
    'interp_uniform_grid': interp,
    'domain'             : domain,
    'srate_interp'       : srate,
    'inclination'        : iota,
    'coalescence_angle'  : phi_c,
    'use_mode_lm'        : [0,1,3,4,8,13],      # List of modes to use/output through EOBRunPy
    'output_hpc'         : 0,
    'output_multipoles'  : 0, 
    'arg_out'            : 1,
    'output_dynamics'    : 0,
    'dt'                 : 0.5,
    }
    return pardic


def align_phase(t, Tf, phi_a_tau, peb):
    """
        
        * t         : time, must be equally spaced
        * Tf        : final time
        * phi_a_tau : time-shifted first phase evolution
        * peb     : second phase evolution
        
        This function returns \Delta\phi.
    """
    dt = t[1] - t[0]
    weight = np.double((np.array(t) >= 0.0) & (t < Tf))
    
    return np.sum(weight * (phi_a_tau - peb) * dt) / np.sum(weight * dt)

def Align(t, Tf, tau_max, tnr, pnr, teb, peb):
    """
        Align two waveforms in phase by minimizing the chi^2
        
        chi^2 = \sum_{t_i=0}^{t_i < Tf} [pnr(t_i + tau) - peb(t_i) - dphi]^2 dt
        
        as a function of dphi and tau.
        
        * t          : time
        * Tf         : final time
        * tau_max    : maximum time shift
        * tnr, pnr : first phase evolution
        * teb, peb : second phase evolution
        
        The two waveforms are re-sampled using the given time t
        
        This function returns a tuple (tau_opt, dphi_opt, chi2_opt)
    """
    dt = t[1] - t[0]
    N = int(tau_max/dt)
    weight = np.double((np.array(t) >= 0) & (t < Tf))
    
    res_phi_b = np.interp(t, teb, peb)
    
    tau = []
    dphi = []
    chi2 = []
    for i in range(-N, N):
        tau.append(i*dt)
        res_phi_a_tau = np.interp(t, tnr + tau[-1], pnr)
        dphi.append(align_phase(t, Tf, res_phi_a_tau, res_phi_b))
        chi2.append(np.sum(weight*
                              (res_phi_a_tau - res_phi_b - dphi[-1])**2)*dt)
    
    chi2 = np.array(chi2)
    imin = np.argmin(chi2)

    return tau[imin], dphi[imin], chi2[imin]

def windowing(h, alpha=0.1): 
   """ Perform windowing with Tukey window on a given strain (time-domain) 
       __________ 
       h    : strain to be tapered 
       alpha : Tukey filter slope parameter. Suggested value: alpha = 1/4/seglen 
       Only tapers beginning of wvf, to taper both, comment: window[len(h)//2:] = 1.
   """ 
   window = tukey(len(h), alpha) 
   #window[len(h)//2:] = 1. 
   wfact  = np.mean(window**2) 
   window = np.array(window) 
   return h*window, wfact

