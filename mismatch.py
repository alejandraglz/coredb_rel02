#! /usr/bin/env python

################
# modules 
################

import numpy as np 
import matplotlib.pyplot as plt
import math
from math import factorial as fact
from scipy import optimize
from scipy import interpolate
from pycbc.filter import compute_max_snr_over_sky_loc_stat_no_phase, sigmasq, matched_filter_core, overlap_cplx, overlap, get_cutoff_indices
from scipy.signal import tukey

Pi     = math.pi 
Msuns  = 4.925491025543575903411922162094833998e-6
Mpc_m  = 3.085677581491367278913937957796471611e22
Msun_m = 1.476625061404649406193430731479084713e3
KMAX   = 14

##############
# functions 
##############

def modes_to_k(modes):
    return [int(x[0]*(x[0]-1)/2 + x[1]-2) for x in modes]

def k_to_ell(k):
    LINDEX = [\
    2,2,\
    3,3,3,\
    4,4,4,4,\
    5,5,5,5,5,\
    6,6,6,6,6,6,\
    7,7,7,7,7,7,7,\
    8,8,8,8,8,8,8,8]
    return LINDEX[k]

def k_to_emm(k):
    MINDEX = [\
    1,2,\
    1,2,3,\
    1,2,3,4,\
    1,2,3,4,5,\
    1,2,3,4,5,6,\
    1,2,3,4,5,6,7,\
    1,2,3,4,5,6,7,8];
    return MINDEX[k]   

def spinsphericalharm(s, l, m, phi, i):  
  c = pow(-1.,-s) * np.sqrt( (2.*l+1.)/(4.*Pi) )
  dWigner = c * wigner_d_function(l,m,-s,i)
  rY = np.cos(m*phi) * dWigner
  iY = np.sin(m*phi) * dWigner
  return rY + 1j*iY

def wigner_d_function(l,m,s,i):
  costheta = np.cos(i*0.5)
  sintheta = np.sin(i*0.5)
  norm = np.sqrt( (fact(l+m) * fact(l-m) * fact(l+s) * fact(l-s)) )
  ki = max( 0  , m-s )
  kf = min( l+m, l-s )
  dWig = 0.
  for k in range(int(ki), int(kf)+1):
    div = 1.0/( fact(k) * fact(l+m-k) * fact(l-s-k) * fact(s-m+k) )
    dWig = dWig+div*( pow(-1.,k) * pow(costheta,2*l+m-s-2*k) * pow(sintheta,2*k+s-m) )
  
  return (norm * dWig)

##############################################################
# Compute hp, hc for SXS waveforms (ligic similar to C code)
##############################################################

def get_A_NR(dict, N=0):
    """
    Compute the aplitude of the NR waveform, skipping the first N points
    """
    r = np.sqrt(dict[N:,1]**2 +  dict[N:,2]**2)
    return r

def get_p_NR(dict, N=0):
    """
    Compute the phase of the NR waveform, skipping the first N points
    """
    r = -np.unwrap(np.arctan2(dict[N:,2], dict[N:,1]))
    return r

def InterpAmpPhase(A, p, t, tnew):
    """
    Interpolate Amplitude(t) and phase(t) on a new time array tnew
    """
    Af   = interpolate.interp1d(t, A, fill_value='extrapolate')
    Pf   = interpolate.interp1d(t, p, fill_value='extrapolate')
    Anew = Af(tnew)
    Pnew = Pf(tnew)
    return Anew, Pnew

def ComputeHpHc(d, d_neg, d0, i, prf, activemode):
    Ylm_d     = {}
    Ylm_d_neg = {}
    Ylm_d_0   = {}
    ellmax    = 2

    for k in range(0, KMAX):
        ell = k_to_ell(k)
        m   = k_to_emm(k)
        Ylm_d[k]    = spinsphericalharm(-2, ell, m, np.pi/2, i)
        Ylm_d_neg[k]= spinsphericalharm(-2, ell,-m, np.pi/2, i)
        
        ellmax = ell
    
    for l in range(2, ellmax+1):
        Ylm_d_0[l]  = spinsphericalharm(-2, l, 0, 0., i)

    sumr=0 
    sumi=0
    # Loop over modes
    for k in range(0, KMAX): 
        
        if (not activemode[k]): 
            continue
        Aki    = prf*d[k][0]
        cosPhi = np.cos(d[k][1])
        sinPhi = np.sin(d[k][1])
        sumr   = sumr+ Aki*(cosPhi*np.real(Ylm_d[k]) + sinPhi*np.imag(Ylm_d[k]))
        sumi   = sumi+ Aki*(cosPhi*np.imag(Ylm_d[k]) - sinPhi*np.real(Ylm_d[k])) 
          
	    # add m<0 modes
        Aki    = prf*d_neg[k][0]
        cosPhi = np.cos(d_neg[k][1])
        sinPhi = np.sin(d_neg[k][1])
        sumr   = sumr+ Aki*(cosPhi*np.real(Ylm_d_neg[k]) + sinPhi*np.imag(Ylm_d_neg[k]))
        sumi   = sumi+ Aki*(cosPhi*np.imag(Ylm_d_neg[k]) - sinPhi*np.real(Ylm_d_neg[k])) 
    
    # M = 0
    #for l in range(2, ellmax+1):
    #    if (d0[l] == None):
    #        continue
    #    Aki    = prf*d0[l][0]
    #    cosPhi = np.cos(d0[l][1])
    #    sinPhi = np.sin(d0[l][1])
    #    sumr   = sumr+ Aki*(cosPhi*np.real(Ylm_d_0[l]) + sinPhi*np.imag(Ylm_d_0[l]))
    #    sumi   = sumi+ Aki*(cosPhi*np.imag(Ylm_d_0[l]) - sinPhi*np.real(Ylm_d_0[l]))
      
    #h = h+ - i hx
    hp = sumr
    hc = -sumi
    
    return hp, hc

def ComputeHpHc_SXS(gw_sxs, lmax, i, prf, M, dT, N):
    d     = {}
    d_neg = {}
    d0    = {}
    # initialize d0 and actmodes:
    actmodes = np.zeros(KMAX)
    for l in range(2, k_to_ell(KMAX)+1):
        d0[l] = None
    
    # read all modes in dictionary
    for ell in range(2, lmax+1):
        for m in range(-ell, ell+1):
            if m == 0:
                continue #skip m = 0 modes

            ylm_str = "Y_l"+str(ell)+"_m"+str(m)+".dat" 
            gw_ext = gw_sxs["Extrapolated_N4.dir"][ylm_str]
            A      = get_A_NR(gw_ext, N)
            p      = get_p_NR(gw_ext, N)
            t_SI   = np.array(gw_ext[N:,0])*M*Msuns
            t_new  = np.arange(t_SI[0], t_SI[-1], dT)
            A, p   = InterpAmpPhase(A,  p,  t_SI, t_new)
            if m < 0:   #handle cases separately
                if m==-1:
                    continue
                m = -m
                k           = modes_to_k([[ell, m]])[0]
                d_neg[k]    = [A, p]
                actmodes[k] = 1

            elif m > 0:
                if m==1:
                    continue
                if m==2:
                    k           = modes_to_k([[ell, m]])[0]
                    d[k]        = [A, p]
                    actmodes[k] = 1
            else:
                d0[ell]     = [A, p]
    # from dictionary, compute hp,hc
    hp, hc = ComputeHpHc(d, d_neg, d0, i, prf, actmodes)
    return t_new, hp, hc

def ComputeHpHc_SXS_HM(gw_sxs, lmax, i, prf, M, dT, N):
    # for BHNS modes: (2,1), (2,2), (3,2), (3,3), (4,4), (5,5)
    d     = {}
    d_neg = {}
    d0    = {}
    # initialize d0 and actmodes:
    actmodes = np.zeros(KMAX)
    for l in range(2, k_to_ell(KMAX)+1):
        d0[l] = None
    
    # read all modes in dictionary
    for ell in range(2, lmax+1):
        for m in range(-ell, ell+1):
            if m == 0:
                continue #skip m = 0 modes

            ylm_str = "Y_l"+str(ell)+"_m"+str(m)+".dat" 
            gw_ext = gw_sxs["Extrapolated_N4.dir"][ylm_str]
            A      = get_A_NR(gw_ext, N)
            p      = get_p_NR(gw_ext, N)
            t_SI   = np.array(gw_ext[N:,0])*M*Msuns
            t_new  = np.arange(t_SI[0], t_SI[-1], dT)
            A, p   = InterpAmpPhase(A,  p,  t_SI, t_new)
            if m < 0:   #handle cases separately
                if( ((ell==3)and(m==-1)) or ((ell==4)and(np.abs(m)<ell)) or ((ell==5)and(np.abs(m)<ell)) ):
                    continue
                m = -m
                k           = modes_to_k([[ell, m]])[0]
                d_neg[k]    = [A, p]
                actmodes[k] = 1

            elif m > 0:
                if( ((ell==3)and(m==1)) or ((ell==4)and(m<ell)) or ((ell==5)and(m<ell)) ):
                    continue
                k           = modes_to_k([[ell, m]])[0]
                d[k]        = [A, p]
                actmodes[k] = 1
            else:
                d0[ell]     = [A, p]
    # from dictionary, compute hp,hc
    hp, hc = ComputeHpHc(d, d_neg, d0, i, prf, actmodes)
    return t_new, hp, hc


########################################
# Compute SXS waveforms with LALSuite 
########################################
# NOTE: needs files from in the format of the lvcnr repo, https://git.ligo.org/waveforms/lvcnr-lfs


def ReadSXSWithLAL(fname, M, iota, dT, DL = 1., phi_ref=0., modes=[(2,2)]):
    import h5py
    try:
        import lalsimulation as lalsim
        import lal  
    except Exception:
        print("Error importing LALSimulation and/or LAL")
    
    # create empty LALdict
    params = lal.CreateDict()

    # read h5
    f = h5py.File(fname, 'r')

    # Modes (default (2,2) only)
    modearr= lalsim.SimInspiralCreateModeArray()
    for mode in modes:
        lalsim.SimInspiralModeArrayActivateMode(modearr, mode[0], mode[1])
    lalsim.SimInspiralWaveformParamsInsertModeArray(params, modearr)
    lalsim.SimInspiralWaveformParamsInsertNumRelData(params, fname)

    Mt = f.attrs['mass1'] + f.attrs['mass2']
    m1 = f.attrs['mass1']*M/Mt
    m2 = f.attrs['mass2']*M/Mt
    m1SI = m1 * lal.MSUN_SI
    m2SI = m2 * lal.MSUN_SI
    DLmpc= DL*1e6*lal.PC_SI #assuming DL given in in Mpc

    flow = f.attrs['f_lower_at_1MSUN']/M
    fref = flow

    spins = lalsim.SimInspiralNRWaveformGetSpinsFromHDF5File(fref, M, fname)
    c1x, c1y, c1z = spins[0], spins[1], spins[2]
    c2x, c2y, c2z = spins[3], spins[4], spins[5]

    new_params = {}
    new_params['m1'] = m1
    new_params['m2'] = m2 
    new_params['chiA'] = [c1x, c1y, c1z]
    new_params['chiB'] = [c2x, c2y, c2z]
    new_params['flow'] = flow 

    if(0):
        print("chiA=", c1x, c1y, c1z)
        print("chiB=", c2x, c2y, c2z)
    
    hp, hc = lalsim.SimInspiralChooseTDWaveform(m1SI,m2SI,c1x,c1y,c1z,c2x,c2y,c2z,DLmpc,iota,phi_ref,0.,0.,0.,dT,flow,fref,params, lalsim.NR_hdf5)

    return hp, hc, new_params

#############################################
# Sky maximized match (HMs and precession)
#############################################

def dual_annealing(func,bounds,maxfun=500):
    result= optimize.dual_annealing(func, bounds, maxfun=maxfun, local_search_options={'method': 'SLSQP','tol': 1e-3})
    opt_pars,opt_val=result['x'],result['fun']

    return opt_pars, opt_val

def snr(h, psd, low_freq, high_freq):
    hh = sigmasq(h,  psd=psd, low_frequency_cutoff=low_freq, high_frequency_cutoff=high_freq)
    return np.sqrt(hh)

def sky_and_time_maxed_match(s, hp, hc, psd, low_freq, high_freq):
    
    ss   = sigmasq(s,  psd=psd, low_frequency_cutoff=low_freq, high_frequency_cutoff=high_freq)
    hphp = sigmasq(hp, psd=psd, low_frequency_cutoff=low_freq, high_frequency_cutoff=high_freq)
    hchc = sigmasq(hc, psd=psd, low_frequency_cutoff=low_freq, high_frequency_cutoff=high_freq)
    hp  /= np.sqrt(hphp)
    hc  /= np.sqrt(hchc)

    rhop, _, nrm     = matched_filter_core(hp,s, psd = psd, low_frequency_cutoff=low_freq, high_frequency_cutoff=high_freq)
    rhop *= nrm
    rhoc, _, nrm     = matched_filter_core(hc,s, psd = psd, low_frequency_cutoff=low_freq, high_frequency_cutoff=high_freq)
    rhoc *= nrm
    
    # Ipc, _, _      = matched_filter_core(hp, hc, psd = psd, low_frequency_cutoff=flow, high_frequency_cutoff=high_freq)
    # Ipc            = Ipc.real()
    # print(Ipc)
    #kminPSD, kmaxPSD = get_cutoff_indices(low_freq, high_freq, psd.delta_f, (len(psd)-1) * 2)
    #print(kminPSD, kmaxPSD)
    #kminH, kmaxH     = get_cutoff_indices(low_freq, high_freq, hp.delta_f, (len(hp)-1) * 2)
    #print(kminH, kmaxH)
    # Ipc = np.real(np.dot(np.conjugate(hp.data[kminH:kmaxH]), hc.data[kminH:kmaxH]/psd.data[kminPSD:kmaxPSD]))
    # print(Ipc)

    hphccorr       = overlap_cplx(hp, hc, psd=psd, low_frequency_cutoff=low_freq, high_frequency_cutoff=high_freq)# matched_filter_core(hp,hc)
    hphccorr       = np.real(hphccorr)
    Ipc            = hphccorr

    #Ipc = hphccorr
    if(0):
        plt.plot(rhop.sample_times, rhop)
        plt.plot(rhoc.sample_times, rhoc, '--')
        plt.show()

    # rhop2 = np.abs(rhop.data)**2
    # rhoc2 = np.abs(rhoc.data)**2
    # gamma = rhop.data * np.conjugate(rhoc.data)
    # gamma = np.real(gamma)

    # sqrt_part = np.sqrt((rhop2-rhoc2)**2 + 4*(Ipc*rhop2-gamma)*(Ipc*rhoc2-gamma))
    # num       = rhop2 - 2.*Ipc*gamma + rhoc2 + sqrt_part
    # den       = 1. - Ipc**2
    
    # o = np.sqrt(max(num)/den/2.)/np.sqrt(ss)
    # if (o > 1.):
    #     o = 1.

    snr = compute_max_snr_over_sky_loc_stat_no_phase(rhop.data, rhoc.data, hphccorr, hpnorm=1., hcnorm=1.)
    snr = np.sqrt(max(snr**2))
    o   = snr/np.sqrt(ss)
    return o

def rotate_in_plane_spins(chiA,chiB,theta=0.):
    from scipy.spatial.transform import Rotation

    zaxis   = np.array([0, 0, 1])
    r       = Rotation.from_rotvec(theta*zaxis)
    chiA_rot=r.apply(chiA)
    chiB_rot=r.apply(chiB)
    #print("z comps A: ", chiA_rot[2], chiA[2])
    #print("z comps B: ", chiB_rot[2], chiB[2])
    return chiA_rot, chiB_rot

def snr_weighted_match(ms, snrs):
    ms   = np.array(ms) 
    snrs = np.array(snrs)
    ms3  = ms**3
    snr3 = snrs**(3.)

    num  = np.sum(ms3*snr3)
    den  = np.sum(snr3)
    snrw = (num/den)**(1./3.)

    return snrw 

#######################################
def windowing(h, alpha=0.1): 
   """ Perform windowing with Tukey window on a given strain (time-domain) 
       __________ 
       h    : strain to be tapered 
       alpha : Tukey filter slope parameter. Suggested value: alpha = 1/4/seglen 
       Only tapers beginning of wvf, to taper both, comment: window[len(h)//2:] = 1.
   """ 
   window = tukey(len(h), alpha) 
   #window[len(h)//2:] = 1. 
   wfact  = np.mean(window**2) 
   window = np.array(window) 
   return h*window, wfact