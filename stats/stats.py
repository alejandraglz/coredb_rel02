import string
from tkinter import E
from turtle import color
import numpy as np
import matplotlib.pyplot as plt
from matplotlib import cm, markers
from matplotlib.colors import ListedColormap, LinearSegmentedColormap
from matplotlib.ticker import (MultipleLocator, AutoMinorLocator)
import array
import matplotlib
import pandas as pd
import csv
from watpy.utils.ioutils import *

import sys
sys.path.append('../')
import csv
from handy_functions import *

matplotlib.rcParams['text.usetex']= True
matplotlib.rcParams['font.serif']= 'Times New Roman'
matplotlib.rcParams['font.size']= 32 #28

##############################
# DATA
##############################

data = csv_reader("../data/database.csv")

P = get_df(data) #ready to go pandas df
#P.index = np.arange(1, len(P)+1)
print(P)

## Add info about every run as a list
json_file = "../breschi_fits/results/fits_results.json"
jdict_list = read_json_into_dict(json_file)

reso = []
for ip, pp in enumerate(P['name']):
    ss = ""
    for dict in jdict_list:
        if not dict==None:
            if dict['database_key'].startswith(pp):
                ss = ss + str(dict["resolution"]) + ' '
    reso.append(ss)  

reso = pd.Series(reso)
reso = reso.to_frame()
reso.columns=['resolution']

P['resolution'] = reso

## correct eos
for i,ee in enumerate(P['eos']):
    if ee == "sly" or ee=="SLY":
        P.at[i,'eos']="SLy"
    elif ee == "MS1B":
        P.at[i,'eos']="MS1b"


################################
# PLOTTING
################################

#####
# simple histograms
#####
''' fig, axs = plt.subplots(2, 2,figsize=(20,10))
bins=50
plt.suptitle(r'\scshape CoRe Database')
axs[0,0].hist(P['mass'], bins, histtype ='step',stacked = True,fill = False)
axs[0,0].set_xlabel(r"$M$")
axs[0,0].set_ylabel(r"Simulations")

axs[0,1].hist(P['q'], bins, histtype ='step',stacked = True,fill = False, label=r"$q$")
axs[0,1].set_xlabel(r"$q$")
axs[0,1].set_ylabel(r"Simulations")

axs[1,0].hist(P['chiAz'], bins, histtype ='step',stacked = True,fill = False, label=r"$\chi^A_2$")
axs[1,0].hist(P['chiBz'], bins, histtype ='step',stacked = True,fill = False, label=r"$\chi^B_2$")
axs[1,0].set_xlabel(r"$\chi_2$")
axs[1,0].set_ylabel(r"Simulations")
axs[1,0].legend()

axs[1,1].hist(P['lamA2'], bins, histtype ='step',stacked = True,fill = False, label=r"$\Lambda^A_2$")
axs[1,1].hist(P['lamB2'], bins, histtype ='step',stacked = True,fill = False, label=r"$\Lambda^B_2$")
axs[1,1].set_xlabel(r"$\Lambda_2$")
axs[1,1].set_ylabel(r"Simulations")
axs[1,1].legend()

plt.savefig('simple_hists.pdf')
plt.show()  '''

#####
# master stats
#####
micro_eos = [float(hash(s) % 256) / 256 for s in ['BLh','DD2','LS220','SFHo','SLy4','BLQ','BL']]  
micro_c = plt.cm.Pastel1(micro_eos)

tt = 10
x = P['name']
c = [float(hash(s) % 256) / 256 for s in P['eos']]  
#colors = plt.cm.tab20(c)
eos_lisst = list(dict.fromkeys(P['eos']))
dict_eos = {eos:i for i,eos in enumerate(eos_lisst)}

colors = plt.get_cmap('tab20b')(np.arange(len(dict_eos.keys())))
#colorarray=[[0,47,121],[0,112,255],[0,169,255],[0,232,255],[0,250,146],'lighseagreen',[70,221,0],[125,248,3],[186,255,59],[236,255,15],'gold',[255,104,4],[255,0,106],[255,0,194],[182,74,248],[211,103,242],[244,187,246]]
colordict = {
    '2B' : '#002F79',#[0,47,121],
    '2H' : '#0070FF',#[0,112,255],
    'ALF2': 'mediumslateblue',#[0,169,255],
    'ENG': '#00E8FF',#[0,232,255],
    'G2': '#00FA92',#[0,250,146],
    'G2k123': 'lightseagreen',
    'H4': '#46DD00',#[70,221,0],
    'MPA1': 'yellowgreen',#[125,248,3],
    'MS1': 'seagreen',#[186,255,59],
    'MS1b': 'gold',#[236,255,15],
    'SLy': 'orange',
    'BHBlp': '#FF6804',#[255,104,4],
    'DD2': '#FF006A',#[255,0,106],
    'LS220': 'hotpink',#[255,0,194],
    'SFHo': 'darkmagenta',#[182,74,248],
    'BLh': 'orchid',#[211,103,242],
    'BLQ': 'crimson',#[244,187,246],
    'SLy4': 'pink'
}


markero = markers.MarkerStyle(marker='o', fillstyle='none')

plt.figure(figsize=(28,19))
plt.subplots_adjust(bottom=0.14,hspace=0,left=0.07,right=0.99)

ax1 = plt.subplot(511)
k=0
for i_eos, eos in enumerate(P['name']):
    if i_eos == k:
        plt.axvline(x = P.at[i_eos,'name'], color ='gray', alpha=0.3,linewidth=0.2)
        k = k + 10
    else:
        plt.axvline(x = P.at[i_eos,'name'], color ='white', alpha=0.,linewidth=0.2)
plt.axhline(y = 3.0, color ='gray', alpha=0.3,linewidth=0.2)
plt.axhline(y = 2.5, color ='gray', alpha=0.3,linewidth=0.2)
#plt.axhline(y = 10*, color ='gray', alpha=0.5,linewidth=0.2)
ax1.xaxis.set_major_locator(MultipleLocator(6))
ax1.xaxis.set_minor_locator(MultipleLocator(1))
#plt.grid(alpha=0.4)
eos_list = []
for i_eos, eos in enumerate(P['eos']):
    if eos == "sly" or eos=="SLY":
        eos="SLy"
    if eos == "MS1B":
        eos="MS1b"
    eos_color=eos
    if eos == "BHBlp":
        eos=r'BHB$\Lambda\phi$'
    if eos in eos_list:
        #plt.scatter(P.at[i_eos,'name'],P.at[i_eos,'mass'], color=colors[i_eos], marker=".")
        plt.scatter(P.at[i_eos,'name'],P.at[i_eos,'mass'], c=colordict[eos_color], marker=".") #color=colors[dict_eos[eos]]
    else:
        #plt.scatter(P.at[i_eos,'name'],P.at[i_eos,'mass'], color=colors[i_eos], marker=".", label=eos)
        plt.scatter(P.at[i_eos,'name'],P.at[i_eos,'mass'], c=colordict[eos_color], marker=".", label=eos)
        eos_list.append(eos)

plt.ylabel(r'$\rm{M}$')
plt.xlim(-1,len(x))
plt.tick_params('x', labelbottom=False)
plt.legend(bbox_to_anchor=(0, 1, 1, 0), loc="lower left", mode="expand", ncol=9, markerscale=2.5, frameon='False', fancybox='False', edgecolor='white')
#cbar=plt.colorbar(cm.ScalarMappable(norm=None, cmap=cmap),location='top',orientation='horizontal', fraction=0.07,label=r'$EOS$')

ax2 = plt.subplot(512)
plt.axhline(y = 2.0, color ='gray', alpha=0.3,linewidth=0.2)
plt.axhline(y = 1.5, color ='gray', alpha=0.3,linewidth=0.2)
plt.axhline(y = 1.0, color ='gray', alpha=0.3,linewidth=0.2)
k=0
for i_eos, eos in enumerate(P['eos']):
    if i_eos == k:
        plt.axvline(x = P.at[i_eos,'name'], color ='gray', alpha=0.3,linewidth=0.2)
        k = k + 10
    else:
        plt.axvline(x = P.at[i_eos,'name'], color ='white', alpha=0.,linewidth=0.2)
    plt.scatter(P.at[i_eos,'name'],P.at[i_eos,'q'], c=colordict[eos], marker=".")
ax2.xaxis.set_major_locator(MultipleLocator(tt))
ax2.xaxis.set_minor_locator(MultipleLocator(1))
#plt.scatter(x,P['q'], color=colors, marker=".")
plt.ylabel(r'$q$')
plt.ylim(0.95,2.25)
plt.xlim(-1,len(x))
plt.tick_params('x', labelbottom=False)

ax3 = plt.subplot(513)
plt.axhline(y = 0.25, color ='gray', alpha=0.3,linewidth=0.2)
plt.axhline(y = 0.0, color ='gray', alpha=0.3,linewidth=0.2)
plt.axhline(y = -0.25, color ='gray', alpha=0.3,linewidth=0.2)
k = 0
for i_eos, eos in enumerate(P['eos']):
    if i_eos == k:
        plt.axvline(x = P.at[i_eos,'name'], color ='gray', alpha=0.3,linewidth=0.2)
        k = k + 10
    else:
        plt.axvline(x = P.at[i_eos,'name'], color ='white', alpha=0.,linewidth=0.2)
    plt.scatter(P.at[i_eos,'name'],P.at[i_eos,'chiAz'], edgecolors=colordict[eos], marker='o',facecolors="white",label=r'$\chi^A_z$')
    plt.scatter(P.at[i_eos,'name'],P.at[i_eos,'chiBz'], c=colordict[eos], marker='x',label=r'$\chi^B_z$')
ax3.xaxis.set_major_locator(MultipleLocator(tt))
ax3.xaxis.set_minor_locator(MultipleLocator(1))
#plt.scatter(x,P['chiAz'], color=colors,marker=markero,label=r'$\chi^A_z$')
#plt.scatter(x,P['chiBz'], color=colors,marker='x',label=r'$\chi^B_z$')
plt.ylabel(r'$\chi_z$')
plt.ylim(-0.40,0.48)
plt.xlim(-1,len(x))
plt.tick_params('x', labelbottom=False)
handles, labels = ax3.get_legend_handles_labels()
leg =plt.legend(handles=[handles[0],handles[1]],ncol=2, loc='upper right')
leg.legendHandles[0].set_edgecolor('black')
leg.legendHandles[1].set_color('black')

ax4 = plt.subplot(514)
plt.axhline(y = 10**4, color ='gray', alpha=0.3,linewidth=0.2)
plt.axhline(y = 10**3, color ='gray', alpha=0.3,linewidth=0.2)
plt.axhline(y = 10**2, color ='gray', alpha=0.3,linewidth=0.2)
k = 0
for i_eos, eos in enumerate(P['eos']):
    if i_eos == k:
        plt.axvline(x = P.at[i_eos,'name'], color ='gray', alpha=0.3,linewidth=0.2)
        k = k + 10
    else:
        plt.axvline(x = P.at[i_eos,'name'], color ='white', alpha=0.,linewidth=0.2)
    plt.scatter(P.at[i_eos,'name'],P.at[i_eos,'lamA2'], edgecolors=colordict[eos], marker='o',facecolors="white",label=r'$\Lambda^A_2$')
    plt.scatter(P.at[i_eos,'name'],P.at[i_eos,'lamB2'], c=colordict[eos], marker='x',label=r'$\Lambda^B_2$')
ax4.xaxis.set_major_locator(MultipleLocator(tt))
ax4.xaxis.set_minor_locator(MultipleLocator(1))
#plt.scatter(x,P['lamA2'], color=colors,marker=markero,label=r'$\Lambda^A_2$')
#plt.scatter(x,P['lamB2'], color=colors,marker='x',label=r'$\Lambda^B_2$')
plt.ylabel(r'$\Lambda_2$')
plt.xlim(-1,len(x))
plt.ylim(11,4e5)
ax4.set_yscale('log')
plt.tick_params('x', labelbottom=False)
handles, labels = ax4.get_legend_handles_labels()
leg =plt.legend(handles=[handles[0],handles[1]],ncol=2, loc='upper right')
leg.legendHandles[0].set_edgecolor('black')
leg.legendHandles[1].set_color('black')

ax5 = plt.subplot(515)
plt.axhline(y = 0.1, color ='gray', alpha=0.3,linewidth=0.2)
plt.axhline(y = 0.2, color ='gray', alpha=0.3,linewidth=0.2)
plt.axhline(y = 0.3, color ='gray', alpha=0.3,linewidth=0.2)
k = 0
num_res = 0
for i_eos, eos in enumerate(P['eos']):
    if i_eos == k:
        plt.axvline(x = P.at[i_eos,'name'], color ='gray', alpha=0.3,linewidth=0.2)
        k = k + 10
    else:
        plt.axvline(x = P.at[i_eos,'name'], color ='white', alpha=0.,linewidth=0.2)
    resolutions = P.at[i_eos,'resolution'].split(' ')
    for rr in resolutions:
        num_res = num_res + 1
        if rr=='':
            continue
        else:
            #plt.scatter(P.at[i_eos,'name'],float(rr), color=colors[i_eos], marker=".")
            plt.scatter(P.at[i_eos,'name'],float(rr), c=colordict[eos], marker=".")
ax5.xaxis.set_major_locator(MultipleLocator(tt))
ax5.xaxis.set_minor_locator(MultipleLocator(1))
ax5.tick_params(which='major', length=4, color='k', rotation=75 )
ax5.tick_params(which='minor', length=1.5, color='k')
plt.ylabel(r'$\Delta$') 
plt.xlim(-1,len(x))
#ax1.grid(alpha=0.4)
#ax2.grid(alpha=0.4)
#ax3.grid(alpha=0.4)
#ax4.grid(alpha=0.4)
#ax5.grid(alpha=0.4)

#plt.xticks(x[::4], rotation=75)
#plt.tight_layout()
plt.savefig("general_stats.pdf")
plt.show() 

print(num_res)
