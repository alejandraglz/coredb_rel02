from cmath import nan
from re import M, X
import string
from turtle import color, ht
import numpy as np
import matplotlib.pyplot as plt
from matplotlib import cm, markers
from matplotlib.ticker import (MultipleLocator, AutoMinorLocator)
import array
import matplotlib
import pandas as pd
import csv
from watpy.utils.ioutils import *
import scipy.stats as st
import mplcursors
import os
from scipy.stats import t
from bajes.obs.gw.utils import lambda_2_kappa

import sys
sys.path.append('../')
import csv
from handy_functions import *
from bns_fits import *

matplotlib.rcParams['text.usetex']= True
matplotlib.rcParams['font.serif']= 'Times New Roman'
matplotlib.rcParams['font.size']= 15 #28

##############################
# DATA
##############################

data = csv_reader("../data/database_old.csv")

P = get_df(data) #ready to go pandas df
#P.index = np.arange(1, len(P)+1)

## Add info about every run as a list
json_file = "../breschi_fits/results/fits_results.json"
jdict_list = read_json_into_dict(json_file)

reso = []
f2p = []
PC = []
for ip, pp in enumerate(P['name']):
    ss = []#""
    ff = []
    ppc = []
    for dict in jdict_list:
        if not dict==None:
            if dict['database_key'].startswith(pp):
                ss.append(float(dict["resolution"])) #= ss + str(dict["resolution"]) + ' '
                if 'f_pm' in dict.keys():
                    ff.append(float(dict['f_pm'])/float(dict['mtot']))
                    ppc.append(dict['PC'])
                else:
                    ff.append(nan)
                    ppc.append('true')
    reso.append(ss)  
    f2p.append(ff)
    PC.append(ppc)

reso = pd.Series(reso)
reso = reso.to_frame()
reso.columns=['resolution']
f2p = pd.Series(f2p)
f2p = f2p.to_frame()
f2p.columns=['f2']
PC = pd.Series(PC)
PC = PC.to_frame()
PC.columns=['PC']

P['resolution'] = reso
P['f2'] = f2p
P['PC'] = PC


f2_hr = []
f2_error = []
PC_hr = []
for ip, pp in enumerate(P['resolution']):
    try:
        highres = np.argmin(pp)
        f2_ = P.at[ip,'f2'][highres]
        pcc = P.at[ip,'PC'][highres]
        f2__ = P.at[ip,'f2'][np.argmax(pp)]
        '''
        if (f2_<0.0513 and f2_>0.0215):
            f2_hr.append(f2_)
            err = np.abs(f2_-f2__)
            if err>0.003:
                f2_error.append(0)
            else:
                f2_error.append(err)
        else:
            f2_hr.append(nan)
            f2_error.append(0)
        '''
        f2_hr.append(f2_)
        err = 0.5*(np.max(P.at[ip,'f2'])-np.min(P.at[ip,'f2']))
        #f2_error.append(err)
        if err>0.003:
            f2_error.append(0)
        else:
            f2_error.append(err)
        PC_hr.append(pcc)
    except ValueError:
        f2_hr.append(nan)
        f2_error.append(0)
        PC_hr.append('True')
    
f2_hr = pd.Series(f2_hr)
f2_hr = f2_hr.to_frame()
f2_hr.columns=['f2_HR']
f2e = pd.Series(f2_error)
f2e = f2e.to_frame()
f2e.columns=['f2_error']
PC_hr = pd.Series(PC_hr)
PC_hr = PC_hr.to_frame()
PC_hr.columns=['PC_hr']

P['f2_HR'] = f2_hr
P['f2_error'] = f2e
P['PC_hr'] = PC_hr

#faulty_wvfs = ['THC:0052','THC:0054','THC:0098','THC:0042','THC:0056','THC:0068','THC:0069','THC:0079']
#no_eos = ['G2','G2k123']
phase_transitions = ['THC:0037','THC:0058','THC:0059','THC:0060','THC:0061','THC:0062','THC:0063','THC:0064','THC:0070','THC:0071','THC:0072','THC:0102','THC:0103','THC:0104','THC:0105','THC:0106','THC:0107','THC:0109','THC:0110']

# correcting eos names
P.loc[P["eos"]=="sly", "eos"] = 'SLy'
P.loc[P["eos"]=="SLY", "eos"] = 'SLy'
P.loc[P["eos"]=="MS1B", "eos"] = 'MS1b'
P.loc[P["name"].isin(phase_transitions), "PC_hr"] = 'True'
P.loc[P["name"].isin(['BAM:0047','THC:0024','THC:0091','THC:0092','THC:0097']), "PC_hr"] = 'True' #actually PC, oscillate shortly and collapse

# filtering data
P = P.loc[P['name']!='THC:0052'] # faulty sims (already removed in tullio)
P = P.loc[P['name']!='THC:0056'] # delete ..
P = P.loc[P['name']!='THC:0069'] # .. this once you correct the stuff in the json too
#P = P.loc[P['q']==1] 
P = P.loc[P['PC_hr']==False]
P = P.loc[P['eos']!='G2']      # not on matteo's paper
P = P.loc[P['eos']!='G2k123']  # ''
#P = P.loc[P['eos']!='DD2']     # ''
P = P.loc[P['eos']!='SLy4']
P = P.loc[P['kap2t']>65]
#P = P.loc[P['chiAz']==0]
#P = P.loc[P['chiBz']==0]

print(P)
PP=P.drop(['name','id_code'], axis=1)
PP.to_csv('Mf2_data.csv')
#print(P.loc[[124]])


nu      = np.array(P['q'])/(1+np.array(P['q']))**2
s1z = np.array(P['chiAz'])
s2z = np.array(P['chiBz'])
delta   = np.sqrt(1.0 - 4.0*nu)
m1M     = 0.5*(1.0 + delta)
m2M     = 0.5*(1.0 - delta)
Shat    = m1M*m1M*np.array(P['chiAz']) + m2M*m2M*np.array(P['chiBz'])
Xnu     = 1. - 4.*nu

lambda_ax       = np.linspace(0,1200, 5000)
#prediction      = predict_fit([lambda_ax,0.,0.])
#prediction_qqqq = predict_fit([lambda_ax,0.,1-4*0.2222222])
#prediction_mchi = predict_fit([lambda_ax,+0.1,0.])
#prediction_pchi = predict_fit([lambda_ax,-0.1,0.])

prediction      = Mf2_fits([lambda_ax,0.,0.])
prediction_qqqq = Mf2_fits([lambda_ax,0.,1-4*0.2222222])
prediction_mchi = Mf2_fits([lambda_ax,+0.1,0.])
prediction_pchi = Mf2_fits([lambda_ax,-0.1,0.])

nn = np.linspace(0,0.25,1000)
kk = np.linspace(0,500,1000)
m1 = np.linspace(1,3,1000)
m2 = np.linspace(1,3,1000)
M = np.linspace(2,6,1000)
chi1 = np.linspace(-0.9,0.9,1000)
chi2 = np.linspace(-0.9,0.9,100)

################################
# PLOTTING
################################

name_fit = np.array(P['name'])
f2_fit = np.array(P['f2_HR'])
k2t_fit = np.array(P['kap2t'])
q = np.array(P['q'])
#m1_fit = []
#m2_fit = []
#nu_fit = []
eos_fit = np.array(P['eos'])
ferror_fit = np.array(P['f2_error'])
pc_fit = np.array(P['PC_hr'])


# find indeces
iq1 = np.where((Xnu==0)&(s1z==0)&(s2z==0)) # equal mass non spinning
inq = np.where((Xnu!=0)&(s1z==0)&(s2z==0)) # unequal mass non spinning
iss = np.where((s1z!=0)&(s2z!=0)) # spinning

fit_data = Mf2_fits([k2t_fit[iq1],Shat[iq1],Xnu[iq1]])
rel_res = (f2_fit[iq1] - fit_data)/fit_data
k22 = k2t_fit[iq1]
fff = ferror_fit[iq1]
resx = rel_res
up = np.percentile(resx, 95)
lo = np.percentile(resx, 5)

print(up,lo)


####################
# QUR of type f2(R)
####################
m1_fit = np.array(P['m1'])
m2_fit = np.array(P['m2'])

eos_dir = '../../tov/Sequences/Stable'
avail_eos = [eos.split('_')[0] for eos in os.listdir(eos_dir) if 'sequence' in eos]

r_14 = []
r_18 = []
for i,eos in enumerate(eos_fit):
    if eos in avail_eos:
        mtot = m1_fit[i] + m2_fit[i]
        m_eos, r_eos = np.genfromtxt(eos_dir+'/{}'.format(eos)+'_sequence.txt', usecols=[1,3], unpack=True)
        #r1         = np.interp(m1_fit, m_eos, r_eos)
        #r2         = np.interp(m2_fit, m_eos, r_eos)
        one = np.where((m_eos>1.37) & (m_eos<1.43))
        two = np.where((m_eos>1.77) & (m_eos<1.83))
        #print(two)
        r14 = r_eos[ one[0][0] ]
        r18 = r_eos[ two[0][0] ]
        r_14.append(r14/mtot)   # R/M
        r_18.append(r18/mtot)
    else:
        r_14.append(nan)
        r_18.append(nan)

############## FITS

##### f2(Rx)
def f2Rx(a,Rx):
    final = []
    for r in Rx:
        final.append(a[0] + a[1]*r + a[2]*r**2)
    return final

##### f2(Rx,R1.4/R1.8)
def f2RxRx(a,Rx,R48):
    final = []
    for i,r in enumerate(Rx):
        final.append(a[0] + a[1]*r + a[2]*r**2 + a[3]*R48[i])
    return final

#a = [0.2,-0.0762,0.0078] #R_1.4
a = [ 0.24195131, -0.10057161,  0.01134283] #fits
#b = [0.236,-0.103,0.0125] #R_1.8
b = [ 0.23655214 ,-0.10205601,  0.01218261] #fits
#c = [0.162,-0.115,0.0145,0.0919] #R_1.4
c = [ 0.15264525, -0.11179275,  0.01385721,  0.09765477] #fits
#d = [0.213,-0.105,0.013,0.0241] #R_1.8
d = [ 0.20412987, -0.10090511,  0.01223752 , 0.02776716] #fits

ratio = np.array(r_14) / np.array(r_18)

x = np.linspace(0.02,0.055,1000)


import matplotlib.cm as cm
norm = matplotlib.colors.Normalize(vmin=min(ratio), vmax=max(ratio), clip=True)
mapper = cm.ScalarMappable(norm=norm, cmap='viridis')
color = np.array([(mapper.to_rgba(v)) for v in ratio])


fig, axs = plt.subplots(2, 2, figsize=(10,6))

####### 1st panel

# confidence intervals
residuals  = f2_fit - np.array(f2Rx(a,r_14))
residuals = residuals[np.logical_not(np.isnan(residuals))]
up = np.percentile(residuals,95)
lo = np.percentile(residuals,5)

axs[0,0].plot(x,x,'k-')
axs[0,0].fill_between(x,x+up,x+lo,color='gray', alpha=0.2, label=r"$90\%$ $\rm{CI}$")
cheese=axs[0,0].scatter(f2Rx(a,r_14),f2_fit, c=ratio, cmap='viridis', s=10.5, label=r"$\rm{CoRe}$ $\rm{DB}$")
for i,co in enumerate(color):
    axs[0,0].errorbar(f2Rx(a,r_14)[i],f2_fit[i],yerr=ferror_fit[i], c=co, fmt='none')
axs[0,0].set_xlabel(r"$Mf^{\rm{fit}}_2(R_{1.4}/M)$")
axs[0,0].set_ylabel(r"$Mf^{\rm{NR}}_2(R_{1.4}/M)$")
axs[0,0].set_xlim([0.02,0.055])
axs[0,0].set_ylim([0.02,0.055])
leg =axs[0,0].legend(ncol=1, frameon='False', fancybox='False', edgecolor='white', fontsize='small')
leg.legendHandles[1].set_color('black')

####### 2nd panel

# confidence intervals
residuals  = f2_fit - np.array(f2Rx(b,r_18))
residuals = residuals[np.logical_not(np.isnan(residuals))]
up = np.percentile(residuals,95)
lo = np.percentile(residuals,5)
axs[0,1].fill_between(x,x+up,x+lo,color='gray', alpha=0.2)
axs[0,1].plot(x,x,'k-')
cheese=axs[0,1].scatter(f2Rx(b,r_18),f2_fit, c=ratio, cmap='viridis', s=10.5)
for i,co in enumerate(color):
    axs[0,1].errorbar(f2Rx(b,r_18)[i],f2_fit[i],yerr=ferror_fit[i], c=co, fmt='none')
axs[0,1].set_xlabel(r"$Mf^{\rm{fit}}_2(R_{1.8}/M)$")
axs[0,1].set_ylabel(r"$Mf^{\rm{NR}}_2(R_{1.8}/M)$")
axs[0,1].set_xlim([0.02,0.055])
axs[0,1].set_ylim([0.02,0.055])

####### 3rd panel

# confidence intervals
residuals  = f2_fit - np.array(f2RxRx(c,r_14,ratio))
residuals = residuals[np.logical_not(np.isnan(residuals))]
up = np.percentile(residuals,95)
lo = np.percentile(residuals,5)
axs[1,0].fill_between(x,x+up,x+lo,color='gray', alpha=0.2)
axs[1,0].plot(x,x,'k-')
cheese=axs[1,0].scatter(f2RxRx(c,r_14,ratio),f2_fit, c=ratio, cmap='viridis', s=10.5)
for i,co in enumerate(color):
    axs[1,0].errorbar(f2RxRx(c,r_14,ratio)[i],f2_fit[i],yerr=ferror_fit[i], c=co, fmt='none')
axs[1,0].set_xlabel(r"$Mf^{\rm{fit}}_2(R_{1.4}/M,R_{1.4}/R_{1.8})$")
axs[1,0].set_ylabel(r"$Mf^{\rm{NR}}_2(R_{1.4}/M,R_{1.4}/R_{1.8})$")
axs[1,0].set_xlim([0.02,0.055])
axs[1,0].set_ylim([0.02,0.055])

####### 4th panel

# confidence intervals
residuals  = f2_fit - np.array(f2RxRx(d,r_18,ratio))
residuals = residuals[np.logical_not(np.isnan(residuals))]
up = np.percentile(residuals,95)
lo = np.percentile(residuals,5)
axs[1,1].fill_between(x,x+up,x+lo,color='gray', alpha=0.2)
axs[1,1].plot(x,x,'k-')
cheese=axs[1,1].scatter(f2RxRx(d,r_18,ratio),f2_fit, c=ratio, cmap='viridis', s=10.5)
for i,co in enumerate(color):
    axs[1,1].errorbar(f2RxRx(d,r_18,ratio)[i],f2_fit[i],yerr=ferror_fit[i], c=co, fmt='none')
axs[1,1].set_xlabel(r"$Mf^{\rm{fit}}_2(R_{1.8}/M,R_{1.4}/R_{1.8})$")
axs[1,1].set_ylabel(r"$Mf^{\rm{NR}}_2(R_{1.8}/M,R_{1.4}/R_{1.8})$")
axs[1,1].set_xlim([0.02,0.055])
axs[1,1].set_ylim([0.02,0.055])

cbar = plt.colorbar(cheese, ax=[axs[0,1],axs[1,1]], anchor=(1.84, 0.5))
cbar.set_label(r'$R_{1.4}/R_{1.8}$')
plt.subplots_adjust(left=0.1,right=0.87, wspace=0.3, hspace=0.4)
##plt.tight_layout()
plt.savefig('f2_R_fits.pdf')
plt.show()

####################
# choose target = amrg, fmrg, lpeak, erad_m
####################
from scipy.optimize import curve_fit
target = f2_fit
'''
##### f2(Rx)
def f2Rx_fit(r,a0,a1,a2):
    return a0 + a1*r + a2*r**2

vars            = r_18#[k2t_fit[iss],Shat[iss],Xnu[iss]]
popt_iss, pcov_iss   = curve_fit(f2Rx_fit, vars, target, maxfev=500000,p0=[0.236, -0.103, 0.0125],bounds=(-1,1))
a0, a1, a2 = popt_iss
print(popt_iss)

fit_data = f2Rx([a0,a1,a2],r_18)
rel_res = (target - fit_data)/fit_data
res = np.sum(rel_res**2)
sigma = np.std(rel_res)

print("Sum res. sq. = ", res)
print("Rel. st. dev. = ", sigma)

resx = rel_res
up = np.percentile(resx, 95)
lo = np.percentile(resx, 5)

print(up,lo)

data_average = np.mean(target)
SStot = np.sum((target-data_average)**2)
SSres = np.sum((target-fit_data)**2)
R_square = 1 - SSres/SStot
print( '\nRsquare:\n', R_square, '\n')
'''
##############################
##### f2(Rx,R1.4/R1.8)
def f2RxRx_fit(R, a0,a1,a2,a3):
    r,ratio = R
    return a0 + a1*r + a2*r**2 + a3*ratio

vars            = [r_18,ratio]
popt_iss, pcov_iss   = curve_fit(f2RxRx_fit, vars, target, maxfev=500000,p0=[0.213,-0.105,0.013,0.0241],bounds=(-1,1))
a0, a1, a2, a3 = popt_iss
print(popt_iss)

fit_data = f2RxRx([a0,a1,a2,a3],r_18, ratio)
rel_res = (target - fit_data)/fit_data
res = np.sum(rel_res**2)
sigma = np.std(rel_res)

print("Sum res. sq. = ", res)
print("Rel. st. dev. = ", sigma)

resx = rel_res
up = np.percentile(resx, 95)
lo = np.percentile(resx, 5)

print(up,lo)

data_average = np.mean(target)
SStot = np.sum((target-data_average)**2)
SSres = np.sum((target-fit_data)**2)
R_square = 1 - SSres/SStot
print( '\nRsquare:\n', R_square, '\n')