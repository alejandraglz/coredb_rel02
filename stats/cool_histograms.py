from cmath import nan
from re import M
import string
from tkinter import E
from turtle import color
import numpy as np
import matplotlib.pyplot as plt
from matplotlib import cm, markers
from matplotlib.colors import ListedColormap, LinearSegmentedColormap
from matplotlib.ticker import (MultipleLocator, AutoMinorLocator)
import array
import matplotlib
import pandas as pd
import csv
from watpy.utils.ioutils import *
from bajes.obs.gw.utils import compute_chi_eff, compute_lambda_tilde
import matplotlib.patches as mpatches

import sys
sys.path.append('../')
import csv
from handy_functions import *

matplotlib.rcParams['text.usetex']= True
#matplotlib.rcParams['font.family'] = ['serif']
matplotlib.rcParams['font.serif']= 'Times New Roman'
matplotlib.rcParams['font.size']= 15 #28

##############################
# DATA
##############################

data = csv_reader("../data/database.csv")

P = get_df(data) #ready to go pandas df
#P.index = np.arange(1, len(P)+1)

## Add info about every run as a list
json_file = "../data/data.json"
jdict_list = read_json_into_dict(json_file)

reso = []
neut = []
orbs = []
for ip, pp in enumerate(P['name']):
    ss = []
    nn = []
    oo = []
    for dict in jdict_list:
        if not dict==None:
            if dict['database_key'].startswith(pp):
                ss.append(float(dict["resolution"]))
                nn.append(dict["neutrino_scheme"])
                try:
                    oo.append(float(dict["number_of_orbits"]))
                except TypeError:
                    oo.append(nan)
    reso.append(ss)  
    neut.append(nn)
    orbs.append(oo)

reso = pd.Series(reso)
reso = reso.to_frame()
reso.columns=['resolution']
neut = pd.Series(neut)
neut = neut.to_frame()
neut.columns=['neutrino_scheme']
orbs = pd.Series(orbs)
orbs = orbs.to_frame()
orbs.columns=['number_of_orbits']

P['resolution'] = reso
P['neutrino_scheme'] = neut
P['number_of_orbits'] = orbs


## correct eos
for i,ee in enumerate(P['eos']):
    if ee == "sly" or ee=="SLY":
        P.at[i,'eos']="SLy"
    elif ee == "MS1B":
        P.at[i,'eos']="MS1b"
    if P.at[i,'id_code']=='Sgrid':
        P.at[i,'id_code']='SGRID'

print(P.loc[P['ecc']<0.0011])

color_code = {
    'LORENE': 'tab:olive',
    'LORENE_border': 'olive',
    'SGRID': 'tab:cyan',
    'SGRID_border': 'darkcyan',
    'BAM': 'skyblue',
    'BAM_border': 'dodgerblue',
    'THC': 'mediumslateblue',
    'THC_border': 'darkslateblue',
    'general': 'orange',
    'general_border': '#FF6804',
    'general1': 'lightgray',
    'general1_border': 'gray',
    '': '#00A9FF',
    'None': '#00A9FF',
    'M0': '#00FA92',
    'Leakage': '#B64AF8'
}
################################
# PLOTTING
################################

####
# lambda_tilde, chi_eff, q, ecc (all together in histograms)
###
fig, axs = plt.subplots(2, 2,figsize=(7,7))
plt.subplots_adjust(wspace=0.07,hspace=0.3)
bins= [20,20,20,20]
vars = ['q','chieff','lamT','ecc']
labels = [r'$q$',r'$\chi_{\rm{eff}}$',r'$\log_{10}(\tilde{\Lambda})$',r"$\log_{10}(e)$"]
for i,vv in enumerate(vars):
    if vv=='ecc':
        P.loc[P['ecc']==0,'ecc'] = 1e-5
        #axs[1,1].set_xscale('log')
        eccc = np.array(P['ecc'])
        var = np.log10(eccc) 
    elif vv=='lamT':
        #axs[1,0].set_xscale('log')
        lamtt = np.array(P['lamT'])
        var = np.log10(lamtt) 
    else:
        var = P[vv]
    if i<2:
        axs[0,i].hist(var,bins=bins[i],color=color_code['THC'], edgecolor=color_code['THC_border'])
        axs[0,i].set_xlabel(labels[i])
        if i==0:
            axs[0,i].set_ylabel(r'Simulations')
        else:
            axs[0,i].sharey(axs[0,0])
            axs[0,i].tick_params(axis='y',labelleft=False, left=True)
    else:
        axs[1,i-2].hist(var,bins=bins[i],color=color_code['THC'], edgecolor=color_code['THC_border'])
        axs[1,i-2].set_xlabel(labels[i])
        if (i-2)==0:
            axs[1,i-2].set_ylabel(r'Simulations')
        else:
            axs[1,i-2].sharey(axs[1,0])
            axs[1,i-2].tick_params(axis='y',labelleft=False, left=True)
##plt.tight_layout()
plt.savefig('multi-hists_logA.pdf')
plt.show()


quit()
####
# lambda_tilde, chi_eff and q
###

## only histograms
fig, axs = plt.subplots(1, 3,figsize=(12,4))
bins=15
vars = ['q','chieff','lamT']
labels = [r'$q$',r'$\chi_{\rm{eff}}$',r'$\tilde{\Lambda}$']
axs[0].set_ylabel(r'Num. of Sim. Configurations')
for i,vv in enumerate(vars):
    if vv=='chieff':
        axs[i].hist(P[vv],bins=10,color=color_code['THC'], edgecolor=color_code['THC_border'])
    else:
        axs[i].hist(P[vv],bins=bins,color=color_code['THC'], edgecolor=color_code['THC_border'])
    axs[i].set_xlabel(labels[i])
plt.tight_layout()
plt.savefig('q-chi-lamt-hists.pdf')
plt.show()


## correlation plot
fig, axs = plt.subplots(2, 2,figsize=(7,7))
bins=50
markk = 'o'
msize = 40
for i in range(0,2):
    for j in range(0,2):
        if i==j:
            if j==1:
                cheese=axs[i,j].scatter(P['chieff'], P['q'],c=P['mass'], cmap='viridis', alpha=0.4, marker=markk, s=msize) # 1,1
                axs[i,j].yaxis.set_tick_params(labelleft=False)
                axs[i,j].set_xlabel(r'$\chi_{\rm{eff}}$')
            elif j==0:
                cheese=axs[i,j].scatter(P['lamT'], P['chieff'],c=P['mass'], cmap='viridis', alpha=0.4, marker=markk, s=msize) # 0,0
                axs[i,j].set_ylabel(r'$\chi_{\rm{eff}}$')
                axs[i,j].xaxis.set_tick_params(labelbottom=False)
        else:
            if i==0 and j==1:
                axs[i,j].set_visible(False) #0,1
            else:
                cheese=axs[i,j].scatter(P['lamT'], P['q'],c=P['mass'], cmap='viridis', alpha=0.4, marker=markk, s=msize) # 1,0
                axs[i,j].set_ylabel(r'$q$')
                axs[i,j].set_xlabel(r'$\tilde{\Lambda}$')
cbar = plt.colorbar(cheese, ax=[axs[0,0],axs[1,0],axs[1,1]], orientation='vertical', location='right', anchor=(1., 0.), fraction=0.1, aspect=35)
cbar.set_label(label=r'$M$',loc='center')
plt.subplots_adjust(right=0.85,wspace=0.1,hspace=0.1)
plt.savefig('lamt-q-chieff.pdf')
#plt.tight_layout()
plt.show()



####
# tidal parameters
###
fig, axs = plt.subplots(3, 3,figsize=(7,7))
bins=50

for i in range(0,3):
    for j in range(0,3):
        if i==j:
            if i==0: # histograms
                axs[i,j].hist(P['lamT'], bins, histtype ='barstacked',stacked = True, color=color_code['general1'], edgecolor=color_code['general1_border']) # 0,0
                axs[i,j].xaxis.set_tick_params(labelbottom=False)
            elif i==1:
                axs[i,j].hist(P['q'], bins, histtype ='barstacked',stacked = True, color=color_code['general1'], edgecolor=color_code['general1_border']) # 1,1
                axs[i,j].xaxis.set_tick_params(labelbottom=False)
            else:
                axs[i,j].hist(P['chieff'], bins, histtype ='barstacked',stacked = True, color=color_code['general1'], edgecolor=color_code['general1_border']) # 2,2
                axs[i,j].set_xlabel(r'$\chi_{\rm{eff}}$')
            axs[i,j].spines['top'].set_visible(False)
            axs[i,j].spines['right'].set_visible(False)
        else:
            if j==2 or (i==0 and j==1):
                axs[i,j].set_visible(False) # 0,1 / 0,2 / 1,2
            else: # correlation plots
                if j==1:
                    cheese=axs[i,j].scatter(P['q'], P['chieff'],c=P['mass'], cmap='viridis', alpha=0.4, marker='.') # 2,1
                    axs[i,j].yaxis.set_tick_params(labelleft=False)
                    axs[i,j].set_xlabel(r'$q$')
                elif i==1:
                    axs[i,j].scatter(P['lamT'], P['q'],c=P['mass'], cmap='viridis', alpha=0.4, marker='.') # 1,0
                    axs[i,j].xaxis.set_tick_params(labelbottom=False)
                    axs[i,j].set_ylabel(r'$q$')
                else:
                    axs[i,j].scatter(P['lamT'], P['chieff'],c=P['mass'], cmap='viridis', alpha=0.4, marker='.') # 2,0
                    axs[i,j].set_xlabel(r'$\tilde{\Lambda}$')
                    axs[i,j].set_ylabel(r'$\chi_{\rm{eff}}$')
cbar = plt.colorbar(cheese, ax=[axs[0,0],axs[0,1],axs[0,2]], orientation='horizontal', location='top', anchor=(0., 40.0), fraction=0.1, aspect=35)
cbar.set_label(label=r'$M$',loc='center')
plt.subplots_adjust(wspace=0.35)
plt.savefig('lamt-lams_hist.pdf')
#plt.tight_layout()
plt.show()
            

#####
# only eccentricity
#####
fig, axs = plt.subplots(1, 1,figsize=(12,5))
bins=40
P.loc[P['ecc']==0,'ecc'] = 1e-5
eccc = np.array(P['ecc'])
print(max(eccc))
eccc = np.log10(eccc)
#L = P.loc[P['id_code']!='SGRID']
#S = P.loc[P['id_code']!='LORENE']
axs.hist(eccc, bins, histtype ='barstacked',stacked = True,color=color_code['SGRID'], edgecolor=color_code['SGRID_border'])
plt.xlabel(r"$\log_{10}(e)$")
plt.ylabel(r'Num. of Sim. Configurations')
#plt.yscale('log')
#plt.legend(ncol=1,loc='upper right', frameon='False', fancybox='False', edgecolor='white')
plt.tight_layout()
plt.savefig('logecc_hist.pdf')

plt.show() 


#####
# q - chi eff
#####

fig, axs = plt.subplots(2, 2,figsize=(7,7))
bins=50

B = P.loc[P['name'].str.contains("BAM")]
T = P.loc[P['name'].str.contains("THC")]
axs[0,0].hist(B['q'], 30, histtype ='barstacked',stacked = True,color=color_code['BAM'], edgecolor=color_code['BAM_border'],label="BAM")
axs[0,0].hist(T['q'], 21, histtype ='barstacked',stacked = True,color=color_code['THC'], edgecolor=color_code['THC_border'],label='THC')
axs[0,0].spines['top'].set_visible(False)
axs[0,0].spines['right'].set_visible(False)
axs[0,0].spines['bottom'].set_visible(True)
axs[0,0].spines['left'].set_visible(True)
axs[0,0].legend(bbox_to_anchor=(1.02, 0.8), loc='upper left', frameon='False', fancybox='False', edgecolor='white')

axs[0,1].set_visible(False)

code_list =[]
for ic,cc in enumerate(P['name']):
    cc = cc.split(':')[0]
    if cc=='BAM':
        col=color_code['BAM_border']
    else:
        col=color_code['THC_border']
    if cc in code_list:
        axs[1,0].scatter(P.at[ic,'q'], P.at[ic,'chieff'], c=color_code[cc], edgecolor=col,alpha=0.5, s=50)
    else:
        axs[1,0].scatter(P.at[ic,'q'], P.at[ic,'chieff'], c=color_code[cc], edgecolor=col, alpha=0.5, s=50)
        code_list.append(cc)
axs[1,0].set_ylabel(r"$\chi_{\rm{eff}}$")
axs[1,0].set_xlabel(r"$q$")
#axs[1,0].legend(bbox_to_anchor=(0, 1, 1, 0), frameon='False', fancybox='False')
#handles, labels = axs[1,0].get_legend_handles_labels()
#leg =plt.legend(handles=[handles[0],handles[1]],ncol=1, loc='upper right')
#leg.legendHandles[0].set_edgecolor('black')
#leg.legendHandles[1].set_color('black')


axs[1,1].set_xlabel(r"$\chi_{\rm{eff}}$")
axs[1,1].hist(B['chieff'], 20, histtype ='barstacked',stacked = True,color=color_code['BAM'], edgecolor=color_code['BAM_border'], label="BAM")
axs[1,1].hist(T['chieff'], 24, histtype ='barstacked',stacked = True,color=color_code['THC'], edgecolor=color_code['THC_border'], label='THC')
axs[1,1].spines['top'].set_visible(False)
axs[1,1].spines['right'].set_visible(False)
axs[1,1].spines['bottom'].set_visible(True)
axs[1,1].spines['left'].set_visible(True)

axs[0,0].text(2.2, 80, "Evolution Code")
#leg =plt.legend(handles=[handles[0],handles[1]],ncol=1,bbox_to_anchor=(0.3, .8),loc='lower left', frameon='False', fancybox='False', edgecolor='white')
plt.savefig('lamt-chieff_hist.pdf')
#plt.tight_layout()
plt.show() 


#####
# resolution - # of orbits
#####

marker_dict = {
    'BAM': 'o',
    'THC': '^'
}

fig, axs = plt.subplots(2, 2,figsize=(7,7))
bins=50

b_res = []
b_orb = []
b_neu = []
t_res = []
t_orb = []
t_neu = []
tot_res = []
tot_orbs = []
for ir, rr in enumerate(P['resolution']):
    for ij, jr in enumerate(rr):
        tot_res.append(jr)
        tot_orbs.append(P.at[ir,'number_of_orbits'][ij])
        if P.at[ir,'name'].startswith("BAM"):
            b_res.append(jr)
            b_orb.append(P.at[ir,'number_of_orbits'][ij])
            b_neu.append(P.at[ir,'neutrino_scheme'][ij])
        else:
            t_res.append(jr)
            t_orb.append(P.at[ir,'number_of_orbits'][ij])
            if P.at[ir,'neutrino_scheme'][ij]=='':
                t_neu.append("None")
            else:
                t_neu.append(P.at[ir,'neutrino_scheme'][ij])

axs[0,0].hist(tot_res, 30, histtype ='barstacked',stacked = True, color=color_code['general'], edgecolor=color_code['general_border'])
axs[0,0].spines['top'].set_visible(False)
axs[0,0].spines['right'].set_visible(False)
axs[0,0].spines['bottom'].set_visible(True)
axs[0,0].spines['left'].set_visible(True)
#axs[0,0].legend(bbox_to_anchor=(1.25, 1), loc='upper left', frameon='False', fancybox='False', edgecolor='white') #1.02,1

axs[0,1].set_visible(False)

axs[1,0].scatter(0.2,10,marker=marker_dict['BAM'], color='white', label=r"BAM")
axs[1,0].scatter(0.2,10,marker=marker_dict['THC'], color='white', label=r'THC')
for ib,bres in enumerate(b_res):
    mar= marker_dict['BAM']
    farbe = color_code['None']
    axs[1,0].scatter(bres, b_orb[ib], c=farbe,alpha=0.5, marker=mar)
marker_list=[]
for ib,bres in enumerate(t_res):
    farbe = color_code[t_neu[ib]]
    maa = marker_dict['THC']
    if t_neu[ib] in marker_list:
        axs[1,0].scatter(bres, t_orb[ib], c=farbe, alpha=0.4, marker=maa)
    else:
        axs[1,0].scatter(bres, t_orb[ib], c=farbe, alpha=0.4, marker=maa)
        marker_list.append(t_neu[ib])
axs[1,0].set_ylabel(r"Num. of Orbits")
axs[1,0].set_xlabel(r"$\Delta x$")
n_patch = mpatches.Patch(color=color_code['None'], label=r'None', alpha=0.4)
l_patch = mpatches.Patch(color=color_code['Leakage'], label=r'Leakage', alpha=0.4)
m_patch = mpatches.Patch(color=color_code['M0'], label=r'M0', alpha=0.4)
handles, labels = axs[1,0].get_legend_handles_labels()
leg = plt.legend(handles=[handles[0],handles[1]],bbox_to_anchor=(0, 1.4),loc='upper left',frameon='False', fancybox='False', edgecolor='white')
leg.legendHandles[0].set_color('black')
leg.legendHandles[1].set_color('black')
leg1 = axs[1,0].legend(handles=[n_patch,l_patch,m_patch],bbox_to_anchor=(1.25, 2),  loc='upper left',frameon='False', fancybox='False', edgecolor='white')
#axs[1,0].legend(bbox_to_anchor=(1., 1.8),  loc='upper left',frameon='False', fancybox='False', edgecolor='white')
#handles, labels = axs[1,0].get_legend_handles_labels()
#leg =plt.legend(handles=[handles[0],handles[1],handles[2]],ncol=1,bbox_to_anchor=(0., 1.8), loc='upper left', frameon='False', fancybox='False', edgecolor='white')
#leg.legendHandles[0].set_hatch('x')
#leg.legendHandles[1].set_hatch('x')
#leg.legendHandles[2].set_hatch('x')


axs[1,1].set_xlabel(r"Num. of Orbits")
axs[1,1].hist(tot_orbs, 20, histtype ='barstacked',stacked = True,color=color_code['general'], edgecolor=color_code['general_border'])
axs[1,1].spines['top'].set_visible(False)
axs[1,1].spines['right'].set_visible(False)
axs[1,1].spines['bottom'].set_visible(True)
axs[1,1].spines['left'].set_visible(True)

plt.text(1.6, 140, "Evolution Code")
plt.text(1.2, 200, "Neutrino Scheme")
#leg =plt.legend(handles=[handles[0],handles[1]],ncol=1,bbox_to_anchor=(0.3, .8),loc='lower left', frameon='False', fancybox='False', edgecolor='white')
plt.savefig('res-orbs_hist.pdf')
#plt.tight_layout()
plt.show() 




quit()
#####
# master stats
#####
micro_eos = [float(hash(s) % 256) / 256 for s in ['BLh','DD2','LS220','SFHo','SLy4','BLQ','BL']]  
micro_c = plt.cm.Pastel1(micro_eos)

tt = 10
x = P['name']
c = [float(hash(s) % 256) / 256 for s in P['eos']]  
#colors = plt.cm.tab20(c)
eos_lisst = list(dict.fromkeys(P['eos']))
dict_eos = {eos:i for i,eos in enumerate(eos_lisst)}

colors = plt.get_cmap('tab20b')(np.arange(len(dict_eos.keys())))
#colorarray=[[0,47,121],[0,112,255],[0,169,255],[0,232,255],[0,250,146],'lighseagreen',[70,221,0],[125,248,3],[186,255,59],[236,255,15],'gold',[255,104,4],[255,0,106],[255,0,194],[182,74,248],[211,103,242],[244,187,246]]
colordict = {
    '2B' : '#002F79',#[0,47,121],
    '2H' : '#0070FF',#[0,112,255],
    'ALF2': 'mediumslateblue',#[0,169,255],
    'ENG': '#00E8FF',#[0,232,255],
    'G2': '#00FA92',#[0,250,146],
    'G2k123': 'lightseagreen',
    'H4': '#46DD00',#[70,221,0],
    'MPA1': 'yellowgreen',#[125,248,3],
    'MS1': 'seagreen',#[186,255,59],
    'MS1b': 'gold',#[236,255,15],
    'SLy': 'orange',
    'BHBlp': '#FF6804',#[255,104,4],
    'DD2': '#FF006A',#[255,0,106],
    'LS220': 'hotpink',#[255,0,194],
    'SFHo': 'darkmagenta',#[182,74,248],
    'BLh': 'orchid',#[211,103,242],
    'BLQ': 'crimson',#[244,187,246],
    'SLy4': 'pink'
}


markero = markers.MarkerStyle(marker='o', fillstyle='none')

plt.figure(figsize=(20,10))
plt.subplots_adjust(hspace=0)

ax1 = plt.subplot(511)
k=0
for i_eos, eos in enumerate(P['name']):
    if i_eos == k:
        plt.axvline(x = P.at[i_eos,'name'], color ='gray', alpha=0.3,linewidth=0.2)
        k = k + 10
    else:
        plt.axvline(x = P.at[i_eos,'name'], color ='white', alpha=0.,linewidth=0.2)
plt.axhline(y = 3.0, color ='gray', alpha=0.3,linewidth=0.2)
plt.axhline(y = 2.5, color ='gray', alpha=0.3,linewidth=0.2)
#plt.axhline(y = 10*, color ='gray', alpha=0.5,linewidth=0.2)
ax1.xaxis.set_major_locator(MultipleLocator(6))
ax1.xaxis.set_minor_locator(MultipleLocator(1))
#plt.grid(alpha=0.4)
eos_list = []
for i_eos, eos in enumerate(P['eos']):
    if eos == "sly" or eos=="SLY":
        eos="SLy"
    if eos == "MS1B":
        eos="MS1b"
    if eos in eos_list:
        #plt.scatter(P.at[i_eos,'name'],P.at[i_eos,'mass'], color=colors[i_eos], marker=".")
        plt.scatter(P.at[i_eos,'name'],P.at[i_eos,'mass'], c=colordict[eos], marker=".") #color=colors[dict_eos[eos]]
    else:
        #plt.scatter(P.at[i_eos,'name'],P.at[i_eos,'mass'], color=colors[i_eos], marker=".", label=eos)
        plt.scatter(P.at[i_eos,'name'],P.at[i_eos,'mass'], c=colordict[eos], marker=".", label=eos)
        eos_list.append(eos)

plt.ylabel(r'$\rm{M}$')
plt.xlim(-1,len(x))
plt.tick_params('x', labelbottom=False)
plt.legend(bbox_to_anchor=(0, 1, 1, 0), loc="lower left", mode="expand", ncol=9, markerscale=2.5, frameon='False', fancybox='False', edgecolor='white')
#cbar=plt.colorbar(cm.ScalarMappable(norm=None, cmap=cmap),location='top',orientation='horizontal', fraction=0.07,label=r'$EOS$')

ax2 = plt.subplot(512)
plt.axhline(y = 2.0, color ='gray', alpha=0.3,linewidth=0.2)
plt.axhline(y = 1.5, color ='gray', alpha=0.3,linewidth=0.2)
plt.axhline(y = 1.0, color ='gray', alpha=0.3,linewidth=0.2)
k=0
for i_eos, eos in enumerate(P['eos']):
    if i_eos == k:
        plt.axvline(x = P.at[i_eos,'name'], color ='gray', alpha=0.3,linewidth=0.2)
        k = k + 10
    else:
        plt.axvline(x = P.at[i_eos,'name'], color ='white', alpha=0.,linewidth=0.2)
    plt.scatter(P.at[i_eos,'name'],P.at[i_eos,'q'], c=colordict[eos], marker=".")
ax2.xaxis.set_major_locator(MultipleLocator(tt))
ax2.xaxis.set_minor_locator(MultipleLocator(1))
#plt.scatter(x,P['q'], color=colors, marker=".")
plt.ylabel(r'$q$')
plt.ylim(0.95,2.25)
plt.xlim(-1,len(x))
plt.tick_params('x', labelbottom=False)

ax3 = plt.subplot(513)
plt.axhline(y = 0.25, color ='gray', alpha=0.3,linewidth=0.2)
plt.axhline(y = 0.0, color ='gray', alpha=0.3,linewidth=0.2)
plt.axhline(y = -0.25, color ='gray', alpha=0.3,linewidth=0.2)
k = 0
for i_eos, eos in enumerate(P['eos']):
    if i_eos == k:
        plt.axvline(x = P.at[i_eos,'name'], color ='gray', alpha=0.3,linewidth=0.2)
        k = k + 10
    else:
        plt.axvline(x = P.at[i_eos,'name'], color ='white', alpha=0.,linewidth=0.2)
    plt.scatter(P.at[i_eos,'name'],P.at[i_eos,'chiAz'], edgecolors=colordict[eos], marker='o',facecolors="white",label=r'$\chi^A_z$')
    plt.scatter(P.at[i_eos,'name'],P.at[i_eos,'chiBz'], c=colordict[eos], marker='x',label=r'$\chi^B_z$')
ax3.xaxis.set_major_locator(MultipleLocator(tt))
ax3.xaxis.set_minor_locator(MultipleLocator(1))
#plt.scatter(x,P['chiAz'], color=colors,marker=markero,label=r'$\chi^A_z$')
#plt.scatter(x,P['chiBz'], color=colors,marker='x',label=r'$\chi^B_z$')
plt.ylabel(r'$\chi_z$')
plt.ylim(-0.40,0.48)
plt.xlim(-1,len(x))
plt.tick_params('x', labelbottom=False)
handles, labels = ax3.get_legend_handles_labels()
leg =plt.legend(handles=[handles[0],handles[1]],ncol=2, loc='upper right')
leg.legendHandles[0].set_edgecolor('black')
leg.legendHandles[1].set_color('black')

ax4 = plt.subplot(514)
plt.axhline(y = 10**4, color ='gray', alpha=0.3,linewidth=0.2)
plt.axhline(y = 10**3, color ='gray', alpha=0.3,linewidth=0.2)
plt.axhline(y = 10**2, color ='gray', alpha=0.3,linewidth=0.2)
k = 0
for i_eos, eos in enumerate(P['eos']):
    if i_eos == k:
        plt.axvline(x = P.at[i_eos,'name'], color ='gray', alpha=0.3,linewidth=0.2)
        k = k + 10
    else:
        plt.axvline(x = P.at[i_eos,'name'], color ='white', alpha=0.,linewidth=0.2)
    plt.scatter(P.at[i_eos,'name'],P.at[i_eos,'lamA2'], edgecolors=colordict[eos], marker='o',facecolors="white",label=r'$\Lambda^A_2$')
    plt.scatter(P.at[i_eos,'name'],P.at[i_eos,'lamB2'], c=colordict[eos], marker='x',label=r'$\Lambda^B_2$')
ax4.xaxis.set_major_locator(MultipleLocator(tt))
ax4.xaxis.set_minor_locator(MultipleLocator(1))
#plt.scatter(x,P['lamA2'], color=colors,marker=markero,label=r'$\Lambda^A_2$')
#plt.scatter(x,P['lamB2'], color=colors,marker='x',label=r'$\Lambda^B_2$')
plt.ylabel(r'$\Lambda_2$')
plt.xlim(-1,len(x))
ax4.set_yscale('log')
plt.tick_params('x', labelbottom=False)
handles, labels = ax4.get_legend_handles_labels()
leg =plt.legend(handles=[handles[0],handles[1]],ncol=2, loc='upper right')
leg.legendHandles[0].set_edgecolor('black')
leg.legendHandles[1].set_color('black')

ax5 = plt.subplot(515)
plt.axhline(y = 0.1, color ='gray', alpha=0.3,linewidth=0.2)
plt.axhline(y = 0.2, color ='gray', alpha=0.3,linewidth=0.2)
plt.axhline(y = 0.3, color ='gray', alpha=0.3,linewidth=0.2)
k = 0
for i_eos, eos in enumerate(P['eos']):
    if i_eos == k:
        plt.axvline(x = P.at[i_eos,'name'], color ='gray', alpha=0.3,linewidth=0.2)
        k = k + 10
    else:
        plt.axvline(x = P.at[i_eos,'name'], color ='white', alpha=0.,linewidth=0.2)
    resolutions = P.at[i_eos,'resolution'].split(' ')
    for rr in resolutions:
        if rr=='':
            continue
        else:
            #plt.scatter(P.at[i_eos,'name'],float(rr), color=colors[i_eos], marker=".")
            plt.scatter(P.at[i_eos,'name'],float(rr), c=colordict[eos], marker=".")
ax5.xaxis.set_major_locator(MultipleLocator(tt))
ax5.xaxis.set_minor_locator(MultipleLocator(1))
ax5.tick_params(which='major', length=4, color='k', rotation=75)
ax5.tick_params(which='minor', length=1.5, color='k')
plt.ylabel(r'$\Delta x$') 
plt.xlim(-1,len(x))
#ax1.grid(alpha=0.4)
#ax2.grid(alpha=0.4)
#ax3.grid(alpha=0.4)
#ax4.grid(alpha=0.4)
#ax5.grid(alpha=0.4)

#plt.xticks(x[::4], rotation=75)
plt.savefig("general_stats.pdf")
plt.show() 
