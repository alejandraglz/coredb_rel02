from cmath import nan
from re import M, X
import string
from turtle import color, ht
import numpy as np
import matplotlib.pyplot as plt
from matplotlib import cm, markers
from matplotlib.ticker import (MultipleLocator, AutoMinorLocator)
import array
import matplotlib
import pandas as pd
import csv
from watpy.utils.ioutils import *
import scipy.stats as st
import mplcursors
import os
from scipy.stats import t
from bajes.obs.gw.utils import lambda_2_kappa

import sys
sys.path.append('../')
import csv
from handy_functions import *
from bns_fits import *

matplotlib.rcParams['text.usetex']= True
matplotlib.rcParams['font.serif']= 'Times New Roman'
matplotlib.rcParams['font.size']= 15 #28

##############################
# DATA
##############################

data = csv_reader("../data/database_old.csv")

P = get_df(data) #ready to go pandas df
#P.index = np.arange(1, len(P)+1)

## Add info about every run as a list
json_file = "../breschi_fits/results/fits_results.json"
jdict_list = read_json_into_dict(json_file)

reso = []
f2p = []
PC = []
for ip, pp in enumerate(P['name']):
    ss = []#""
    ff = []
    ppc = []
    for dict in jdict_list:
        if not dict==None:
            if dict['database_key'].startswith(pp):
                ss.append(float(dict["resolution"])) #= ss + str(dict["resolution"]) + ' '
                try:
                    ff.append(float(dict['a_0']))
                    ppc.append(dict['PC'])
                except TypeError:
                    ff.append(np.nan)
                    ppc.append('true')
    reso.append(ss)  
    f2p.append(ff)
    PC.append(ppc)

reso = pd.Series(reso)
reso = reso.to_frame()
reso.columns=['resolution']
f2p = pd.Series(f2p)
f2p = f2p.to_frame()
f2p.columns=['a_0']
PC = pd.Series(PC)
PC = PC.to_frame()
PC.columns=['PC']

P['resolution'] = reso
P['a_0'] = f2p
P['PC'] = PC


f2_hr = []
f2_error = []
PC_hr = []
for ip, pp in enumerate(P['resolution']):
    try:
        highres = np.argmin(pp)
        f2_ = P.at[ip,'a_0'][highres]
        pcc = P.at[ip,'PC'][highres]
        f2__ = P.at[ip,'a_0'][np.argmax(pp)]
        '''
        if (f2_<0.0513 and f2_>0.0215):
            f2_hr.append(f2_)
            err = np.abs(f2_-f2__)
            if err>0.003:
                f2_error.append(0)
            else:
                f2_error.append(err)
        else:
            f2_hr.append(nan)
            f2_error.append(0)
        '''
        f2_hr.append(f2_)
        err = 0.5*(np.max(P.at[ip,'a_0'])-np.min(P.at[ip,'a_0']))
        #f2_error.append(err)
        if err>0.003:
            f2_error.append(0)
        else:
            f2_error.append(err)
        PC_hr.append(pcc)
    except ValueError:
        f2_hr.append(nan)
        f2_error.append(0)
        PC_hr.append('True')
    
f2_hr = pd.Series(f2_hr)
f2_hr = f2_hr.to_frame()
f2_hr.columns=['a0_HR']
f2e = pd.Series(f2_error)
f2e = f2e.to_frame()
f2e.columns=['a0_error']
PC_hr = pd.Series(PC_hr)
PC_hr = PC_hr.to_frame()
PC_hr.columns=['PC_hr']

P['a0_HR'] = f2_hr
P['a0_error'] = f2e
P['PC_hr'] = PC_hr

#faulty_wvfs = ['THC:0052','THC:0054','THC:0098','THC:0042','THC:0056','THC:0068','THC:0069','THC:0079']
#no_eos = ['G2','G2k123']
phase_transitions = ['THC:0037','THC:0058','THC:0059','THC:0060','THC:0061','THC:0062','THC:0063','THC:0064','THC:0070','THC:0071','THC:0072','THC:0102','THC:0103','THC:0104','THC:0105','THC:0106','THC:0107','THC:0109','THC:0110']

# correcting eos names
P.loc[P["eos"]=="sly", "eos"] = 'SLy'
P.loc[P["eos"]=="SLY", "eos"] = 'SLy'
P.loc[P["eos"]=="MS1B", "eos"] = 'MS1b'
P.loc[P["name"].isin(phase_transitions), "PC_hr"] = 'True'
P.loc[P["name"].isin(['BAM:0047','THC:0024','THC:0091','THC:0092','THC:0097']), "PC_hr"] = 'True' #actually PC, oscillate shortly and collapse
P.loc[P['a0_HR']>0.2,'a0_HR'] = np.nan
# filtering data
P = P.loc[P['name']!='THC:0052'] # faulty sims (already removed in tullio)
P = P.loc[P['name']!='THC:0056'] # delete ..
P = P.loc[P['name']!='THC:0069'] # .. this once you correct the stuff in the json too
#P = P.loc[P['q']==1] 
#P = P.loc[P['PC_hr']==False]
P = P.loc[P['eos']!='G2']      # not on matteo's paper
P = P.loc[P['eos']!='G2k123']  # ''
#P = P.loc[P['eos']!='DD2']     # ''
P = P.loc[P['eos']!='SLy4']
#P = P.loc[P['kap2t']>60]
P = P.loc[P['kap2t']<750]
#P = P.loc[P['chiAz']==0]
#P = P.loc[P['chiBz']==0]
P.dropna()
P=P.fillna(0)
print(P)
#print(P.loc[[124]])

###
# errors
'''
from watpy.coredb.coredb import *
db_path = '/home/agonzalez/Documents/git/watpy/msc/CoRe_DB_clone/'
cdb = CoRe_db(db_path)
idb = cdb.idb
sim = cdb.sim
f_error = np.zeros(len(name))
i=0

for simu in idb.dbkeys:
    runs = list(sim[simu].run.keys())
    if len(runs)==1:
        ferror = 0
    else:
        resos =[]
        for run in runs:
            runmd = sim[simu].run[run].md
            resos.append(float(runmd.data['grid_spacing_min']))
        hr_run = runs[np.argmin(resos)]
        lr_run = runs[np.argmax(resos)]
        # high res
        dseth5 = sim[simu].run[hr_run].data.read_dset()
        rh22_files = list(dseth5['rh_22'].keys())[0] 
        dset = dseth5['rh_22'][rh22_files]
        ht          = dset[:,1] + 1j*dset[:,2]
        peb=np.unwrap(np.angle(ht)) #Phase EOB
        feb=np.abs(np.diff(peb)/np.diff(dset[:,0])) #Frequency EOB
        f_hmax = feb[np.argmax(abs(ht))]
        # low res
        dseth5 = sim[simu].run[lr_run].data.read_dset()
        rh22_files = list(dseth5['rh_22'].keys())[0] 
        dset = dseth5['rh_22'][rh22_files]
        ht          = dset[:,1] + 1j*dset[:,2]
        peb=np.unwrap(np.angle(ht)) #Phase EOB
        feb=np.abs(np.diff(peb)/np.diff(dset[:,0])) #Frequency EOB
        f_lmax = feb[np.argmax(abs(ht))]
        ferror = f_hmax - f_lmax
    for run in runs:
        if ferror/mtot[i] >0.004:
            f_error[i]=0
        else:
            f_error[i]=ferror/mtot[i]
        i=i+1
'''

'''
save = 'BAM:0001'
resos = [] #resolutions
same_sim = []
f_error = np.zeros(len(name))
i=0
for nam in name:
    if nam.startswith(save):
        resos.append(res[i])
        same_sim.append(i)
    else:
        # save for prev sim
        max_res = np.argmax(resos)
        min_res = np.argmin(resos)
        for sim in same_sim:
            f_error[sim]=f_pm[max_res] - f_pm[min_res]
            if f_error[sim]<0:
                f_error[sim] = 0
        # new sim
        resos=[]
        same_sim=[]
        save = nam.split(':')[0] + ':' + nam.split(':')[1]
        resos.append(res[i])
        same_sim.append(i)
    i=i+1
'''
###
# fits
#a0,a1,b0,b1,n1,n2,d1,d2,q1,q2,q3,q4 = (0.0881,22.814700323004796,0.29246401361707447,24.999999962619935,
#                                       0.007023135582189979,-1.7816176582640582e-06,0.025874168850673,6.579585900552341e-06,
#                                       5.427803269576005,0.0,39.29307296443815,0.0)
nu      = np.array(P['q'])/(1+np.array(P['q']))**2
s1z = np.array(P['chiAz'])
s2z = np.array(P['chiBz'])
delta   = np.sqrt(1.0 - 4.0*nu)
m1M     = 0.5*(1.0 + delta)
m2M     = 0.5*(1.0 - delta)
Shat    = m1M*m1M*np.array(P['chiAz']) + m2M*m2M*np.array(P['chiBz'])
Xnu     = 1. - 4.*nu
'''
# compare & plot
def predict_fit(xxx):
    kkk,sss,qqq = xxx
    #qqq =  1. - 4.*(qqq/(1+qqq)**2)
    p1s = b0 * (1 + b1 * qqq)
    _n1 = n1 * (1 + q1 * qqq)
    _n2 = n2 * (1 + q2 * qqq)
    _d1 = d1 * (1 + q3 * qqq)
    _d2 = d2 * (1 + q4 * qqq)
    _up = (1.+ _n1 * kkk + _n2 * kkk **2)
    _lo = (1.+ _d1 * kkk + _d2 * kkk **2)
    return a0 * (1 + a1*qqq) * (1 + p1s*sss) *  _up / _lo

vars = [np.array(P['kap2t']),Shat,Xnu]
fit_data = predict_fit(vars)
'''
lambda_ax       = np.linspace(0,1200, 5000)
#prediction      = predict_fit([lambda_ax,0.,0.])
#prediction_qqqq = predict_fit([lambda_ax,0.,1-4*0.2222222])
#prediction_mchi = predict_fit([lambda_ax,+0.1,0.])
#prediction_pchi = predict_fit([lambda_ax,-0.1,0.])

prediction      = Mf2_fits([lambda_ax,0.,0.])
prediction_qqqq = Mf2_fits([lambda_ax,0.,1-4*0.2222222])
prediction_mchi = Mf2_fits([lambda_ax,+0.1,0.])
prediction_pchi = Mf2_fits([lambda_ax,-0.1,0.])

nn = np.linspace(0,0.25,1000)
kk = np.linspace(0,500,1000)
m1 = np.linspace(1,3,1000)
m2 = np.linspace(1,3,1000)
M = np.linspace(2,6,1000)
chi1 = np.linspace(-0.9,0.9,1000)
chi2 = np.linspace(-0.9,0.9,100)
'''
def Qfit(kk,q,S):
    nu = q / ((1+q)*(1+q))

    X = 1 - 4*nu
    a0 = 0.0881
    a1_M = 22.81
    QM = 1 + a1_M*X

    #S = chi1*(m1/M)**2 + chi2*(m2/M)**2 
    ai_S = 0.2925
    bi_S = 25
    p1_S = ai_S*(1+bi_S*X)
    QS = 1+p1_S*S

    ai_T = [0.007023,-1.782e-6,0.02587,6.58e-6]
    bi_T = [5.428,0,39.29,0]
    p_T = [ai_T[0]*(1+bi_T[0]*X),ai_T[1]*(1+bi_T[1]*X),ai_T[2]*(1+bi_T[2]*X),ai_T[3]*(1+bi_T[3]*X)]
    QT = ( 1+ p_T[0]*kk + p_T[1]*kk*kk)/(1 + p_T[2]*kk + p_T[3]*kk*kk)

    return a0*QM*QS*QT
'''
#faulty_wvfs = ['THC:0052:R01','THC:0054:R01','THC:0098:R02','THC:0042:R02','THC:0054:R01','THC:0056:R01','THC:0056:R02','THC:0056:R03','THC:0068:R01','THC:0068:R02','THC:0069:R01','THC:0079:R03']
#lowres_wvfs = ['THC:0079:R02']

################################
# PLOTTING
################################

name_fit = np.array(P['name'])
f2_fit = np.array(P['a0_HR'])
k2t_fit = np.array(P['kap2t'])
q = np.array(P['q'])
#m1_fit = []
#m2_fit = []
#nu_fit = []
eos_fit = np.array(P['eos'])
ferror_fit = np.array(P['a0_error'])
pc_fit = np.array(P['PC_hr'])


# find indeces
iq1 = np.where((Xnu==0)&(s1z==0)&(s2z==0)) # equal mass non spinning
inq = np.where((Xnu!=0)&(s1z==0)&(s2z==0)) # unequal mass non spinning
iss = np.where((s1z!=0)&(s2z!=0)) # spinning

fit_data = Mf2_fits([k2t_fit[iq1],Shat[iq1],Xnu[iq1]])
rel_res = (f2_fit[iq1] - fit_data)/fit_data
k22 = k2t_fit[iq1]
fff = ferror_fit[iq1]
resx = rel_res
up = np.percentile(resx, 95)
lo = np.percentile(resx, 5)

print(up,lo)
#c = [float(hash(s) % 256) / 256 for s in eos]  
#colors = plt.cm.jet(c)

eos_lisst = list(dict.fromkeys(eos_fit))
dict_eos = {eos:i for i,eos in enumerate(eos_lisst)}

colors = plt.get_cmap('tab20')(np.arange(len(dict_eos.keys())))
colordict = {
    '2B' : '#002F79',#[0,47,121],
    '2H' : '#0070FF',#[0,112,255],
    'ALF2': 'mediumslateblue',#[0,169,255],
    'ENG': '#00E8FF',#[0,232,255],
    'G2': '#00FA92',#[0,250,146],
    'G2k123': 'lightseagreen',
    'H4': '#46DD00',#[70,221,0],
    'MPA1': 'yellowgreen',#[125,248,3],
    'MS1': 'seagreen',#[186,255,59],
    'MS1b': 'gold',#[236,255,15],
    'SLy': 'orange',
    'BHBlp': '#FF6804',#[255,104,4],
    'DD2': '#FF006A',#[255,0,106],
    'LS220': 'hotpink',#[255,0,194],
    'SFHo': 'darkmagenta',#[182,74,248],
    'BLh': 'orchid',#[211,103,242],
    'BLQ': 'crimson',#[244,187,246],
    'SLy4': 'pink'
}


markero = markers.MarkerStyle(marker='o', fillstyle='none')

fig, (ax,ax2) = plt.subplots(2,1,figsize=(7,5),gridspec_kw={'height_ratios': [1, 0.3]})#plt.figure(figsize=(12,5))
ax1 = ax
plt.subplots_adjust(hspace=0)
#ax1 = plt.subplot(111)
lin1 = ax.plot(lambda_ax,prediction,'k-',label=r'$q=1, \hat{S}=0$')
lin2 = ax.plot(lambda_ax,prediction_qqqq,'k--',label=r'$q=2, \hat{S}=0$')
lin3 = ax.plot(lambda_ax,prediction_pchi,'k:',label=r'$q=1, \hat{S}=0.1$')
handles, labels = ax.get_legend_handles_labels()
leg1 = ax.legend(ncol=1, loc="upper right",handles=handles,labels=labels, frameon='False', fancybox='False', edgecolor='white')
ax.add_artist(leg1)
#plt.fill_between(xif, F+low, F-low, color='gray', alpha = 0.3)
eos_list = []
for i,i_eos in enumerate(eos_fit):
    if i_eos in eos_list:
        ax1.errorbar(k2t_fit[i],f2_fit[i], yerr=ferror_fit[i], color=colordict[i_eos], fmt=".", capsize=1.5)     
    else:  
        if i_eos=='BHBlp':
            lal = r'BHB$\Lambda\phi$'
        else:
            lal = i_eos
        ax1.errorbar(k2t_fit[i],f2_fit[i], yerr=ferror_fit[i], color=colordict[i_eos], fmt=".", label=lal, capsize=1.5)
        eos_list.append(i_eos)
ax1.set_ylim([0.021,0.056])
ax1.set_xlim([40,538])   
ax1.set_ylabel(r'$Mf_2$')
    #if k2t_fit[i]<98 and k2t_fit[i]>96 and f2_fit[i]>0.04555:
    #    print(name_fit[i],i_eos)
for ik,kk in enumerate(k22):
    ax2.errorbar(kk,rel_res[ik], yerr=fff[ik], color=colordict[eos_fit[iq1][ik]], fmt=".", capsize=1.5)
xax = ax1.get_xaxis()
xax = xax.set_visible(False)
ax2.plot(lambda_ax,np.zeros(len(lambda_ax)), c='k')
#ax2.fill_between(lambda_ax,y1=up,y2=lo,facecolor ='gray', alpha = 0.2)
ax2.set_xlim([40,538])
ax2.set_ylim([-0.1,0.1])  
ax2.set_ylabel(r'Rel. Diff.')
ax2.set_xlabel(r"$\kappa^T_2$")
#plt.tick_params('x', labelbottom=False)
handles, labels = ax1.get_legend_handles_labels()
leg2 = ax1.legend(handles=handles[3:],labels=labels[3:], bbox_to_anchor=(0, 1, 1, 0), loc="lower left", mode="expand",ncol=5, markerscale=2.5, frameon='False', fancybox='False', edgecolor='white')
#cbar=plt.colorbar(cm.ScalarMappable(norm=None, cmap=cmap),location='top',orientation='horizontal', fraction=0.07,label=r'$EOS$')
plt.tight_layout()
#plt.savefig('f2_fits.pdf')
##mplcursors.cursor(ax1).connect("add", lambda sel: sel.annotation.set_text(name[sel.index]))
##cursor = mplcursors.cursor(hover=True)
##cursor.connect("add", lambda sel: sel.annotation.set_text(name[sel.index]))
plt.show()

'''
####################
# QUR of type f2(R)
####################
m1_fit = np.array(P['m1'])
m2_fit = np.array(P['m2'])

eos_dir = '../../tov/Sequences/Stable'
avail_eos = [eos.split('_')[0] for eos in os.listdir(eos_dir) if 'sequence' in eos]

r_14 = []
r_18 = []
for i,eos in enumerate(eos_fit):
    if eos in avail_eos:
        mtot = m1_fit[i] + m2_fit[i]
        m_eos, r_eos = np.genfromtxt(eos_dir+'/{}'.format(eos)+'_sequence.txt', usecols=[1,3], unpack=True)
        #r1         = np.interp(m1_fit, m_eos, r_eos)
        #r2         = np.interp(m2_fit, m_eos, r_eos)
        one = np.where((m_eos>1.37) & (m_eos<1.43))
        two = np.where((m_eos>1.77) & (m_eos<1.83))
        #print(two)
        r14 = r_eos[ one[0][0] ]
        r18 = r_eos[ two[0][0] ]
        r_14.append(r14/mtot)   # R/M
        r_18.append(r18/mtot)
    else:
        r_14.append(nan)
        r_18.append(nan)

############## FITS

##### f2(Rx)
def f2Rx(a,Rx):
    final = []
    for r in Rx:
        final.append(a[0] + a[1]*r + a[2]*r**2)
    return final

##### f2(Rx,R1.4/R1.8)
def f2RxRx(a,Rx,R48):
    final = []
    for i,r in enumerate(Rx):
        final.append(a[0] + a[1]*r + a[2]*r**2 + a[3]*R48[i])
    return final

a = [0.2,-0.0762,0.0078] #R_1.4
b = [0.236,-0.103,0.0125] #R_1.8
c = [0.162,-0.115,0.0145,0.0919] #R_1.4
d = [0.213,-0.105,0.013,0.0241] #R_1.8

ratio = np.array(r_14) / np.array(r_18)

x = np.linspace(0.02,0.055,1000)


import matplotlib.cm as cm
norm = matplotlib.colors.Normalize(vmin=min(ratio), vmax=max(ratio), clip=True)
mapper = cm.ScalarMappable(norm=norm, cmap='viridis')
color = np.array([(mapper.to_rgba(v)) for v in ratio])


fig, axs = plt.subplots(2, 2, figsize=(10,6))

####### 1st panel

# confidence intervals
residuals  = f2_fit - np.array(f2Rx(a,r_14))
residuals = residuals[np.logical_not(np.isnan(residuals))]
up = np.percentile(residuals,95)
lo = np.percentile(residuals,5)

axs[0,0].plot(x,x,'k-')
axs[0,0].fill_between(x,x+up,x+lo,color='gray', alpha=0.2, label=r"$90\%$ $\rm{CI}$")
cheese=axs[0,0].scatter(f2Rx(a,r_14),f2_fit, c=ratio, cmap='viridis', s=10.5, label=r"$\rm{CoRe}$ $\rm{DB}$")
for i,co in enumerate(color):
    axs[0,0].errorbar(f2Rx(a,r_14)[i],f2_fit[i],yerr=ferror_fit[i], c=co, fmt='none')
axs[0,0].set_xlabel(r"$Mf^{\rm{fit}}_2(R_{1.4}/M)$")
axs[0,0].set_ylabel(r"$Mf^{\rm{NR}}_2(R_{1.4}/M)$")
axs[0,0].set_xlim([0.02,0.055])
axs[0,0].set_ylim([0.02,0.055])
leg =axs[0,0].legend(ncol=1, frameon='False', fancybox='False', edgecolor='white', fontsize='small')
leg.legendHandles[1].set_color('black')

####### 2nd panel

# confidence intervals
residuals  = f2_fit - np.array(f2Rx(b,r_18))
residuals = residuals[np.logical_not(np.isnan(residuals))]
up = np.percentile(residuals,95)
lo = np.percentile(residuals,5)
axs[0,1].fill_between(x,x+up,x+lo,color='gray', alpha=0.2)
axs[0,1].plot(x,x,'k-')
cheese=axs[0,1].scatter(f2Rx(b,r_18),f2_fit, c=ratio, cmap='viridis', s=10.5)
for i,co in enumerate(color):
    axs[0,1].errorbar(f2Rx(b,r_18)[i],f2_fit[i],yerr=ferror_fit[i], c=co, fmt='none')
axs[0,1].set_xlabel(r"$Mf^{\rm{fit}}_2(R_{1.8}/M)$")
axs[0,1].set_ylabel(r"$Mf^{\rm{NR}}_2(R_{1.8}/M)$")
axs[0,1].set_xlim([0.02,0.055])
axs[0,1].set_ylim([0.02,0.055])

####### 3rd panel

# confidence intervals
residuals  = f2_fit - np.array(f2RxRx(c,r_14,ratio))
residuals = residuals[np.logical_not(np.isnan(residuals))]
up = np.percentile(residuals,95)
lo = np.percentile(residuals,5)
axs[1,0].fill_between(x,x+up,x+lo,color='gray', alpha=0.2)
axs[1,0].plot(x,x,'k-')
cheese=axs[1,0].scatter(f2RxRx(c,r_14,ratio),f2_fit, c=ratio, cmap='viridis', s=10.5)
for i,co in enumerate(color):
    axs[1,0].errorbar(f2RxRx(c,r_14,ratio)[i],f2_fit[i],yerr=ferror_fit[i], c=co, fmt='none')
axs[1,0].set_xlabel(r"$Mf^{\rm{fit}}_2(R_{1.4}/M,R_{1.4}/R_{1.8})$")
axs[1,0].set_ylabel(r"$Mf^{\rm{NR}}_2(R_{1.4}/M,R_{1.4}/R_{1.8})$")
axs[1,0].set_xlim([0.02,0.055])
axs[1,0].set_ylim([0.02,0.055])

####### 4th panel

# confidence intervals
residuals  = f2_fit - np.array(f2RxRx(d,r_18,ratio))
residuals = residuals[np.logical_not(np.isnan(residuals))]
up = np.percentile(residuals,95)
lo = np.percentile(residuals,5)
axs[1,1].fill_between(x,x+up,x+lo,color='gray', alpha=0.2)
axs[1,1].plot(x,x,'k-')
cheese=axs[1,1].scatter(f2RxRx(d,r_18,ratio),f2_fit, c=ratio, cmap='viridis', s=10.5)
for i,co in enumerate(color):
    axs[1,1].errorbar(f2RxRx(d,r_18,ratio)[i],f2_fit[i],yerr=ferror_fit[i], c=co, fmt='none')
axs[1,1].set_xlabel(r"$Mf^{\rm{fit}}_2(R_{1.8}/M,R_{1.4}/R_{1.8})$")
axs[1,1].set_ylabel(r"$Mf^{\rm{NR}}_2(R_{1.8}/M,R_{1.4}/R_{1.8})$")
axs[1,1].set_xlim([0.02,0.055])
axs[1,1].set_ylim([0.02,0.055])

cbar = plt.colorbar(cheese, ax=[axs[0,1],axs[1,1]], anchor=(1.84, 0.5))
cbar.set_label(r'$R_{1.4}/R_{1.8}$')
plt.subplots_adjust(left=0.1,right=0.87, wspace=0.3, hspace=0.4)
#plt.tight_layout()
plt.savefig('f2_R_fits.pdf')
plt.show()

'''

####################
# choose target = amrg, fmrg, lpeak, erad_m
####################
from scipy.optimize import curve_fit
target = f2_fit

# equal mass non spinning
def fit_func_q1(xxx, a0, a1, a2, a3, a4):
    kkk,sss,qqq = xxx
    _up = (1.+ a1 * kkk + a2 * kkk**2)
    _lo = (1.+ a3 * kkk + a4 * kkk**2)
    return a0 *  _up / _lo

vars            = [k2t_fit[iq1],Shat[iq1],Xnu[iq1]]
popt_q1, pcov   = curve_fit(fit_func_q1, vars, target[iq1], maxfev=500000)
a0, a1, a2, a3, a4 = popt_q1
print(popt_q1)


# unequal mass
def fit_func_nq(xxx, aM, b1, b2, b3, b4):
    kkk,sss,qqq = xxx
    qM = 1 + aM*qqq
    p1T = a1*(1+b1*qqq)
    p2T = a2*(1+b2*qqq)
    p3T = a3*(1+b3*qqq)
    p4T = a4*(1+b4*qqq)
    _up = (1.+ p1T * kkk + p2T * kkk**2)
    _lo = (1.+ p3T * kkk + p4T * kkk**2)
    qT = _up / _lo
    return a0 * qM * qT

vars            = [k2t_fit[inq],[0]*len(k2t_fit[inq]),Xnu[inq]]
popt_nq, pcov_nq   = curve_fit(fit_func_nq, vars, target[inq], maxfev=500000)
aM, b1, b2, b3, b4 = popt_nq
print(popt_nq)

# spinning!
def fit_func_iss(xxx,a1S,b1S):
    kkk,sss,qqq = xxx
    qM = 1 + aM*qqq
    p1S = a1S * (1 + b1S*qqq)
    qS = 1 + p1S * sss
    p1T = a1*(1+b1*qqq)
    p2T = a2*(1+b2*qqq)
    p3T = a3*(1+b3*qqq)
    p4T = a4*(1+b4*qqq)
    _up = (1.+ p1T * kkk + p2T * kkk**2)
    _lo = (1.+ p3T * kkk + p4T * kkk**2)
    qT = _up / _lo
    return a0 * qM * qS * qT

vars            = [k2t_fit[iss],Shat[iss],Xnu[iss]]
popt_iss, pcov_iss   = curve_fit(fit_func_iss, vars, target[iss], maxfev=500000)
a1S, b1S = popt_iss
print(popt_iss)

### prediction
def predict_fit(xxx):
    kkk,sss,qqq = xxx
    qM = 1 + aM*qqq
    p1S = a1S * (1 + b1S*qqq)
    qS = 1 + p1S * sss
    p1T = a1*(1+b1*qqq)
    p2T = a2*(1+b2*qqq)
    p3T = a3*(1+b3*qqq)
    p4T = a4*(1+b4*qqq)
    _up = (1.+ p1T * kkk + p2T * kkk**2)
    _lo = (1.+ p3T * kkk + p4T * kkk**2)
    qT = _up / _lo
    return a0 * qM * qS * qT

_prediction      = predict_fit([lambda_ax,0.,0.])
_prediction_qqqq = predict_fit([lambda_ax,0.,1-4*0.2222222])
_prediction_mchi = predict_fit([lambda_ax,+0.1,0.])
_prediction_pchi = predict_fit([lambda_ax,-0.1,0.])

fit_data = predict_fit([k2t_fit[iq1],Shat[iq1],Xnu[iq1]])
rel_res = (target[iq1] - fit_data)/fit_data
res = np.sum(rel_res**2)
sigma = np.std(rel_res)

print("Sum res. sq. = ", res)
print("Rel. st. dev. = ", sigma)

resx = rel_res
up = np.percentile(resx, 95)
lo = np.percentile(resx, 5)

print(up,lo)

data_average = np.mean(target[iq1])
SStot = np.sum((target[iq1]-data_average)**2)
SSres = np.sum((target[iq1]-fit_data)**2)
R_square = 1 - SSres/SStot
print( '\nRsquare:\n', R_square, '\n')

n, b, _     = plt.hist(rel_res, bins=30,density=True)
b = b[:-1] + 0.5*(b[1]-b[0])
gaus_plot   = np.exp(-0.5*(b/sigma)**2.)/np.sqrt(2.*np.pi)/sigma
plt.plot(b, gaus_plot, c='r')
plt.xlabel(r"residuals")
plt.show()

fig, ax = plt.subplots(2,1,gridspec_kw={'height_ratios': [1, 0.3]})

cheese = ax[0].scatter(k2t_fit[target>0],target[target>0],label=r'CoReDB',marker='.',c=q[target>0],cmap='viridis')
ax[0].plot(lambda_ax,_prediction,label=r'$\hat{S}=q=0$',color='k')
ax[0].plot(lambda_ax,_prediction_qqqq,label=r'$\hat{S}=0$,$q=2$',color='k',linestyle=':')
ax[0].plot(lambda_ax,_prediction_pchi,label=r'$\hat{S}=0.1$,$q=0$',color='k',linestyle='--')
ax[0].legend()
ax[1].set_xlabel(r'$\kappa^T_2$')
ax[0].set_ylabel(r'$A_0/M$')
ax[0].set_xlim([0,np.max(k2t_fit[target>0])])
ax[0].set_ylim([np.min(target[target>0]),np.max(target[target>0])])

ax[1].scatter(k2t_fit[iq1],rel_res,marker='.',c=q[iq1],cmap='viridis')
ax[1].plot(lambda_ax,np.zeros(len(lambda_ax)), c='k')
ax[1].set_xlim([0,np.max(k2t_fit[target>0])])
#ax[1].set_ylim([np.min(target[target>0]),np.max(target[target>0])])
cbar = plt.colorbar(cheese, ax=[ax[0],ax[1]],orientation='horizontal', location='top', anchor=(0., 1), fraction=0.1, aspect=35)
cbar.set_label(label=r'$q$',loc='center')
plt.show()
