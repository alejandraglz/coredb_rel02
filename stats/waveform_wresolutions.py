from re import M
import string
from turtle import color
import numpy as np
import matplotlib.pyplot as plt
import matplotlib
import pandas as pd
import csv
from watpy.utils.ioutils import *
from watpy.coredb.coredb import *
from watpy.utils.coreh5 import *

import sys
sys.path.append('../')
import csv
from handy_functions import *

matplotlib.rcParams['text.usetex']= True
matplotlib.rcParams['font.serif']= 'Times New Roman'
matplotlib.rcParams['font.size']= 15 #28

##############################
# DATA
##############################

#sim_path    = '/home/agonzalez/Documents/git/watpy/msc/CoRe_DB_clone/' #previous release
sim_path = '/data/numrel/DATABASE/CoRe_DB_clone/'
rel2_path   = '/data/numrel/DATABASE/Release02/'

cdb = CoRe_db(sim_path)
idb = cdb.idb

dirc = ['BAM:0129','BAM:0052']
runc = ['R01','R01']
runc2 = ['R02','R03']
end = [5000,1680]

################################
# PLOTTING
################################
i=0
fig, axs = plt.subplots(2, 1,figsize=(7,5))
for ss in dirc:
    try: # first release
        sim = cdb.sim[ss]
        simrun = sim.run[runc[i]]
        simrun2 = sim.run[runc2[i]]
    except: #second release
        ssd=ss.replace(':','_')
        simrun = CoRe_run(rel2_path+ssd+'/'+runc[i])
        simrun2 = CoRe_run(rel2_path+ssd+'/'+runc2[i])

    ## first res
    datah5 = simrun.data
    dseth5 = datah5.read_dset()

    rh22_files = list(dseth5['rh_22'].keys())[0] # different distances and take smallest distance
    dset = dseth5['rh_22'][rh22_files] 

    # read
    hp          = dset[:,1]
    hc          = dset[:,2]
    t           = dset[:,0]
    ht          = hp + 1j*hc

    ## second res
    datah52 = simrun2.data
    dseth52 = datah52.read_dset()

    rh22_files2 = list(dseth52['rh_22'].keys())[0] # different distances and take smallest distance
    dset2 = dseth52['rh_22'][rh22_files2] 

    # read
    hp2          = dset2[:,1]
    hc2          = dset2[:,2]
    t2           = dset2[:,0]
    ht2          = hp2 + 1j*hc2


    axs[i].set_title(ss)
    axs[i].grid(True)
    axs[i].plot(t,hp,linestyle='-',color='black',label="highest resolution")
    axs[i].plot(t2,hp2,linestyle='--',color='cornflowerblue',label='second highest')
    
    axs[i].legend(loc='upper left',fontsize='small')
    axs[i].set_xlim([0,end[i]])
    axs[i].set_xlabel(r'$u/M$')
    axs[i].set_ylabel(r'$Re(Rh_{22})/M$')
    i=i+1
plt.tight_layout()
#plt.savefig('waves.pdf')
plt.show()

