from cmath import nan
from re import M
import string
from tkinter import E
from turtle import color
import numpy as np
import matplotlib.pyplot as plt
from matplotlib import cm, markers
from matplotlib.colors import ListedColormap, LinearSegmentedColormap
from matplotlib.ticker import (MultipleLocator, AutoMinorLocator)
import array
import matplotlib
import pandas as pd
import csv
from watpy.utils.ioutils import *
from watpy.utils.num import *
from watpy.wave.gwutils import *
from bajes.obs.gw.utils import compute_chi_eff, compute_lambda_tilde
import matplotlib.patches as mpatches

import sys
sys.path.append('../')
import csv
from handy_functions import *
from mismatch import *

matplotlib.rcParams['text.usetex']= True
#matplotlib.rcParams['font.family'] = ['serif']
matplotlib.rcParams['font.serif']= 'Times New Roman'
matplotlib.rcParams['font.size']= 15#28

Msuns  = 4.925491025543575903411922162094833998e-6
Mpc_m  = 3.085677581491367278913937957796471611e22
Msun_m = 1.476625061404649406193430731479084713e3

data = csv_reader("../data/database.csv")
P = get_df(data)

#sim_path    = '/home/agonzalez/Documents/git/watpy/msc/CoRe_DB_clone/'
sim_path    = '/data/numrel/DATABASE/CoRe_DB_clone/'
rel2_path   = '/data/numrel/DATABASE/Release02/'

cdb = CoRe_db(sim_path)
idb = cdb.idb

#to_plot = ['THC:0093','BAM:0104','BAM:0130','BAM:0112']
to_plot = ['THC:0093','BAM:0129','THC:0098','BAM:0112']
labels = [r'$q=1$, $M=2.7~M_{\odot}$, $\hat{S}=$ ,$\kappa^T_2=67$',r'$q=1$, $M=2.7~M_{\odot}$, $\hat{S}=$ ,$\kappa^T_2=72$',r'$q=2$, $M=2.7~M_{\odot}$, $\hat{S}=$ ,$\kappa^T_2=109$',r'$q=1$, $M=2.7~M_{\odot}$, $\hat{S}=$ ,$\kappa^T_2=70$, eccentric']
colors = ['#d7191c','#fdae61','gold','#abdda4','#2b83ba']

fig, ax = plt.subplots(figsize=(12.1,5.5))

### SXS BBH
jorb_nr = np.loadtxt('Ej_q1_s0.dat',usecols=[0],delimiter=',')
eb_nr = np.loadtxt('Ej_q1_s0.dat',usecols=[1],delimiter=',')
jnm = np.loadtxt('Ej_q1_s0_mrg.dat',usecols=[0])
enm = np.loadtxt('Ej_q1_s0_mrg.dat',usecols=[1])

plt.plot(jorb_nr,eb_nr,label=r'BBH, $q=1.0$',color=colors[-1])
plt.scatter(jnm,enm,color=colors[-1])

### CoRe BNS
saved_jorb = 0
saved_ebin = 0
saved_color = 'k'
for ie, entry in enumerate(to_plot): 
    if entry in idb.dbkeys: # CURRENT RELEASE
        sim = cdb.sim[entry]
    else:                       # NEW RELEASE
        dir            = entry.replace(':','_')
        this_path = os.path.join(rel2_path,dir)
        sim = CoRe_sim(this_path)

    res = 'R01'
    simrun = sim.run[res]
    datah5 = simrun.data
    dseth5 = datah5.read_dset()
    # energy
    energy_files = list(dseth5['energy'].keys())
    dset = dseth5['energy'][energy_files[0]]
    jorb = dset[:,0] # angular momentum
    ebin = dset[:,1] # binding energy
    # merger
    rh22_files = list(dseth5['rh_22'].keys())
    dset2 = dseth5['rh_22'][rh22_files[0]]
    hp          = dset2[:,1]
    hc          = dset2[:,2]
    ht          = hp + 1j*hc
    amp22   = np.abs(ht)
    imerg   = np.argmax(amp22)

    qu = float(P.loc[P['name']==entry, 'q'])
    nu = q_to_nu(qu)
    delta   = np.sqrt(1.0 - 4.0*nu)
    m1M     = 0.5*(1.0 + delta)
    m2M     = 0.5*(1.0 - delta)
    s1 = float(P.loc[P['name']==entry, 'chiAz'])
    s2 = float(P.loc[P['name']==entry, 'chiBz'])
    ssh    = m1M*m1M*s1 + m2M*m2M*s2
    kkp = float(P.loc[P['name']==entry, 'kap2t'])
    qu = float("{0:.1f}".format(qu))
    ssh = float("{0:.1f}".format(ssh))
    kkp = float("{0:.1f}".format(kkp))
    
    if entry==to_plot[-1]:
        lab = r'\texttt{'+entry+'}, $q='+str(qu)+'$, $M=2.7~M_{\odot}$, $\hat{S}='+str(ssh)+'$ ,$\kappa^T_2='+str(kkp)+'$, eccentric'
    else:
        lab = r'\texttt{'+entry+'}, $q='+str(qu)+'$, $M=2.7~M_{\odot}$, $\hat{S}='+str(ssh)+'$ ,$\kappa^T_2='+str(kkp)+'$'    #+labels[ie]
    ax.plot(jorb,ebin,label=lab,color=colors[ie])
    ax.scatter(jorb[imerg],ebin[imerg],color=colors[ie])
    print(entry, "ebin at mrg= ",ebin[imerg])
    axins = ax.inset_axes([0.67,0.1,0.3,0.4])
    axins.plot(jorb,ebin,color=colors[ie])
    axins.plot(saved_jorb,saved_ebin,color=saved_color)
    axins.set_xlim(4, 4.285)
    axins.set_ylim(-0.0348, -0.028)
    axins.set_xticklabels([])
    axins.set_yticklabels([])

    # save previous
    saved_jorb = jorb
    saved_ebin = ebin
    saved_color = colors[ie]

## SXS sim
'''
bbh_file = 'rhOverM_Asymptotic_GeometricUnits.h5'
with h5py.File(bbh_file,"r") as f:
    nr_f=list(f['Extrapolated_N3.dir']['Y_l2_m2.dat'])
tnr=[] #t/M
rnr0=[] #Re(h_{lm})
inr0=[] #Im(h_{lm})
for i in range(0,len(nr_f)):
    d=nr_f[i]
    tnr.append(d[0])
    rnr0.append(d[1])
    inr0.append(d[2])
rnr=np.array(rnr0)
inr=np.array(inr0)
tnr = np.array(tnr)
hnr = {}
hdot = {}
hnr[2,2]=np.array(rnr) - 1j*np.array(inr)
hdot[2,2] = diff1(tnr,hnr[2,2])

M = 20
m1 = m2 = 10
eadm = 0.9940
padm = 1.169e-8
madm = np.sqrt(eadm - padm)
print("Madm= ",madm)
jadm = 1.21
modes = [(2,2)]
mmodes = [2]
e_nr, _, j_nr, _ = waveform2energetics(hnr,hdot,tnr,modes,mmodes)
eb_nr = (madm - e_nr - m1 - m2) / (m1*m2/(m1+m2))
jorb_nr = (jadm - j_nr) / (m1*m2)

print(eb_nr,jorb_nr)

anr=np.abs(hnr[2,2]) #Amplitude NR
imnr = np.argmax(anr)
'''
ax.indicate_inset_zoom(axins, edgecolor="black")

ax.annotate("evolution",
            xy=(3.3, -0.11), xycoords='data',
            xytext=(3.58, -0.088), textcoords='data',
            arrowprops=dict(arrowstyle="fancy",
                            color="0.5",
                            shrinkB=5,
                            connectionstyle="arc3,rad=0.3",
                            ),
            )


plt.ylabel(r'$E_b$')
plt.xlabel(r'$j$')
plt.xlim([2.6,4.78])
plt.ylim([-0.15,0.02])
plt.legend(fontsize='small',loc='upper left',ncol=2)
plt.tight_layout()
#plt.savefig('energy_curves.pdf')
plt.show()
        
