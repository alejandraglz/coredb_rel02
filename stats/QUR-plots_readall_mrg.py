from cmath import nan
import matplotlib
import matplotlib.pyplot as plt
#matplotlib.use('TkAgg')
import numpy as np 
#from pycbc.types.frequencyseries import FrequencySeries 
import json
import h5py 
import pandas as pd
from matplotlib.ticker import (MultipleLocator, AutoMinorLocator)
from scipy.optimize import curve_fit

from pycbc.filter import match, overlap, optimized_match
#from pycbc.filter.matchedfilter import optimized_match
from pycbc.types.timeseries import TimeSeries
from pycbc.psd import  aLIGODesignSensitivityP1200087, AdVDesignSensitivityP1200087, EinsteinTelescopeP1600143, aLIGOZeroDetHighPower #choose your PSD
from pycbc import waveform

from watpy.coredb.coredb import *
from watpy.wave.wave import *
from watpy.utils.coreh5 import *
from watpy.utils.ioutils import *

import sys
import os
sys.path.append('../')
from mismatch import *
from handy_functions import *

matplotlib.rcParams['text.usetex']= True
#matplotlib.rcParams['font.family'] = ['serif']
matplotlib.rcParams['font.serif']= 'Times New Roman'
matplotlib.rcParams['font.size']= 15 #28

def get_mrg(resos,sim):
    highres = np.argmin(resos)
    highres_run = list(sim.run.keys())[highres]

    simrun1 = sim.run[highres_run]
    meta_dict = simrun1.md.data
    eos_name = meta_dict['id_eos']
    datah51 = simrun1.data
    #datah5.dump()
    dseth51 = datah51.read_dset()
    rh22_files1 = list(dseth51['rh_22'].keys())[0]

    dset1 = dseth51['rh_22'][rh22_files1]
    hp1          = dset1[:,1]
    hc1          = dset1[:,2]
    t1           = dset1[:,0] # u/M
    ht1          = hp1 + 1j*hc1
    #t1           = t1/mtot
    ap1 = np.abs(ht1)
    pp1 = -np.unwrap(np.angle(ht1))
    ff1 = np.abs(np.diff(pp1)/np.diff(t1))
    i_merg = np.argmax(ap1)
    t_merg = t1[i_merg]
    a_merg = np.max(ap1)
    f_merg = ff1[i_merg]*nu*nu

    # energetics
    try:
        efile = list(dseth51['energy'])[0]
        dsete = dseth51['energy'][efile]
        uM = dsete[:,2] # u/M
        angmom = dsete[:,0] # Jorb
        ebin = dsete[:,1] # binding energy
        erad = dsete[:,3] # radiated energy
        te = desete[:,5] # t
        lum = np.gradient(erad,te)*(nu*nu)/((q*q))
        # values at merger
        ii_merg = np.where(uM>t_merg)[0]
        print(ii_merg)
        eb_merg = ebin[ii_merg]
        er_merg = np.max(erad)
        l_peak = lum[ii_merg]
    except:
        l_peak = nan
        er_merg = nan

    return a_merg, f_merg, l_peak, er_merg
    
    
    

#sim_path    = '/home/agonzalez/Documents/git/watpy/msc/CoRe_DB_clone/'
sim_path    = '/data/numrel/DATABASE/CoRe_DB_clone/'
rel2_path   = '/data/numrel/DATABASE/Release02/'

data = csv_reader("../data/database.csv")
P = get_df(data) #ready to go pandas df
cdb = CoRe_db(sim_path)
idb = cdb.idb

amrg = []
fmrg = []
lpeak = []
ermrg = []
resos = []
weird_cases = ['THC:0076']
phase_transitions = ['THC:0037','THC:0056','THC:0057','THC:0058','THC:0059','THC:0060','THC:0061','THC:0062','THC:0067','THC:0068','THC:0069','THC:0099','THC:0100','THC:0101','THC:0102','THC:0103','THC:0104','THC:0106','THC:0107']
precessing = ['BAM:0143','BAM:0146','BAM:0147']
for i_dbkey, entry in enumerate(P['name']):
    print("Working with: ",entry)
    dir            = entry.replace(':','_')
    target         = dir.split('_')[0]
    dirc = dir.replace('_',':')
    splitdir = dir.split('_')
    #resos = []

    if entry in idb.dbkeys:  ## CURRENT RELEASE
        sim = cdb.sim[dirc]
        this_path = os.path.join(sim_path,dir) 
    
        for res in sim.run.keys():
            simrun = sim.run[res]
            meta_dict = simrun.md.data
            resos.append(float(meta_dict['grid_spacing_min']))
            q = float(meta_dict['id_mass_ratio'])
            nu = q_to_nu(q)
            mtot = float(meta_dict['id_mass'])
            k2t = float(meta_dict['id_kappa2T'])
            simname = meta_dict['simulation_name']
        splitdir = simname.split('_')
        if entry in phase_transitions or entry in precessing or (':' in splitdir[3]) or (':' in splitdir[4]):
            print(" - Skipping precessing simulations")
            a_merg, f_merg, l_peak, er_merg  = nan, nan, nan, nan
        else:
            a_merg, f_merg, l_peak, er_merg = get_mrg(resos,sim)
        amrg.append(a_merg)
        fmrg.append(f_merg)
        lpeak.append(l_peak)
        ermrg.append(er_merg)
    else:                    ## NEW RELEASE
        this_path = os.path.join(rel2_path,dir)
        metamain = this_path + '/metadata_main.txt'
        sim = CoRe_sim(this_path)
    
        for res in sim.run.keys():
            simrun = sim.run[res]
            meta_dict = simrun.md.data
            resos.append(float(meta_dict['grid_spacing_min']))
            q = float(meta_dict['id_mass_ratio'])
            nu = q_to_nu(q)
            mtot = float(meta_dict['id_mass'])
            k2t = float(meta_dict['id_kappa2T'])
            simname = meta_dict['simulation_name']
        splitdir = simname.split('_')
        if entry in weird_cases or entry in phase_transitions or entry in precessing or (':' in splitdir[3]) or (':' in splitdir[4]):
            a_merg = nan
            f_merg = nan
            l_peak = nan
            er_merg = nan
        else:
            a_merg, f_merg, l_peak, er_merg = get_mrg(resos,sim)
        amrg.append(a_merg)
        fmrg.append(f_merg)
        lpeak.append(l_peak)
        ermrg.append(er_merg)

    print(er_merg)
    resos = []

orbs = pd.Series(amrg)
orbs = orbs.to_frame()
orbs.columns=['amrg']
P['amrg'] = orbs

radd = pd.Series(fmrg)
radd = radd.to_frame()
radd.columns=['fmrg']
P['fmrg']=radd

add = pd.Series(lpeak)
add = add.to_frame()
add.columns=['lpeak']
P['lpeak']=radd

erad = pd.Series(ermrg)
erad = erad.to_frame()
erad.columns=['erad']
P['erad'] = erad

#phase_transitions = ['THC:0037','THC:0056','THC:0057','THC:0058','THC:0059','THC:0060','THC:0061','THC:0062','THC:0067','THC:0068','THC:0069','THC:0099','THC:0100','THC:0101','THC:0102','THC:0103','THC:0104','THC:0106','THC:0107']

P.loc[P["eos"]=="sly", "eos"] = 'SLy'
P.loc[P["eos"]=="SLY", "eos"] = 'SLy'
P.loc[P["eos"]=="MS1B", "eos"] = 'MS1b'
P = P.loc[P['eos']!='G2']      # not on matteo's paper
P = P.loc[P['eos']!='G2k123']  # ''
P = P.loc[P['eos']!='SLy4']
#P = P.loc[P['q']==1] 

P.dropna()
P=P.fillna(0)
#print(P.loc[P['lpeak']<0.02])
print(P)

###
# fits for Amrg
a0,a1,b0,b1,n1,n2,d1,d2,q1,q2,q3,q4 = (0.39475762,-1.13292325,-0.02991597,-2.59318042,
                                       0.03901957425837708,5.184564561045753e-05,0.06032721528620493,0.00013795694839938442,
                                       10.410256292591564,54.513466134598985,10.826296683028199,54.53588176973234)
nu      = np.array(P['q'])/(1+np.array(P['q']))**2
delta   = np.sqrt(1.0 - 4.0*nu)
m1M     = 0.5*(1.0 + delta)
m2M     = 0.5*(1.0 - delta)
Shat    = m1M*m1M*np.array(P['chiAz']) + m2M*m2M*np.array(P['chiBz'])
Xnu     = 1. - 4.*nu
# compare & plot
def predict_fit(xxx):
    kkk,sss,qqq = xxx
    #qqq =  1. - 4.*(qqq/(1+qqq)**2)
    p1s = b0 * (1 + b1 * qqq)
    _n1 = n1 * (1 + q1 * qqq)
    _n2 = n2 * (1 + q2 * qqq)
    _d1 = d1 * (1 + q3 * qqq)
    _d2 = d2 * (1 + q4 * qqq)
    _up = (1.+ _n1 * kkk + _n2 * kkk **2)
    _lo = (1.+ _d1 * kkk + _d2 * kkk **2)
    return a0 * (1 + a1*qqq) * (1 + p1s*sss) *  _up / _lo

vars = [np.array(P['kap2t']),Shat,Xnu]
fit_data = predict_fit(vars)
lambda_ax       = np.linspace(0,700, 5000)
prediction      = predict_fit([lambda_ax,0.,0.])
prediction_qqqq = predict_fit([lambda_ax,0.,1-4*0.2222222])
prediction_mchi = predict_fit([lambda_ax,+0.1,0.])
prediction_pchi = predict_fit([lambda_ax,-0.1,0.])

###
# fits for Fmrg
a0,a1,b0,b1,n1,n2,d1,d2,q1,q2,q3,q4 = (0.22754806,0.92330138,0.59374838,-1.99378496,
                                       0.03445340731627873,5.5799962023491245e-06,0.08404652974611324,0.00011328992320789428,
                                       13.828175998146255,517.4149218303298,12.74661916436829,139.76057108600236)
# compare & plot
def predict_fitf(xxx):
    kkk,sss,qqq = xxx
    #qqq =  1. - 4.*(qqq/(1+qqq)**2)
    p1s = b0 * (1 + b1 * qqq)
    _n1 = n1 * (1 + q1 * qqq)
    _n2 = n2 * (1 + q2 * qqq)
    _d1 = d1 * (1 + q3 * qqq)
    _d2 = d2 * (1 + q4 * qqq)
    _up = (1.+ _n1 * kkk + _n2 * kkk **2)
    _lo = (1.+ _d1 * kkk + _d2 * kkk **2)
    return a0 * (1 + a1*qqq) * (1 + p1s*sss) *  _up / _lo

fprediction      = predict_fitf([lambda_ax,0.,0.])
fprediction_qqqq = predict_fitf([lambda_ax,0.,1-4*0.2222222])
fprediction_mchi = predict_fitf([lambda_ax,+0.1,0.])
fprediction_pchi = predict_fitf([lambda_ax,-0.1,0.])

### Lpeak fits from Zappa 2017
def get_lpeak(kappal):
    q = 1
    L0 = 2.178e-2
    n1 = 5.24e-4
    n2 = -9.36e-8
    d1 = 2.77e-2
    nu = 0.25
    q_factor = q*q/(nu*nu)
    fits = ( 1 + n1*kappal + n2*kappal*kappal )  / ( 1 + d1*kappal  )
    return L0*fits#/q_factor

# sanity check
# for kappa^L_2~1472 --> Lpeak~8.168e-4
print('Lpeak check: ',get_lpeak(1472))


################################
# PLOTTING
################################
colordict = {
    '2B' : '#002F79',#[0,47,121],
    '2H' : '#0070FF',#[0,112,255],
    'ALF2': 'mediumslateblue',#[0,169,255],
    'ENG': '#00E8FF',#[0,232,255],
    'G2': '#00FA92',#[0,250,146],
    'G2k123': 'lightseagreen',
    'H4': '#46DD00',#[70,221,0],
    'MPA1': 'yellowgreen',#[125,248,3],
    'MS1': 'seagreen',#[186,255,59],
    'MS1b': 'gold',#[236,255,15],
    'SLy': 'orange',
    'BHBlp': '#FF6804',#[255,104,4],
    'DD2': '#FF006A',#[255,0,106],
    'LS220': 'hotpink',#[255,0,194],
    'SFHo': 'darkmagenta',#[182,74,248],
    'BLh': 'orchid',#[211,103,242],
    'BLQ': 'crimson',#[244,187,246],
    'SLy4': 'pink'
}
q = np.array(P['q'])
mtot = np.array(P['mass'])
nu = q_to_nu(np.array(P['q']))
fmrg = np.array(P['fmrg'])
amrg = np.array(P['amrg'])
#ferr = np.array(P['fm_error'])
#amerr= np.array(P['am_error'])
k2t = np.array(P['kap2t'])
eos_label = np.array(P['eos'])
lpeak = np.array(P['lpeak'])
erad_m = np.array(P['erad'])


#markero = markers.MarkerStyle(marker='o', fillstyle='none')

fig, axs = plt.subplots(2,2,figsize=(12,7))#plt.figure(figsize=(12,5))
lin1 = axs[0,0].plot(lambda_ax,prediction,'k-',label=r'$q=1, \hat{S}=0$')
lin2 = axs[0,0].plot(lambda_ax,prediction_qqqq,'k--',label=r'$q=2, \hat{S}=0$')
lin3 = axs[0,0].plot(lambda_ax,prediction_mchi,'k:',label=r'$q=1, \hat{S}=0.1$')
handles, labels = axs[0,0].get_legend_handles_labels()
leg1 = axs[0,0].legend(ncol=1, loc="upper right",handles=handles,labels=labels, frameon='False', fancybox='False', edgecolor='white')
axs[0,0].add_artist(leg1)
axs[0,1].plot(lambda_ax,fprediction,'k-',label=r'$q=1, \hat{S}=0$')
axs[0,1].plot(lambda_ax,fprediction_qqqq,'k--',label=r'$q=2, \hat{S}=0$')
axs[0,1].plot(lambda_ax,fprediction_mchi,'k:',label=r'$q=1, \hat{S}=0.1$')
handles, labels = axs[0,1].get_legend_handles_labels()
leg1 = axs[0,1].legend(ncol=1, loc="upper right",handles=handles,labels=labels, frameon='False', fancybox='False', edgecolor='white')
axs[0,1].add_artist(leg1)
axs[1,0].plot(lambda_ax*8,get_lpeak(lambda_ax*8),'k-',label=r'$q=1$')
handles, labels = axs[1,0].get_legend_handles_labels()
leg1 = axs[1,0].legend(ncol=1, loc="upper right",handles=handles,labels=labels, frameon='False', fancybox='False', edgecolor='white')
axs[1,0].add_artist(leg1)

eos_list = []
for i,eos in enumerate(eos_label):
    if eos in eos_list:
        axs[0,0].scatter(k2t[i],amrg[i], color=colordict[eos],marker='.')
    else:
        if eos=='BHBlp':
            lal = r'BHB$\Lambda\phi$'
        else:
            lal = eos
        axs[0,0].scatter(k2t[i],amrg[i], color=colordict[eos],marker='.',label=lal)
        eos_list.append(eos)

    axs[0,1].scatter(k2t[i],fmrg[i], color=colordict[eos],marker='.')
    axs[1,0].scatter(k2t[i]*8,lpeak[i],color=colordict[eos],marker='.')
    axs[1,1].scatter(k2t[i],erad_m[i],color=colordict[eos],marker='.')

axs[0,0].set_ylabel(r'$A_{\rm mrg}/M$')
axs[0,0].set_xlabel(r'$\kappa^T_2$')
axs[0,0].set_ylim([0.159,0.32])
axs[0,0].set_xlim([0,700])

axs[0,1].set_ylabel(r'$Mf_{\rm mrg}/\nu$')
axs[0,1].set_xlabel(r'$\kappa^T_2$')
axs[0,1].set_ylim([0.0554,0.141])
axs[0,1].set_xlim([0,700])

axs[1,0].set_ylabel(r'$L_{\rm peak}$')
axs[1,0].set_xlabel(r'$\kappa^L_2$')
#axs[1,0].set_ylim([0.03,0.126])
axs[1,0].set_xlim([0,4000])
axs[1,0].set_yscale('log')

axs[1,1].set_ylabel(r'$e^{\rm mrg}_{\rm GW}$')
axs[1,1].set_xlabel(r'$\kappa^T_2$')
axs[1,1].set_xlim([0,700])

handles, labels = axs[0,0].get_legend_handles_labels()
leg2 = axs[0,0].legend(handles=handles[3:],labels=labels[3:], bbox_to_anchor=(0, 1, 2.2, 0), loc="lower left", mode="expand",ncol=8, markerscale=2.5, frameon='False', fancybox='False', edgecolor='white')
plt.tight_layout()
plt.show()

quit()

####################
# fitting of Lpeak
####################
def new_fits(x,n1,n2,d1):
    BBH = 0.0217445683085
    return BBH*( 1 + n1*x + n2*x*x )  / ( 1 + d1*x  )

param, param_cov = curve_fit(new_fits,k2t,lpeak)
fits = new_fits(k2t,param[0],param[1],param[2])
plot_fits = new_fits(lambda_ax,param[0],param[1],param[2])

#get residual
residuals = (lpeak - fits)/lpeak
data_avrg = np.mean(lpeak)
SStot = np.sum((lpeak-data_avrg)**2)
SSres = np.sum((lpeak-fits)**2)
R2 = 1 - SSres/SStot
print('Rsquare: ',R2)

plt.scatter(k2t,lpeak,label=r'CoReDB',marker='.')
plt.plot(lambda_ax,plot_fits,label=r'Fits 2022',color='k')
plt.yscale('log')
plt.legend()
plt.xlabel(r'$\kappa^T_2$')
plt.ylabel(r'$L_{\rm peak}$')
plt.show()

