#!/usr/bin/env python

""" Fit models for luminosity peak """

__author__      = "F.Zappa (Parma U), S.Bernuzzi (Parma U & INFN)"
__copyright__   = "Copyright 2017"

import os
import sys
import matplotlib.pyplot as plt
import numpy as np
from scipy.optimize import leastsq
from scipy.optimize import curve_fit
from utils import *
import matplotlib.patches as mpatches
import matplotlib.lines as mlines
from errors import *

#~ def funct_curve(x, pars0, pars1): #2 pars. 5 residuals over 30%, one at 75%, Rsquare = 0.912
    #~ BBH = 0.0217445683085
    #~ return BBH * (1. + x * pars0) / (1 + x * pars1)

def funct_curve(x, pars0, pars1, pars2): #3 pars. One residual at 65%, one at 31%, Rsquare = 0.944
    BBH = 0.0217445683085
    return BBH * (1. + x * pars0 + x**2 * pars1) / (1 + x * pars2)

if __name__=='__main__':

        ###################################################################
    temp = np.genfromtxt('../dat/simu_quantities.txt', dtype = 'str')
    alldirs = [line[-1] for line in temp]
    dirs = one_resol_simu(alldirs)
    print len(dirs)

    q = []
    nu = []
    stable = []
    ktid = []
    keff = []
    Lpeak = []
    LpeakBBH = []
    simname = []

    for dir in dirs:
        for line in temp:
            if line[2] == '1' and not float(line[10])>1 and dir == line[-1] and not line.__contains__('BAM/SLy_1.350_1.350_0.00_0.00_0.0379_0.059'):
                q.append(float(line[0]))
                nu.append(float(line[1]))
                stable.append(int(line[3]))
                ktid.append(float(line[4]))
                keff.append(float(line[5]))
                Lpeak.append(float(line[10]))
                LpeakBBH.append(float(line[11]))
                simname.append(dir)
                break
    q = np.array(q)
    nu = np.array(nu)
    stable = np.array(stable)
    ktid = np.array(ktid)
    keff = np.array(keff)
    Lpeak = np.array(Lpeak)
    LpeakBBH = np.array(LpeakBBH)

    N_irrot = len(q)





    norm = nu**2 * q**(-2)
    #~ norm = q**(-1.5)

    kgeneric = keff #It can be chosen ktid or keff calculated from the theory

    BBH = np.mean(LpeakBBH/norm)
    errBBH = np.std(LpeakBBH/norm)
    print 'BBH value (kT2 = 0): ', BBH
    print 'Error (std): ', errBBH

    Lpeak_n = Lpeak/norm
    #~ for i in range(len(Lpeak)):
        #~ print '\n', Lpeak_n[i], ' ', simname[i].split('/')[-1], ' ', kgeneric[i]

    if 0:
        plt.figure(1)
        plt.plot(kgeneric, Lpeak, 'b.', label = 'Data')
        if spin0.__contains__(0):
            plt.plot(kgeneric[0], Lpeak[0], 'r.', label = 'Data')
            for i in range(1, len(Lpeak)):
                if irrot[i] == 0:
                    plt.plot(kgeneric[i], Lpeak[i], 'r.')
        plt.show()

    ###################################################################
    # fit

    print '\n ==> fit Lpeak\n'

    # fit 1D, only kappa:
    eps = 1.e-10
    vars = 1., 1., 1.
    #~ vars = 1., 1.

    sigma = np.ones_like(kgeneric)*1e-9
    sigma[kgeneric>3000] = 0.1e-9
    btw_1500_1800 = list(np.array([kgeneric>1500]) & np.array([kgeneric<1800]) & np.array([stable == 1]))
    sigma[btw_1500_1800] = 0.3e-9

    popt, pcov = curve_fit(funct_curve, kgeneric, Lpeak_n, vars, sigma=sigma, absolute_sigma = False)

    perr = np.sqrt(np.diag(pcov))

    print '\nInitial guess'
    print vars
    print '\nBest pars:'
    print list(popt)
    print '\nErrors:'
    print np.sqrt(np.diag(pcov))



        # residual
        fit_eval = funct_curve(kgeneric, *popt)
    residuals = (Lpeak_n - fit_eval)/Lpeak_n


    data_average = np.mean(Lpeak_n)
    SStot = np.sum((Lpeak_n-data_average)**2)
    SSres = np.sum((Lpeak_n-fit_eval)**2)
    R_square = 1 - SSres/SStot
    print '\nRsquare:\n', R_square, '\n'

    print '\nWorse largest violation'
        for i in range(len(simname)):
        if np.abs(residuals[i])>0.3:
            print residuals[i],kgeneric[i],simname[i].split('/')[-1]

    #~ uga

    k_fit = np.linspace(0., 4000, 1000)
        Lp_fit = funct_curve(k_fit, *popt)


        Lplanck =  3.628504984913064e59 #erg/s

        mk_size = 7 # for scatter plot

    #~ f, (ax1, ax2, ax3) = plt.subplots(3, sharex=True, gridspec_kw={'height_ratios':[3,1, 1]}, figsize=(11,11./1.618))
    f, (ax1, ax2) = plt.subplots(2, sharex=True, gridspec_kw={'height_ratios':[2,1]}, figsize=(11,11./1.618))

    ax1.semilogy(k_fit, Lp_fit, 'r--')

    LBBH_min = np.min(LpeakBBH/norm)
    LBBH_max = np.max(LpeakBBH/norm)

    k = np.linspace(0, 4000, 8)
    ax1.errorbar(k, LBBH_min*np.ones(8), yerr=0.015*np.array([0,1,1,1,1,1,1,0]), lolims = [0,1,1,1,1,1,1,0], color='black', linestyle='--', linewidth = 2, markeredgewidth = 2)


    ax1.fill_between(k, LBBH_min*np.ones(8),LBBH_max*np.ones(8), color='lightgray')

    lum = []
    simall = []
    with open('../dat/simu_quantities.txt') as file:
        for line in file:
            if not line[0] == '#':
                aux = line.split()
                simall.append(aux[-1])
                lum.append(float(aux[10]))
    lum = np.array(lum)

    errLum = errors(lum, simname, simall)
    errLum_n = errLum/norm
    rel_err = errLum_n / fit_eval

        f.subplots_adjust(hspace=0.0, bottom= 0.15, left = 0.12, right = 0.88, top = 0.99)

    stab = [0, 1, 2, 3]
    orange = '#FF9900'
    cols = ['m', 'b', orange, 'g']
    labs = ['BH', 'HMNS', 'SMNS', 'MNS']
    markers = ['o', 'v']
    cods = ['BAM', 'THC']



    for i in range(len(Lpeak)):
        col = cols[stab.index(stable[i])]
        if simname[i].__contains__('BAM'):
            mark = 'o'
        else:
            mark = 'v'
        ax1.semilogy(kgeneric[i], Lpeak_n[i], marker = mark, color = col, markersize = mk_size)
        ax2.plot(kgeneric[i], residuals[i], marker = mark, color = col, markersize = mk_size)
        ax1.errorbar(kgeneric[i], Lpeak_n[i], errLum_n[i], ls = 'None', color = col, capthick=0, elinewidth = 1.5)
        ax2.errorbar(kgeneric[i], residuals[i], rel_err[i], ls = 'None', color = col, capthick=0, elinewidth = 1.5)
        #~ ax3.plot(kgeneric[i], residuals[i], marker = mark, color = col, markersize = mk_size)

    ax1.set_ylim(10**-4/1.3, BBH*2)
    ylim = np.max(np.abs(residuals)) + 0.1
    ax2.set_ylim(-ylim, +ylim)
    #~ ax3.set_ylim(-ylim, +ylim)



    ####Errors




    fontsz = 30

    leg1 = ax1.legend(loc = 'center right', numpoints = 1, fancybox=True, ncol = 2, bbox_to_anchor=(1.01,0.63), fontsize = 17,
    handles = [mpatches.Patch(color=col, label = labs[cols.index(col)]) for col in cols] +
    [mlines.Line2D([], [], color='white', marker=mark, markersize=mk_size, label = cods[markers.index(mark)], ls = 'None') for mark in markers] +
    [mlines.Line2D([], [], color='r', ls='--', label = 'Fit'),
    mlines.Line2D([], [], color='k', label = 'BBH [Keitel et al. 2017]', ls = '--', linewidth = 2)])
    leg1.get_frame().set_alpha(0.1)

    ax12 = ax1.twinx()
    ax12.semilogy(kgeneric,Lpeak_n*Lplanck, 'bo', alpha = 0)

    ax1.set_ylabel(r'$L_{\rm peak}q^2/\nu^{2}$', fontsize = fontsz)
    ax12.set_ylabel(r'$L_{\rm peak}q^2/\nu^{2}$' + '$[erg/s] $', fontsize = fontsz)


    ax12.set_ylim(10**-4/1.3*Lplanck, BBH*Lplanck*2)
        ax2.set_ylabel(r'${\rm Rel. Diff.}$', fontsize = fontsz)

    q = []
    nu = []
    stable = []
    ktid = []
    keff = []
    Lpeak = []
    LpeakBBH = []
    simname = []

    for dir in dirs:
        for line in temp:
            if not line[2] == '1' and not float(line[10])>1 and dir == line[-1] and not line.__contains__('BAM/H4_1.651_1.100_0.10_-0.10_0.0350_0.167'):
                q.append(float(line[0]))
                nu.append(float(line[1]))
                stable.append(int(line[3]))
                ktid.append(float(line[4]))
                keff.append(float(line[5]))
                Lpeak.append(float(line[10]))
                LpeakBBH.append(float(line[11]))
                simname.append(dir)
                break
    q = np.array(q)
    nu = np.array(nu)
    stable = np.array(stable)
    ktid = np.array(ktid)
    keff = np.array(keff)
    Lpeak = np.array(Lpeak)
    LpeakBBH = np.array(LpeakBBH)


    for name in sorted(simname):
        print name
    #~ uga

    N_rot = len(q)

    print "Total number: ", N_irrot + N_rot
    print "Spin number: ", N_rot

    lum = []
    simall = []
    with open('../dat/simu_quantities.txt') as file:
        for line in file:
            if not line[0] == '#':
                aux = line.split()
                simall.append(aux[-1])
                lum.append(float(aux[10]))
    lum = np.array(lum)



    #~ ax3.plot(np.arange(0,4000), np.zeros(4000), 'k--')
    ax2.plot(np.arange(0,4000), np.zeros(4000), 'k--')
    ax2.set_yticks(np.arange(-0.5,0.7,0.5))
    ax2.set_yticks(np.arange(-0.8,0.8,0.1), minor = True)
    ax2.yaxis.set_ticks_position("left")


    #~ ax3.yaxis.tick_right()
    #~ ax3.yaxis.set_label_position("right")
    #~ ax3.yaxis.tick_right()
    #~ ax3.set_yticks(np.arange(-0.5,0.7,0.5))
    #~ ax3.set_yticks(np.arange(-0.8,0.8,0.1), minor = True)
    #~ ax3.set_ylabel(r'${\rm Rel. Diff.}$', fontsize = fontsz)

    TICKFONTSZ = 20

    #~ ax3.tick_params('y', labelsize = TICKFONTSZ, labelright = 'on', labelleft = 'off')


    ax12.tick_params('y', labelsize = TICKFONTSZ, labelright = 'on', labelleft = 'off')


    fit_eval = funct_curve(keff, *popt)
    SPINLUM = Lpeak/q**-2/nu**2
    residuals = (SPINLUM - fit_eval)/SPINLUM

    print '\nWorse largest violation'
        for i in range(len(simname)):
        if np.abs(residuals[i])>0.1:
            print residuals[i],kgeneric[i],simname[i].split('/')[-1]

    norm = q**-2*nu**2

    errLum = errors(lum, simname, simall)
    rel_err = errLum/norm / fit_eval

    ind_max = np.argmax(rel_err)

    ax2.plot(keff, residuals, 'xr', markersize = mk_size-2, label = 'Spin data', markeredgewidth = 1.5)
    ax2.errorbar(keff, residuals, rel_err, ls = 'None', color = 'red', capthick=0, elinewidth = 1.5)
    leg2 = ax2.legend(loc = 'best', numpoints = 1, fancybox=True, fontsize = 17)
    leg2.get_frame().set_alpha(0.3)
    ax2.set_xlabel(r'$\kappa^{\rm L}_2$', fontsize = fontsz)

    axes = [ax1, ax2]




    for tick in ax2.xaxis.get_major_ticks():
        tick.label.set_fontsize(TICKFONTSZ)


    for ax in axes:
        for tick in ax.yaxis.get_major_ticks():
            tick.label.set_fontsize(TICKFONTSZ)



    #~ for tick in ax3.yaxis.get_major_ticks():
        #~ tick.label.set_fontsize(20)






        plt.savefig('../fig/' + sys.argv[0].replace(".py", "") + ".png", format = 'png')
    plt.savefig('../fig/' + sys.argv[0].replace(".py", "") + ".pdf", format = 'pdf')
    plt.show()
