from re import M
import string
from turtle import color
import numpy as np
import matplotlib.pyplot as plt
import matplotlib
import pandas as pd
import csv
from watpy.utils.ioutils import *
from watpy.coredb.coredb import *
from watpy.utils.coreh5 import *
import matplotlib.ticker as ticker

import sys
sys.path.append('../')
import csv
from handy_functions import *

matplotlib.rcParams['text.usetex']= True
matplotlib.rcParams['font.serif']= 'Times New Roman'
matplotlib.rcParams['font.size']= 15 #28

##############################
# DATA
##############################

#sim_path    = '/home/agonzalez/Documents/git/watpy/msc/CoRe_DB_clone/' #previous release
sim_path = '/data/numrel/DATABASE/CoRe_DB_clone/'
rel2_path   = '/data/numrel/DATABASE/Release02/'

cdb = CoRe_db(sim_path)
idb = cdb.idb

dirc = ['THC:0107','BAM:0094','BAM:0142','BAM:0113']
charac = ['High mass','High mass ratio','Precessing','Eccentric']
pmtype = ['Prompt collapse','Tidally-disrupted','Short-lived','Long-lived']
runc = ['R01','R01','R01','R01']
end = [450,4390,4160,8000] #960

text = [r'\texttt{THC:0107}, BLh, $M=3.2~M_{\odot}$, $q=1$, $\kappa^T_2=31$', # HLLE-MP5, M0
        r'\texttt{BAM:0094}, MS1b, $M=2.9~M_{\odot}$, $q=2.1$, $\kappa^T_2=251$', # LLF-WENOZ
        r'\texttt{BAM:0142}, SLy, $M=2.7~M_{\odot}$, $q=1$, $\kappa^T_2=73$', # H0_LLF-WENOZ
        r'\texttt{BAM:0113}, SLy, $M=2.7~M_{\odot}$, $q=1$, $\kappa^T_2=70$'] # LLF-WENOZ
tx = [11,100,80,120]
ty = [-0.26,-0.155,-0.24,-0.24]

#c_insp = ['#001CFF','#6FD904','#FFB10D','#BC50F7']
c_insp = ['#f1a340','#7fbf7b','#af8dc3','#ef8a62']
#c_pm = ['#000D6E','#53A303','#D9980B','#8638B0']
c_pm = ['#b35806','#1b7837','#762a83','#b2182b']

################################
# PLOTTING
################################
i=0
fig, axs = plt.subplots(len(dirc), 2,figsize=(10,7),gridspec_kw={'width_ratios': [1.5, 1]})
for ss in dirc:
    if ss in idb.dbkeys: # first release
        sim = cdb.sim[ss]
        simrun = sim.run[runc[i]]
        #simrun2 = sim.run[runc2[i]]
    else: #second release
        ssd=ss.replace(':','_')
        simrun = CoRe_run(rel2_path+ssd+'/'+runc[i])
        #simrun2 = CoRe_run(rel2_path+ssd+'/'+runc2[i])

    ## first res
    datah5 = simrun.data
    dseth5 = datah5.read_dset()

    rh22_files = list(dseth5['rh_22'].keys())[0] # different distances and take smallest distance
    print(rh22_files)
    dset = dseth5['rh_22'][rh22_files] 

    # read
    hp          = dset[:,1]
    hc          = dset[:,2]
    t           = dset[:,0]
    ht          = hp + 1j*hc
    amp         = np.abs(ht)
    tmrg        = t[np.argmax(amp)]

    # only for BAM:0139
    if ss=='BAM:0139':
        hp = hp/900

    '''
    ## second res
    datah52 = simrun2.data
    dseth52 = datah52.read_dset()

    rh22_files2 = list(dseth52['rh_22'].keys())[0] # different distances and take smallest distance
    dset2 = dseth52['rh_22'][rh22_files2] 

    # read
    hp2          = dset2[:,1]
    hc2          = dset2[:,2]
    t2           = dset2[:,0]
    ht2          = hp2 + 1j*hc2
    '''

    #axs[i,0].set_title(ss)
    #axs[i,0].grid(True)
    axs[i,0].plot(t,hp,linestyle='-',color=c_insp[i],label=charac[i])
    axs[i,0].plot(t[t>tmrg],hp[t>tmrg],linestyle='--',color=c_pm[i])
    axs[i,0].legend(loc='upper left',fontsize='small')
    axs[i,0].set_xlim([0,end[i]])
    axs[i,0].set_ylabel(r'$Re(Rh_{22})/M$')
    axs[i,0].text(tx[i],ty[i],text[i],fontsize='small')

    #axs[i,1].grid(True)
    axs[i,1].plot(t,hp,linestyle='--',color=c_pm[i],label=pmtype[i])
    axs[i,1].legend(loc='upper right',fontsize='small')
    axs[i,1].set_xlim([tmrg,end[i]])
    axs[i,1].yaxis.set_major_locator(ticker.NullLocator())
    axs[i,0].text(tx[i],ty[i],text[i],fontsize='small')
    i=i+1
axs[3,0].set_xlabel(r'$u/M$')
axs[3,1].set_xlabel(r'$u/M$')
plt.subplots_adjust(wspace=0.0)
plt.tight_layout()
plt.savefig('waveforms.pdf')
plt.show()

