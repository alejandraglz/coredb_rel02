from cmath import nan
from re import M, X
import string
from turtle import color, ht
import numpy as np
import matplotlib.pyplot as plt
from matplotlib import cm, markers
from matplotlib.ticker import (MultipleLocator, AutoMinorLocator)
import array
import matplotlib
import pandas as pd
import csv
from watpy.utils.ioutils import *
import scipy.stats as st
import mplcursors
import os
from scipy.stats import t
from bajes.obs.gw.utils import lambda_2_kappa

import sys
sys.path.append('../')
import csv
from handy_functions import *

matplotlib.rcParams['text.usetex']= True
matplotlib.rcParams['font.serif']= 'Times New Roman'
matplotlib.rcParams['font.size']= 15 #28

##############################
# DATA
##############################

data = csv_reader("../data/database.csv")

P = get_df(data) #ready to go pandas df
#P.index = np.arange(1, len(P)+1)

## Add info about every run as a list
json_file = "../breschi_fits/results/fits_results.json"
jdict_list = read_json_into_dict(json_file)

reso = []
f2p = []
PC = []
for ip, pp in enumerate(P['name']):
    ss = []#""
    ff = []
    ppc = []
    for dict in jdict_list:
        if not dict==None:
            if dict['database_key'].startswith(pp):
                ss.append(float(dict["resolution"])) #= ss + str(dict["resolution"]) + ' '
                if 'f_merg' in dict.keys():
                    ff.append(float(dict['f_merg']))
                else:
                    ff.append(nan)
                if 'a_merg' in dict.keys():
                    ppc.append(float(dict['a_merg']))
                else:
                    ppc.append(nan)
    reso.append(ss)  
    f2p.append(ff)
    PC.append(ppc)

reso = pd.Series(reso)
reso = reso.to_frame()
reso.columns=['resolution']
f2p = pd.Series(f2p)
f2p = f2p.to_frame()
f2p.columns=['f_merg']
PC = pd.Series(PC)
PC = PC.to_frame()
PC.columns=['a_merg']

P['resolution'] = reso
P['f_merg'] = f2p
P['a_merg'] = PC


f2_hr = []
f2_error = []
am_hr = []
am_error = []
for ip, pp in enumerate(P['resolution']):
    try:
        highres = np.argmin(pp)
        f2_ = P.at[ip,'f_merg'][highres]
        f2__ = P.at[ip,'f_merg'][np.argmax(pp)]
        f2_hr.append(f2_)
        am_ = P.at[ip,'a_merg'][highres]
        am_hr.append(am_)
        err = 0.5*(np.max(P.at[ip,'f_merg'])-np.min(P.at[ip,'f_merg']))
        aerr = 0.5*(np.max(P.at[ip,'a_merg'])-np.min(P.at[ip,'a_merg']))
        if aerr>0.01:
            am_error.append(0)
        else:
            am_error.append(aerr)
        if err>0.004:
            f2_error.append(0)
        else:
            f2_error.append(err)
    except ValueError:
        f2_hr.append(nan)
        f2_error.append(0)
        am_hr.append(nan)
        am_error.append(0)

    
f2_hr = pd.Series(f2_hr)
f2_hr = f2_hr.to_frame()
f2_hr.columns=['fm_HR']
f2e = pd.Series(f2_error)
f2e = f2e.to_frame()
f2e.columns=['fm_error']
PC_hr = pd.Series(am_hr)
PC_hr = PC_hr.to_frame()
PC_hr.columns=['am_HR']
e_hr = pd.Series(am_error)
e_hr = e_hr.to_frame()
e_hr.columns=['am_error']

P['fm_HR'] = f2_hr
P['fm_error'] = f2e
P['am_HR'] = PC_hr
P['am_error'] = e_hr

#faulty_wvfs = ['THC:0052','THC:0054','THC:0098','THC:0042','THC:0056','THC:0068','THC:0069','THC:0079']
#no_eos = ['G2','G2k123']
phase_transitions = ['THC:0037','THC:0058','THC:0059','THC:0060','THC:0061','THC:0062','THC:0063','THC:0064','THC:0070','THC:0071','THC:0072','THC:0102','THC:0103','THC:0104','THC:0105','THC:0106','THC:0107','THC:0109','THC:0110']
P.loc[P["name"].isin(phase_transitions), "f_merg"] = nan
P.loc[P["name"].isin(phase_transitions), "a_merg"] = nan
#P.loc[P["name"].isin(['BAM:0047','THC:0024','THC:0091','THC:0092','THC:0097']), "f_merg"] = nan
#P.loc[P["name"].isin(['BAM:0047','THC:0024','THC:0091','THC:0092','THC:0097']), "a_merg"] = nan
# correcting eos names
P.loc[P["eos"]=="sly", "eos"] = 'SLy'
P.loc[P["eos"]=="SLY", "eos"] = 'SLy'
P.loc[P["eos"]=="MS1B", "eos"] = 'MS1b'
# filtering data
P = P.loc[P['name']!='THC:0052'] # faulty sims (already removed in tullio)
P = P.loc[P['name']!='THC:0056'] # delete ..
P = P.loc[P['name']!='THC:0069'] # .. this once you correct the stuff in the json too
P = P.loc[P['eos']!='G2']      # not on matteo's paper
P = P.loc[P['eos']!='G2k123']  # ''
#P = P.loc[P['q']==1]     
P = P.loc[P['eos']!='SLy4']
print(P)
#print(P.loc[[124]])


###
# fits for Amrg
a0,a1,b0,b1,n1,n2,d1,d2,q1,q2,q3,q4 = (0.39475762,-1.13292325,-0.02991597,-2.59318042,
                                       0.03901957425837708,5.184564561045753e-05,0.06032721528620493,0.00013795694839938442,
                                       10.410256292591564,54.513466134598985,10.826296683028199,54.53588176973234)
nu      = np.array(P['q'])/(1+np.array(P['q']))**2
delta   = np.sqrt(1.0 - 4.0*nu)
m1M     = 0.5*(1.0 + delta)
m2M     = 0.5*(1.0 - delta)
Shat    = m1M*m1M*np.array(P['chiAz']) + m2M*m2M*np.array(P['chiBz'])
Xnu     = 1. - 4.*nu
# compare & plot
def predict_fit(xxx):
    kkk,sss,qqq = xxx
    #qqq =  1. - 4.*(qqq/(1+qqq)**2)
    p1s = b0 * (1 + b1 * qqq)
    _n1 = n1 * (1 + q1 * qqq)
    _n2 = n2 * (1 + q2 * qqq)
    _d1 = d1 * (1 + q3 * qqq)
    _d2 = d2 * (1 + q4 * qqq)
    _up = (1.+ _n1 * kkk + _n2 * kkk **2)
    _lo = (1.+ _d1 * kkk + _d2 * kkk **2)
    return a0 * (1 + a1*qqq) * (1 + p1s*sss) *  _up / _lo

vars = [np.array(P['kap2t']),Shat,Xnu]
fit_data = predict_fit(vars)
lambda_ax       = np.linspace(0,700, 5000)
prediction      = predict_fit([lambda_ax,0.,0.])
prediction_qqqq = predict_fit([lambda_ax,0.,1-4*0.2222222])
prediction_mchi = predict_fit([lambda_ax,+0.1,0.])
prediction_pchi = predict_fit([lambda_ax,-0.1,0.])

###
# fits for Fmrg
a0,a1,b0,b1,n1,n2,d1,d2,q1,q2,q3,q4 = (0.22754806,0.92330138,0.59374838,-1.99378496,
                                       0.03445340731627873,5.5799962023491245e-06,0.08404652974611324,0.00011328992320789428,
                                       13.828175998146255,517.4149218303298,12.74661916436829,139.76057108600236)
# compare & plot
def predict_fitf(xxx):
    kkk,sss,qqq = xxx
    #qqq =  1. - 4.*(qqq/(1+qqq)**2)
    p1s = b0 * (1 + b1 * qqq)
    _n1 = n1 * (1 + q1 * qqq)
    _n2 = n2 * (1 + q2 * qqq)
    _d1 = d1 * (1 + q3 * qqq)
    _d2 = d2 * (1 + q4 * qqq)
    _up = (1.+ _n1 * kkk + _n2 * kkk **2)
    _lo = (1.+ _d1 * kkk + _d2 * kkk **2)
    return a0 * (1 + a1*qqq) * (1 + p1s*sss) *  _up / _lo

fprediction      = predict_fitf([lambda_ax,0.,0.])
fprediction_qqqq = predict_fitf([lambda_ax,0.,1-4*0.2222222])
fprediction_mchi = predict_fitf([lambda_ax,+0.1,0.])
fprediction_pchi = predict_fitf([lambda_ax,-0.1,0.])

################################
# PLOTTING
################################
colordict = {
    '2B' : '#002F79',#[0,47,121],
    '2H' : '#0070FF',#[0,112,255],
    'ALF2': 'mediumslateblue',#[0,169,255],
    'ENG': '#00E8FF',#[0,232,255],
    'G2': '#00FA92',#[0,250,146],
    'G2k123': 'lightseagreen',
    'H4': '#46DD00',#[70,221,0],
    'MPA1': 'yellowgreen',#[125,248,3],
    'MS1': 'seagreen',#[186,255,59],
    'MS1b': 'gold',#[236,255,15],
    'SLy': 'orange',
    'BHBlp': '#FF6804',#[255,104,4],
    'DD2': '#FF006A',#[255,0,106],
    'LS220': 'hotpink',#[255,0,194],
    'SFHo': 'darkmagenta',#[182,74,248],
    'BLh': 'orchid',#[211,103,242],
    'BLQ': 'crimson',#[244,187,246],
    'SLy4': 'pink'
}
mtot = np.array(P['mass'])
nu = q_to_nu(np.array(P['q']))
fmrg = np.array(P['fm_HR'])
amrg = np.array(P['am_HR'])
ferr = np.array(P['fm_error'])
amerr= np.array(P['am_error'])
k2t = np.array(P['kap2t'])
eos_label = np.array(P['eos'])

markero = markers.MarkerStyle(marker='o', fillstyle='none')

fig, axs = plt.subplots(1,3,figsize=(12,5))#plt.figure(figsize=(12,5))
lin1 = axs[0].plot(lambda_ax,prediction,'k-',label=r'$q=1, \hat{S}=0$')
lin2 = axs[0].plot(lambda_ax,prediction_qqqq,'k--',label=r'$q=2, \hat{S}=0$')
lin3 = axs[0].plot(lambda_ax,prediction_mchi,'k:',label=r'$q=1, \hat{S}=0.1$')
handles, labels = axs[0].get_legend_handles_labels()
leg1 = axs[0].legend(ncol=1, loc="upper right",handles=handles,labels=labels, frameon='False', fancybox='False', edgecolor='white')
axs[0].add_artist(leg1)
axs[1].plot(lambda_ax,fprediction,'k-',label=r'$q=1, \hat{S}=0$')
axs[1].plot(lambda_ax,fprediction_qqqq,'k--',label=r'$q=2, \hat{S}=0$')
axs[1].plot(lambda_ax,fprediction_mchi,'k:',label=r'$q=1, \hat{S}=0.1$')
handles, labels = axs[1].get_legend_handles_labels()
leg1 = axs[1].legend(ncol=1, loc="upper right",handles=handles,labels=labels, frameon='False', fancybox='False', edgecolor='white')
axs[1].add_artist(leg1)
eos_list = []
for i,eos in enumerate(eos_label):
    if eos in eos_list:
        axs[0].errorbar(k2t[i],amrg[i], yerr=amerr[i], color=colordict[eos],fmt='.', capsize=1.5)
    else:
        if eos=='BHBlp':
            lal = r'BHB$\Lambda\phi$'
        else:
            lal = eos
        axs[0].errorbar(k2t[i],amrg[i], yerr=amerr[i], color=colordict[eos],fmt='.', capsize=1.5,label=lal)
        eos_list.append(eos)

    axs[1].errorbar(k2t[i],fmrg[i], yerr=ferr[i], color=colordict[eos],fmt='.', capsize=1.5)

axs[0].set_ylabel(r'$A_{\rm mrg}/M$')
axs[0].set_xlabel(r'$\kappa^T_2$')
axs[0].set_ylim([0.159,0.32])
axs[0].set_xlim([0,700])

axs[1].set_ylabel(r'$Mf_{\rm mrg}/\nu$')
axs[1].set_xlabel(r'$\kappa^T_2$')
axs[1].set_ylim([0.03,0.126])
axs[1].set_xlim([0,700])

axs[2].set_ylabel(r'$L_{\rm peak}$')
axs[2].set_xlabel(r'$\kappa^T_2$')
#axs[2].set_ylim([0.03,0.126])
axs[2].set_xlim([0,700])

handles, labels = axs[0].get_legend_handles_labels()
leg2 = axs[0].legend(handles=handles[3:],labels=labels[3:], bbox_to_anchor=(0, 1, 3.5, 0), loc="lower left", mode="expand",ncol=8, markerscale=2.5, frameon='False', fancybox='False', edgecolor='white')
plt.tight_layout()
plt.show()