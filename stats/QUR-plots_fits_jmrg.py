from cmath import nan
import matplotlib
import matplotlib.pyplot as plt
#matplotlib.use('TkAgg')
import numpy as np 
#from pycbc.types.frequencyseries import FrequencySeries 
import json
import h5py 
import pandas as pd
from matplotlib.ticker import (MultipleLocator, AutoMinorLocator)
from scipy.optimize import curve_fit

from pycbc.filter import match, overlap, optimized_match
#from pycbc.filter.matchedfilter import optimized_match
from pycbc.types.timeseries import TimeSeries
from pycbc.psd import  aLIGODesignSensitivityP1200087, AdVDesignSensitivityP1200087, EinsteinTelescopeP1600143, aLIGOZeroDetHighPower #choose your PSD
from pycbc import waveform

from watpy.coredb.coredb import *
from watpy.wave.wave import *
from watpy.utils.coreh5 import *
from watpy.utils.ioutils import *

import sys
import os
sys.path.append('../')
from mismatch import *
from handy_functions import *

matplotlib.rcParams['text.usetex']= True
#matplotlib.rcParams['font.family'] = ['serif']
matplotlib.rcParams['font.serif']= 'Times New Roman'
matplotlib.rcParams['font.size']= 15 #28


#sim_path    = '/home/agonzalez/Documents/git/watpy/msc/CoRe_DB_clone/'
sim_path    = '/data/numrel/DATABASE/CoRe_DB_clone/'
rel2_path   = '/data/numrel/DATABASE/Release02/'

data = csv_reader("../data/database.csv")
P = get_df(data) #ready to go pandas df
#cdb = CoRe_db(sim_path)
#idb = cdb.idb

## Add info about every run as a list
json_file = "../breschi_fits/fits_data.json"
jdict_list = read_json_into_dict(json_file)

amrg = []
fmrg = []
lpeak = []
ermrg = []
weird_cases = ['THC:0076']
phase_transitions = ['THC:0037','THC:0056','THC:0057','THC:0058','THC:0059','THC:0060','THC:0061','THC:0062','THC:0067','THC:0068','THC:0069','THC:0099','THC:0100','THC:0101','THC:0102','THC:0103','THC:0104','THC:0106','THC:0107']
precessing = ['BAM:0110','BAM:0111','BAM:0143','BAM:0146','BAM:0147']
faulty = ['THC:0044','THC:0045','THC:0046','THC:0047','THC:0048','THC:0049','THC:0050','THC:0051','THC:0070','THC:0071','THC:0072','THC:0073','THC:0074','THC:0075','THC:0076'] # tiny amplitude

for i_dbkey, entry in enumerate(P['name']):
    print("Working with: ",entry)
    aa = []
    ff = []
    lp = []
    er = []
    res = []
    for dict in jdict_list:
        if not dict==None:
            if dict['database_key'].startswith(entry):
                res.append(float(dict["resolution"]))
                if 'f_merg' in dict.keys() and (entry not in phase_transitions) and (entry not in precessing) and (entry not in faulty):
                    aa.append(float(dict['a_merg']))
                    ff.append(float(dict['f_merg'])*(2*np.pi)*q_to_nu(float(dict['q'])))
                else:
                    aa.append(np.nan)
                    ff.append(np.nan)
                if 'jr_merg' in dict.keys() and (entry not in phase_transitions) and (entry not in precessing) and (entry not in faulty):
                    lp.append(float(dict['jorb_merg']))
                    er.append(float(dict['jr_merg']))
                else:
                    lp.append(np.nan)
                    er.append(np.nan)
    #print(lp)
    try:
        highres = np.argmin(res)
        if entry=='BAM:0097':
            amrg.append(aa[0])
        elif entry=='BAM:0141':
            amrg.append(aa[2])
        else:
            amrg.append(aa[highres])  
        fmrg.append(ff[highres])
        #if lp[highres]<0.048 or entry=='BAM:0097':
        #    lpeak.append(np.nan)
        #else:
        #    lpeak.append(lp[highres])
        #if er[highres]>0.0:
        #    ermrg.append(np.nan)
        #else:
        #    ermrg.append(er[highres])
        ermrg.append(er[highres])
        lpeak.append(lp[highres])
    except ValueError:
        amrg.append(np.nan)  
        fmrg.append(np.nan)
        lpeak.append(np.nan)
        ermrg.append(np.nan)


print(lpeak)  
orbs = pd.Series(amrg)
orbs = orbs.to_frame()
orbs.columns=['amrg']
P['amrg'] = orbs

radd = pd.Series(fmrg)
radd = radd.to_frame()
radd.columns=['fmrg']
P['fmrg']=radd

add = pd.Series(lpeak)
add = add.to_frame()
add.columns=['jorb_mrg']
P['jorb_mrg']=add

erad = pd.Series(ermrg)
erad = erad.to_frame()
erad.columns=['jr_mrg']
P['jr_mrg'] = erad

'''
P.loc[P['amrg']>0.32,'amrg'] = np.nan
P.loc[P['amrg']<0.15,'amrg'] = np.nan
P.loc[P['fmrg']>0.141,'fmrg'] = np.nan
P.loc[P['fmrg']<0.05,'fmrg'] = np.nan
P.loc[P['lpeak']>0.32,'lpeak'] = np.nan
P.loc[P['lpeak']<0.044,'lpeak'] = np.nan
P.loc[P['erad']>0.04,'erad'] = np.nan
P.loc[P['erad']<0.0044,'erad'] = np.nan
'''
P.loc[P['name']=='BAM:0136','lpeak'] = np.nan
P.loc[P['name']=='THC:0085','lpeak'] = np.nan
#P.loc[P['ebin']>-0.03,'ebin'] = np.nan
P.loc[P['kap2t']>700,'jorb_mrg'] = np.nan
P.loc[P["eos"]=="sly", "eos"] = 'SLy'
P.loc[P["eos"]=="SLY", "eos"] = 'SLy'
P.loc[P["eos"]=="MS1B", "eos"] = 'MS1b'
P = P.loc[P['eos']!='G2']      # not on matteo's paper
P = P.loc[P['eos']!='G2k123']  # ''
P = P.loc[P['eos']!='SLy4']
#P = P.loc[P['name']!='BAM:0097'] # dont fall on the trend
#P = P.loc[P['name']!='THC:0066'] # "
#P = P.loc[P['name']!='THC:0040'] # kappa too high >> messing with the fits
#P = P.loc[P['name']!='BAM:0139'] #"
#P = P.loc[P['name']!='THC:0055']
#P = P.loc[P['name']!='THC:0041'] # kappa too high >> messing with the fits
#P = P.loc[P['name']!='THC:0042'] #"
#P = P.loc[P['name']!='THC:0043']
#P = P.loc[P['q']==1] 
#P = P.loc[P['chieff']==0]

#P.dropna()
#P=P.fillna(0)
#print(P.loc[P['ebin']>0])
print(P.loc[(P['lpeak']<0.097)&(P['kap2t']<85)])
print(P)

###
# fits for Amrg
a0,a1,b0,b1,n1,n2,d1,d2,q1,q2,q3,q4 = (0.39475762,-1.13292325,-0.02991597,-2.59318042,
                                       0.03901957425837708,5.184564561045753e-05,0.06032721528620493,0.00013795694839938442,
                                       10.410256292591564,54.513466134598985,10.826296683028199,54.53588176973234)
nu      = np.array(P['q'])/(1+np.array(P['q']))**2
delta   = np.sqrt(1.0 - 4.0*nu)
m1M     = 0.5*(1.0 + delta)
m2M     = 0.5*(1.0 - delta)
Shat    = m1M*m1M*np.array(P['chiAz']) + m2M*m2M*np.array(P['chiBz'])
Xnu     = 1. - 4.*nu
# compare & plot
def predict_fit(xxx):
    kkk,sss,qqq = xxx
    #qqq =  1. - 4.*(qqq/(1+qqq)**2)
    p1s = b0 * (1 + b1 * qqq)
    _n1 = n1 * (1 + q1 * qqq)
    _n2 = n2 * (1 + q2 * qqq)
    _d1 = d1 * (1 + q3 * qqq)
    _d2 = d2 * (1 + q4 * qqq)
    _up = (1.+ _n1 * kkk + _n2 * kkk **2)
    _lo = (1.+ _d1 * kkk + _d2 * kkk **2)
    return a0 * (1 + a1*qqq) * (1 + p1s*sss) *  _up / _lo

vars = [np.array(P['kap2t']),Shat,Xnu]
fit_data = predict_fit(vars)
lambda_ax       = np.linspace(0,700, 5000)
prediction      = predict_fit([lambda_ax,0.,0.])
prediction_qqqq = predict_fit([lambda_ax,0.,1-4*0.2222222])
prediction_mchi = predict_fit([lambda_ax,+0.1,0.])
prediction_pchi = predict_fit([lambda_ax,-0.1,0.])

###
# fits for Fmrg
a0,a1,b0,b1,n1,n2,d1,d2,q1,q2,q3,q4 = (0.22754806,0.92330138,0.59374838,-1.99378496,
                                       0.03445340731627873,5.5799962023491245e-06,0.08404652974611324,0.00011328992320789428,
                                       13.828175998146255,517.4149218303298,12.74661916436829,139.76057108600236)
# compare & plot
def predict_fitf(xxx):
    kkk,sss,qqq = xxx
    #qqq =  1. - 4.*(qqq/(1+qqq)**2)
    p1s = b0 * (1 + b1 * qqq)
    _n1 = n1 * (1 + q1 * qqq)
    _n2 = n2 * (1 + q2 * qqq)
    _d1 = d1 * (1 + q3 * qqq)
    _d2 = d2 * (1 + q4 * qqq)
    _up = (1.+ _n1 * kkk + _n2 * kkk **2)
    _lo = (1.+ _d1 * kkk + _d2 * kkk **2)
    return a0 * (1 + a1*qqq) * (1 + p1s*sss) *  _up / _lo

fprediction      = predict_fitf([lambda_ax,0.,0.])
fprediction_qqqq = predict_fitf([lambda_ax,0.,1-4*0.2222222])
fprediction_mchi = predict_fitf([lambda_ax,+0.1,0.])
fprediction_pchi = predict_fitf([lambda_ax,-0.1,0.])

### Lpeak fits from Zappa 2017
def get_lpeak(kappal):
    q = 1
    L0 = 2.178e-2
    n1 = 5.24e-4
    n2 = -9.36e-8
    d1 = 2.77e-2
    nu = 0.25
    q_factor = q*q/(nu*nu)
    fits = ( 1 + n1*kappal + n2*kappal*kappal )  / ( 1 + d1*kappal  )
    return L0*fits#/q_factor

# sanity check
# for kappa^L_2~1472 --> Lpeak~8.168e-4
print('Lpeak check: ',get_lpeak(1472))

'''
################################
# PLOTTING
################################
colordict = {
    '2B' : '#002F79',#[0,47,121],
    '2H' : '#0070FF',#[0,112,255],
    'ALF2': 'mediumslateblue',#[0,169,255],
    'ENG': '#00E8FF',#[0,232,255],
    'G2': '#00FA92',#[0,250,146],
    'G2k123': 'lightseagreen',
    'H4': '#46DD00',#[70,221,0],
    'MPA1': 'yellowgreen',#[125,248,3],
    'MS1': 'seagreen',#[186,255,59],
    'MS1b': 'gold',#[236,255,15],
    'SLy': 'orange',
    'BHBlp': '#FF6804',#[255,104,4],
    'DD2': '#FF006A',#[255,0,106],
    'LS220': 'hotpink',#[255,0,194],
    'SFHo': 'darkmagenta',#[182,74,248],
    'BLh': 'orchid',#[211,103,242],
    'BLQ': 'crimson',#[244,187,246],
    'SLy4': 'pink'
}
q = np.array(P['q'])
mtot = np.array(P['mass'])
nu = q_to_nu(np.array(P['q']))
fmrg = np.array(P['fmrg'])
amrg = np.array(P['amrg'])
k2t = np.array(P['kap2t'])
eos_label = np.array(P['eos'])
lpeak = np.array(P['lpeak'])#*(nu*nu)/(q*q)
ebin_m = np.array(P['ebin'])


#markero = markers.MarkerStyle(marker='o', fillstyle='none')

fig, axs = plt.subplots(2,2,figsize=(12,7))#plt.figure(figsize=(12,5))
#lin1 = axs[0,0].plot(lambda_ax,prediction,'k-',label=r'$q=1, \hat{S}=0$')
#lin2 = axs[0,0].plot(lambda_ax,prediction_qqqq,'k--',label=r'$q=2, \hat{S}=0$')
#lin3 = axs[0,0].plot(lambda_ax,prediction_mchi,'k:',label=r'$q=1, \hat{S}=0.1$')
#handles, labels = axs[0,0].get_legend_handles_labels()
#leg1 = axs[0,0].legend(ncol=1, loc="upper right",handles=handles,labels=labels, frameon='False', fancybox='False', edgecolor='white')
#axs[0,0].add_artist(leg1)
#axs[0,1].plot(lambda_ax,fprediction,'k-',label=r'$q=1, \hat{S}=0$')
#axs[0,1].plot(lambda_ax,fprediction_qqqq,'k--',label=r'$q=2, \hat{S}=0$')
#axs[0,1].plot(lambda_ax,fprediction_mchi,'k:',label=r'$q=1, \hat{S}=0.1$')
#handles, labels = axs[0,1].get_legend_handles_labels()
#leg1 = axs[0,1].legend(ncol=1, loc="upper right",handles=handles,labels=labels, frameon='False', fancybox='False', edgecolor='white')
#axs[0,1].add_artist(leg1)
#axs[1,0].plot(lambda_ax*8,get_lpeak(lambda_ax*8),'k-',label=r'$q=1$')
#handles, labels = axs[1,0].get_legend_handles_labels()
#leg1 = axs[1,0].legend(ncol=1, loc="upper right",handles=handles,labels=labels, frameon='False', fancybox='False', edgecolor='white')
#axs[1,0].add_artist(leg1)

eos_list = []
for i,eos in enumerate(eos_label):
    if eos in eos_list:
        axs[0,0].scatter(k2t[i],amrg[i], color=colordict[eos],marker='.')
    else:
        if eos=='BHBlp':
            lal = r'BHB$\Lambda\phi$'
        else:
            lal = eos
        axs[0,0].scatter(k2t[i],amrg[i], color=colordict[eos],marker='.',label=lal)
        eos_list.append(eos)

    axs[0,1].scatter(k2t[i],fmrg[i], color=colordict[eos],marker='.')
    axs[1,0].scatter(k2t[i],lpeak[i],color=colordict[eos],marker='.')
    axs[1,1].scatter(k2t[i],ebin_m[i],color=colordict[eos],marker='.')

axs[0,0].set_ylabel(r'$A_{\rm mrg}/M$')
axs[0,0].set_xlabel(r'$\kappa^T_2$')
#axs[0,0].set_ylim([0.159,0.32])
axs[0,0].set_xlim([0,700])

axs[0,1].set_ylabel(r'$Mf_{\rm mrg}/\nu$')
axs[0,1].set_xlabel(r'$\kappa^T_2$')
#axs[0,1].set_ylim([0.0554,0.141])
axs[0,1].set_xlim([0,700])

axs[1,0].set_ylabel(r'$L_{\rm peak}$')
axs[1,0].set_xlabel(r'$\kappa^T_2$')
#axs[1,0].set_ylim([0.03,0.126])
axs[1,0].set_xlim([0,700])
axs[1,0].set_yscale('log')

axs[1,1].set_ylabel(r'$e^{\rm mrg}_{\rm b}$')
axs[1,1].set_xlabel(r'$\kappa^T_2$')
axs[1,1].set_xlim([0,700])
#axs[1,1].set_ylim([0.004,0.04])

handles, labels = axs[0,0].get_legend_handles_labels()
leg2 = axs[0,0].legend(handles=handles[3:],labels=labels[3:], bbox_to_anchor=(0, 1, 2.2, 0), loc="lower left", mode="expand",ncol=6, markerscale=2.5, frameon='False', fancybox='False', edgecolor='white')
plt.tight_layout()
plt.show()
'''
############################################################################################
#  FITTING
############################################################################################
P.dropna()
P=P.fillna(0)
q = np.array(P['q'])
mtot = np.array(P['mass'])
nu = q_to_nu(np.array(P['q']))
s1z = np.array(P['chiAz'])
s2z = np.array(P['chiBz'])
fmrg = np.array(P['fmrg'])
amrg = np.array(P['amrg'])
k2t = np.array(P['kap2t'])
eos_label = np.array(P['eos'])
jorb = np.array(P['jorb_mrg']) 
jrr = np.array(P['jr_mrg'])

#delta   = np.sqrt(1.0 - 4.0*nu)
m1M     = 0.5*(1.0 + delta)
m2M     = 0.5*(1.0 - delta)
Shat    = m1M*m1M*s1z + m2M*m2M*s2z
Xnu     = 1. - 4.*nu


####################
# choose target = amrg, fmrg, lpeak, ebin_m
# fit with eta = kap2t + c*(1-4*nu)
####################
target = jorb
lambda_ax       = np.linspace(0,1500, 5000)

# find indeces
inz = np.where(target>0)
#inz = np.where(target<0)
iq1 = np.where((Xnu==0)&(s1z==0)&(s2z==0)&(target>0)) # equal mass non spinning
inq = np.where((Xnu!=0)&(s1z==0)&(s2z==0)&(target>0)) # unequal mass non spinning
iss = np.where((s1z!=0)&(s2z!=0)&(target>0)) # spinning

# similar to Zappa17
#target = lpeak*(nu**2)/(q**2)

def fit_zappa(kap,n1,n2,d1):
    l0 = 0.0217445683085
    return l0*(1 + n1*kap + n2*kap**2)/( 1 + d1*kap  )
'''
opt, cov,info, _,_ = curve_fit(fit_zappa,k2t[iq1],target[iq1],full_output=True)#,  p0=[0.000524,-9.36e-8,0.0277])
plt.scatter(k2t[inz],target[inz],label=r'CoReDB',marker='.')
plt.plot(lambda_ax,fit_zappa(lambda_ax,*opt),color='k')
plt.legend()
plt.yscale('log')
plt.show()
'''

# simple fits
#target = lpeak[inz]*0.0217445683085
#c = 3341.677955885443 # lpeak

target = jorb[inz] 
c = 573.1879212169972

eta = k2t[inz] + c*Xnu[inz]
def fit_func_q1(xxx, f0, n1,n2, d1, d2):
    #kkk,nnn = xxx
    eta = xxx#kkk + c*(nnn)
    _up = (1.+ n1 * eta + n2 * eta**2 )
    _lo = (1.+ d1 * eta + d2 * eta**2)
    return f0 *  _up / _lo

vars            = eta#[k2t[inz],Xnu[inz]]
popt_q1, pcov   = curve_fit(fit_func_q1, vars, target,maxfev=500000)
#f0,c,n1, n2, d1, d2 = popt_q1
print('\nBest pars:')
print(list(popt_q1))
print('\nErrors:')
print(np.sqrt(np.diag(pcov)))


fit_data = fit_func_q1(vars,*popt_q1)

#target1 = lpeak[iq1]
rel_res = (target - fit_data)/fit_data
res = np.sum(rel_res**2)
sigma = np.std(rel_res)

up = np.percentile(rel_res, 95)
lo = np.percentile(rel_res, 5)

print("Range = [ {} , {} ]".format(np.min(target),np.max(target)))
print("Sum res. sq. = ", res)
print("Rel. st. dev. = ", sigma)

residuals = (target - fit_data)/target
data_average = np.mean(target)
SStot = np.sum((target-data_average)**2)
SSres = np.sum((target-fit_data)**2)
R_square = 1 - SSres/SStot
print('\nRsquare:\n', R_square, '\n')

n, b, _     = plt.hist(rel_res, bins=30,density=True)
b = b[:-1] + 0.5*(b[1]-b[0])
gaus_plot   = np.exp(-0.5*(b/sigma)**2.)/np.sqrt(2.*np.pi)/sigma
plt.plot(b, gaus_plot, c='r')
plt.xlabel(r"residuals")
plt.show()

xnuu = np.linspace(0,0.25,5000)
Eta_ = lambda_ax + c* xnuu
#etaa = k2t[inz] + c * Xnu[inz]
prediction      = fit_func_q1(Eta_,*popt_q1)#([lambda_ax,0],*popt_q1)
prediction_q2   = fit_func_q1(Eta_,*popt_q1)#([lambda_ax,1-4*0.22222],*popt_q1)

fig, ax = plt.subplots(2,1,gridspec_kw={'height_ratios': [1, 0.3]})
cheese=ax[0].scatter(eta,target,label=r'\texttt{CoRe DB}',marker='.',c=q[inz],cmap='viridis')
ax[0].plot(Eta_,prediction,label=r'fits',color='k')
ax[0].set_xlim([0,np.max(eta)])
ax[0].set_ylim([np.min(target),np.max(target)])
ax[0].legend()
#ax[0].set_yscale('log')

ax[1].scatter(eta,rel_res,marker='.',c=q[inz],cmap='viridis')
ax[1].plot(Eta_,np.zeros(len(Eta_)), c='k')
#ax[1].plot(Eta_, np.ones(len(Eta_))*up*100, c='k', alpha=0.3)
#ax[1].plot(Eta_, np.ones(len(Eta_))*lo*100, c='k', alpha=0.3)
ax[1].set_xlim([0,np.max(eta)])

cbar = plt.colorbar(cheese,ax=[ax[0],ax[1]], orientation='horizontal', location='top', anchor=(0., 1.4), fraction=0.1, aspect=35)
cbar.set_label(label=r'$q$',loc='center')
plt.xlabel(r'$\xi=\kappa^T_2+c(1-4\nu)$')
ax[0].set_ylabel(r'$j_{\rm mrg}$')
ax[1].set_ylabel(r'Rel. Diff.')
#plt.tight_layout()
plt.subplots_adjust(wspace=0.35)
plt.show()


####################################################################################################3
###################### other ...
k2t_fit = k2t
f2_fit = jorb
target = f2_fit*(q*nu)

# find indeces
iq1 = np.where((Xnu==0)&(s1z==0)&(s2z==0)&(target>0)) # equal mass non spinning
inq = np.where((Xnu!=0)&(s1z==0)&(s2z==0)&(target>0)) # unequal mass non spinning
iss = np.where((s1z!=0)&(s2z!=0)&(target>0)) # spinning

# equal mass non spinning
def fit_func_q1(xxx, a0, a1, a2, a3, a4):
    kkk,sss,qqq = xxx
    _up = (1.+ a1 * kkk + a2 * kkk**2)
    _lo = (1.+ a3 * kkk + a4 * kkk**2)
    return a0 *  _up / _lo

vars            = [k2t_fit[iq1],Shat[iq1],Xnu[iq1]]
popt_q1, pcov   = curve_fit(fit_func_q1, vars, target[iq1], maxfev=500000)
a0, a1, a2, a3, a4 = popt_q1
print(popt_q1)

lambda_ax       = np.linspace(0,1200, 5000)
'''
prediction      = fit_func_q1([lambda_ax,0.,0.],*popt_q1)
plt.scatter(k2t[iq1],target[iq1],label=r'CoReDB',marker='.')
plt.plot(lambda_ax,prediction,label=r'Fits 2022',color='k')
#plt.yscale('log')
plt.legend()
plt.xlabel(r'$\kappa^T_2$')
plt.ylabel(r'$A^{\rm mrg}$')
plt.show()
'''

# unequal mass
def fit_func_nq(xxx, aM, b1, b2, b3, b4):
    kkk,sss,qqq = xxx
    qM = 1 + aM*qqq
    p1T = a1*(1+b1*qqq)
    p2T = a2*(1+b2*qqq)
    p3T = a3*(1+b3*qqq)
    p4T = a4*(1+b4*qqq)
    _up = (1.+ p1T * kkk + p2T * kkk**2)
    _lo = (1.+ p3T * kkk + p4T * kkk**2)
    qT = _up / _lo
    return a0 * qM * qT

vars            = [k2t_fit[inq],[0]*len(k2t_fit[inq]),Xnu[inq]]
popt_nq, pcov_nq   = curve_fit(fit_func_nq, vars, target[inq], maxfev=500000)
aM, b1, b2, b3, b4 = popt_nq
print(popt_nq)


# spinning!
def fit_func_iss(xxx,a1S,b1S):
    kkk,sss,qqq = xxx
    qM = 1 + aM*qqq
    p1S = a1S * (1 + b1S*qqq)
    qS = 1 + p1S * sss
    p1T = a1*(1+b1*qqq)
    p2T = a2*(1+b2*qqq)
    p3T = a3*(1+b3*qqq)
    p4T = a4*(1+b4*qqq)
    _up = (1.+ p1T * kkk + p2T * kkk**2)
    _lo = (1.+ p3T * kkk + p4T * kkk**2)
    qT = _up / _lo
    return a0 * qM * qS * qT

vars            = [k2t_fit[iss],Shat[iss],Xnu[iss]]
popt_iss, pcov_iss   = curve_fit(fit_func_iss, vars, target[iss], maxfev=500000)
a1S, b1S = popt_iss
print(popt_iss)

### prediction
def predict_fits(xxx):
    kkk,sss,qqq = xxx
    qM = 1 + aM*qqq
    p1S = a1S * (1 + b1S*qqq)
    qS = 1 + p1S * sss
    p1T = a1*(1+b1*qqq)
    p2T = a2*(1+b2*qqq)
    p3T = a3*(1+b3*qqq)
    p4T = a4*(1+b4*qqq)
    _up = (1.+ p1T * kkk + p2T * kkk**2)
    _lo = (1.+ p3T * kkk + p4T * kkk**2)
    qT = _up / _lo
    return a0 * qM * qS * qT

prediction      = predict_fits([lambda_ax,0.,0.])
prediction_qqqq = predict_fits([lambda_ax,0.,1-4*0.2222222])
prediction_mchi = predict_fits([lambda_ax,+0.1,0.])
prediction_pchi = predict_fits([lambda_ax,-0.1,0.])

fit_data = predict_fits([k2t_fit[iq1],Shat[iq1],Xnu[iq1]])
rel_res = (target[iq1] - fit_data)/fit_data
res = np.sum(rel_res**2)
sigma = np.std(rel_res)

print("Sum res. sq. = ", res)
print("Rel. st. dev. = ", sigma)

resx = rel_res
up = np.percentile(resx, 95)
lo = np.percentile(resx, 5)

print(up,lo)

data_average = np.mean(target[iq1])
SStot = np.sum((target[iq1]-data_average)**2)
SSres = np.sum((target[iq1]-fit_data)**2)
R_square = 1 - SSres/SStot
print( '\nRsquare:\n', R_square, '\n')

n, b, _     = plt.hist(rel_res, bins=30,density=True)
b = b[:-1] + 0.5*(b[1]-b[0])
gaus_plot   = np.exp(-0.5*(b/sigma)**2.)/np.sqrt(2.*np.pi)/sigma
plt.plot(b, gaus_plot, c='r')
plt.xlabel(r"residuals")
plt.show()

fig, ax = plt.subplots(2,1,gridspec_kw={'height_ratios': [0.9, 0.25]},figsize=(7,5))

cheese = ax[0].scatter(k2t_fit[target>0],target[target>0],label=r'\texttt{CoRe DB}',marker='.',c=q[target>0],cmap='viridis')
ax[0].plot(lambda_ax,prediction,label=r'$\hat{S}=0$,$q=0$',color='k')
ax[0].plot(lambda_ax,prediction_qqqq,label=r'$\hat{S}=0$,$q=2$',color='k',linestyle=':')
ax[0].plot(lambda_ax,prediction_pchi,label=r'$\hat{S}=0.1$,$q=0$',color='k',linestyle='--')
ax[0].legend()
ax[1].set_xlabel(r'$\kappa^T_2$')
ax[0].set_ylabel(r'$j_{\rm mrg}$')
ax[0].set_xlim([0,np.max(k2t_fit[target>0])])
ax[0].set_ylim([np.min(target[target>0]),np.max(target[target>0])])
#ax[0].set_yscale('log')

ax[1].scatter(k2t_fit[iq1],rel_res,marker='.',c=q[iq1],cmap='viridis')
ax[1].plot(lambda_ax,np.zeros(len(lambda_ax)), c='k')
ax[1].fill_between(lambda_ax,y1=up,y2=lo,facecolor ='gray', alpha = 0.2)
ax[1].set_xlim([0,np.max(k2t_fit[target>0])])
ax[1].set_ylabel(r'Rel. Diff.')
xax = ax[0].get_xaxis()
xax = xax.set_visible(False)

#ax[1].set_ylim([np.min(target[target>0]),np.max(target[target>0])])
cbar = plt.colorbar(cheese, ax=[ax[0],ax[1]],orientation='vertical', location='right', fraction=0.06, aspect=40, anchor=(1, 0.)) #anchor=(0., 0.9)
cbar.set_label(label=r'$q$',loc='center')
#plt.tight_layout()
plt.subplots_adjust(bottom=0.15,right=0.77,hspace=0.1)
#plt.savefig('lpeak_fits.pdf')
plt.show()
