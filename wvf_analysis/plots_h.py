import numpy as np
import matplotlib.pyplot as plt
import matplotlib
from watpy.utils.ioutils import *
from watpy.coredb.coredb import *
from watpy.utils.coreh5 import *
import matplotlib.ticker as ticker
from scipy import signal

import sys
sys.path.append('../')
from handy_functions import *
from align_tools import *

matplotlib.rcParams['text.usetex']= True
matplotlib.rcParams['font.serif']= 'Times New Roman'
matplotlib.rcParams['font.size']= 15 #28

##############################
# DATA
##############################
Njunk = 0#600
alpha = 0.001
sim_path    = '/home/agonzalez/Documents/git/watpy/msc/CoRe_DB_clone/' #previous release
#sim_path = '/data/numrel/DATABASE/CoRe_DB_clone/'
#rel2_path   = '/data/numrel/DATABASE/Release02/'
cdb = CoRe_db(sim_path)
idb = cdb.idb

dirc = 'BAM:0066'
M = 2.7
end_wvf = 3000
runc = ['R01','R02','R03']
text = r'\texttt{BAM:0066}, MS1b, $M=2.7~M_{\odot}$, $q=1$, $\kappa^T_2=287$'

c_runs = {
    'R01': '#66c2a5',
    'R02': '#fc8d62',
    'R03': '#8da0cb'
}

c_radii = {
    '700.0': '#8dd3c7',
    '750.0': 'gold',
    '800.0': '#bebada',
    '850.0': '#fb8072',
    '900.0': '#1f78b4',#'#80b1d3',
    '1000.0': '#fdb462'
}

sim = cdb.sim[dirc]

## HIGH
simrun = sim.run[runc[0]]

datah5 = simrun.data
datah5.write_strain_to_txt()
dseth5 = datah5.read_dset()

rh22_files = list(dseth5['rh_22'].keys())[0] # different distances and take smallest distance
dset = dseth5['rh_22'][rh22_files] 

# read
t           = dset[:,0][Njunk:] #u/M
Redh        = dset[:,1][Njunk:]
Imdh        = dset[:,2][Njunk:]
momg        = dset[:,5][Njunk:]
amp         = np.abs(Redh + 1j*Imdh) #dset[:,6]*M 
phi         = dset[:,7][Njunk:] #np.unwrap(np.angle(Redh + 1j*Imdh))#dset[:,7][Njunk:]
tmrg        = t[np.argmax(amp)]

# Tapering
L = t[-1] # length of signal
Redh, _ = windowing(Redh, alpha/L)
Imdh, _ = windowing(Imdh, alpha/L)


########################
# Studies with rh_22
#######################

## HIGH
rh22_files = list(dseth5['rh_22'].keys())[0] # different distances and take smallest distance
dset = dseth5['rh_22'][rh22_files] 

# read
t           = dset[:,0][Njunk:] #u/M
Redh        = dset[:,1][Njunk:]
Imdh        = dset[:,2][Njunk:]
momg        = dset[:,3][Njunk:]
amp         = np.abs(Redh + 1j*Imdh) #dset[:,6]*M 
phi         = dset[:,5][Njunk:] #np.unwrap(np.angle(Redh + 1j*Imdh))#dset[:,7][Njunk:]
tmrg        = t[np.argmax(amp)]

# Tapering
L = t[-1] # length of signal
Redh, _ = windowing(Redh, alpha/L)
Imdh, _ = windowing(Imdh, alpha/L)

## MEDIUM
simrun2 = sim.run[runc[1]]

datah52 = simrun2.data
datah52.write_strain_to_txt()
dseth52 = datah52.read_dset()

rh22_files2 = list(dseth52['rh_22'].keys())[0] # different distances and take smallest distance
dset2 = dseth52['rh_22'][rh22_files2] 

# read
t2           = dset2[:,0][Njunk:] #u/M
Redh2        = dset2[:,1][Njunk:]
Imdh2        = dset2[:,2][Njunk:]
momg2        = dset2[:,3][Njunk:]
amp2         = np.abs(Redh2 + 1j*Imdh2) #dset[:,6]*M 
phi2         = dset2[:,5][Njunk:]#np.unwrap(np.angle(Redh2 + 1j*Imdh2)) #dset2[:,7][Njunk:]
tmrg2        = t2[np.argmax(amp2)]

# Tapering
L2 = t2[-1] # length of signal
Redh2, _ = windowing(Redh2, alpha/L2)
Imdh2, _ = windowing(Imdh2, alpha/L2)

## LOW
simrun3 = sim.run[runc[2]]

datah53 = simrun3.data
datah53.write_strain_to_txt()
dseth53 = datah53.read_dset()

rh22_files3 = list(dseth53['rh_22'].keys())[0] # different distances and take smallest distance
dset3 = dseth53['rh_22'][rh22_files3] 

# read
t3           = dset3[:,0][Njunk:] #u/M
Redh3        = dset3[:,1][Njunk:]
Imdh3        = dset3[:,2][Njunk:]
momg3        = dset3[:,3][Njunk:]
amp3         = np.abs(Redh3 + 1j*Imdh3) #dset[:,6]*M 
phi3         = dset3[:,5][Njunk:]#np.unwrap(np.angle(Redh3 + 1j*Imdh3)) #dset2[:,7][Njunk:]
tmrg3        = t3[np.argmax(amp3)]

# Tapering
L3 = t3[-1] # length of signal
Redh3, _ = windowing(Redh3, alpha/L3)
Imdh3, _ = windowing(Imdh3, alpha/L3)

print(phi,phi2,phi3)
####################
# Self-Convergence 
# HIGH-Medium resolution difference at different radii
# and Medium-Low
####################

# scaling factor
# r is the convergence rate and h are the grid spacings

def scaling_factor(r):
    hL = 0.194
    hM = 0.145
    hH = 0.097
    return (hL**r - hM**r)/(hM**r - hH**r)

print('SF(2) = ',scaling_factor(2),', SF(4) = ',scaling_factor(4))

SF= scaling_factor(2) # choose convergence rate

fig, axs = plt.subplots(1,2,figsize=(9,4))

# Phase
phi2 = np.interp(t, t2, phi2)
phi3 = np.interp(t, t3, phi3)
phi_diff_hm = phi2-phi
phi_diff_ml = phi3-phi2
sf2_pd = SF* np.abs(phi_diff_hm)
sf2_pdl = SF* np.abs(phi_diff_ml)
#Amplitude
amp2 = np.interp(t, t2, amp2)
amp3 = np.interp(t, t3, amp3)
amp_diff_hm = np.abs(amp2-amp)
amp_diff_ml = np.abs(amp3-amp2)
sf2_ad = SF*amp_diff_hm
sf2_adl = SF*amp_diff_ml

axs[0].plot(t,np.log10(amp_diff_ml),'-',label=r'Low-Medium',color=c_runs['R02'],linewidth=1.2)
axs[0].plot(t,np.log10(amp_diff_hm),'-',label=r'Medium-High',color=c_runs['R01'],linewidth=1.2)
axs[0].plot(t,np.log10(sf2_ad),'--',label=r'SF(2) M-H',color=c_runs['R01'],linewidth=0.8)
#axs[0].plot(t,np.log10(sf2_adl),'--',label=r'SF(2) L-M',color=c_runs['R02'],linewidth=0.8)
axs[0].fill_betweenx(y=np.arange(-20.,20.,0.1),x1=tmrg2,x2=tmrg,color='gray',alpha=0.4)
axs[0].fill_betweenx(y=np.arange(-20.,20,0.1),x1=tmrg3,x2=tmrg2,color='gray',alpha=0.2)
axs[0].set_xlim([0,end_wvf])
axs[0].set_ylim([-8.1,-0.5])
axs[0].set_ylabel(r'$\log_{10}|\Delta a_{22}|$')
axs[0].set_xlabel(r'$u/M$')
axs[0].grid(True)

axs[1].plot(t,np.log10(np.abs(phi_diff_ml)),'-',label=r'Low-Medium',color=c_runs['R02'],linewidth=1.2)
axs[1].plot(t,np.log10(np.abs(phi_diff_hm)),'-',label=r'Medium-High',color=c_runs['R01'],linewidth=1.2)
axs[1].plot(t,np.log10(sf2_pd),'--',label=r'SF(2) M-H',color=c_runs['R01'],linewidth=0.8)
#axs[1].plot(t,np.log10(sf2_pdl),'--',label=r'SF(2) L-M',color=c_runs['R02'],linewidth=0.8)
axs[1].fill_betweenx(y=np.arange(-20.,20.,0.1),x1=tmrg2,x2=tmrg,color='gray',alpha=0.4)
axs[1].fill_betweenx(y=np.arange(-20.,20.,0.1),x1=tmrg3,x2=tmrg2,color='gray',alpha=0.2)
axs[1].set_xlim([0,end_wvf])
axs[1].set_ylim([-6.1,0])
axs[1].set_ylabel(r'$\log_{10}|\Delta\phi_{22}|$')
axs[1].set_xlabel(r'$u/M$')
axs[1].grid(True)
axs[1].legend(loc='upper left')

plt.tight_layout()
plt.savefig('bam66h_amp_phase_conv.pdf')
plt.show()



####################
# finite-radius extraction
# phase difference between consecutive radii @ HIGH resolution 
####################
file_list = list(dseth5['rh_22'].keys())

fig, axs = plt.subplots(1,2,figsize=(9,4))

for i in range(1,len(file_list)-1):
    file1 = file_list[i]
    file2 = file_list[i-1]
    rad1 = get_rad(file1) #float
    rad2 = get_rad(file2)
    print(rad1, " - ",rad2)
    rad_s = str(rad1)
    dset = dseth5['rh_22'][file1] 
    # read 1
    t           = dset[:,0][Njunk:] #u/M
    Redh        = dset[:,1][Njunk:]
    Imdh        = dset[:,2][Njunk:]
    phi         = dset[:,5][Njunk:] #np.unwrap(np.angle(Redh + 1j*Imdh))#dset[:,7][Njunk:]
    amp         = np.abs(Redh + 1j*Imdh) #dset[:,6]*M 
    tmrg        = t[np.argmax(amp)]
    # read 2
    dset2 = dseth5['rh_22'][file2] ## same resolution!!
    t2           = dset2[:,0][Njunk:] #u/M
    Redh2        = dset2[:,1][Njunk:]
    Imdh2        = dset2[:,2][Njunk:]
    phi2         = dset2[:,5][Njunk:]#np.unwrap(np.angle(Redh2 + 1j*Imdh2)) #dset2[:,7][Njunk:]
    amp2         = np.abs(Redh2 + 1j*Imdh2) #dset[:,6]*M 
    tmrg2        = t2[np.argmax(amp2)]
    '''
    if rad1==750.0:
        phi= phi - 2*np.pi
        phi2 = phi2 - 2*2*np.pi
    elif rad1==850.0:
        phi= phi - 2*2*np.pi
        phi2 = phi2 - 4*2*np.pi
    elif rad1==1000.0:
        phi = phi - 3*2*np.pi
    elif rad1==800.0:
        phi = phi - 4*2*np.pi
        phi2 = phi2 - 2*np.pi
    elif rad1==900.0:
        phi2=phi2-2*2*np.pi
    '''
    # Amplitude
    amp2 = np.interp(t, t2, amp2)
    amp_diff = (amp-amp2)/amp
    sign_amp = np.sign(amp_diff)
    #amp_diff = signal.savgol_filter(amp_diff, window_length=31, polyorder=3)
    axs[0].plot(t,np.log10(np.abs(amp_diff)),'--',linewidth=1,label=r'$R_i$='+rad_s,color=c_radii[rad_s])
    #axs[0].fill_betweenx(y=np.arange(-20.,15.2,0.1),x1=tmrg2,x2=tmrg,color='gray',alpha=0.2)
    # Phase
    phi2 = np.interp(t, t2, phi2)
    phi_diff = phi-phi2
    sign_phi = np.sign(phi_diff)
    #phi_diff = signal.savgol_filter(phi_diff, window_length=31, polyorder=3)
    axs[1].plot(t,np.log10(np.abs(phi_diff)),'--',label=r'$R_i$='+rad_s,linewidth=1,color=c_radii[rad_s])
    #axs[1].fill_betweenx(y=np.arange(-150.,150.2,1),x1=tmrg2,x2=tmrg,color='gray',alpha=0.2)

axs[0].set_ylabel(r'$\log_{10}|\Delta^*a_{22}/a_{22}|$')
axs[0].set_xlabel(r'$u/M$')
axs[0].set_xlim([0,end_wvf])
axs[0].set_ylim([-6,-2.4])
##axs[0].set_ylim([-0.022,0.045]) for non log
axs[0].grid(True)
axs[0].legend(loc="lower left")

axs[1].set_xlim([0,end_wvf])
axs[1].set_ylim([-9,0])
##axs[1].set_ylim([-2.8,-1.31]) for log
##axs[1].set_ylim([-0.004,0.026]) for non log
axs[1].set_ylabel(r'$\log_{10}|\Delta^*\phi_{22}(R_i)|$')
axs[1].set_xlabel(r'$u/M$')
axs[1].grid(True)
#plt.legend()
plt.tight_layout()
plt.savefig('bam66h_amp_phase_star.pdf')
plt.show()


