import numpy as np
import matplotlib.pyplot as plt
import matplotlib
from watpy.utils.ioutils import *
from watpy.coredb.coredb import *
from watpy.utils.coreh5 import *
from watpy.utils import num as num
from watpy.wave.gwutils import *
import matplotlib.ticker as ticker
from scipy import signal

import sys
sys.path.append('../')
from handy_functions import *
from align_tools import *

matplotlib.rcParams['text.usetex']= True
matplotlib.rcParams['font.serif']= 'Times New Roman'
matplotlib.rcParams['font.size']= 15 #28

##############################
# DATA
##############################
Njunk = 0#600
alpha = 0.001
sim_path    = '../../watpy/msc/CoRe_DB_clone/' 
cdb = CoRe_db(sim_path)
idb = cdb.idb

'''
for entry in idb.dbkeys:
    sim = cdb.sim[entry]
    meta = sim.md.data
    if meta['reference_bibkeys']=='Radice:2016gym':
        print(entry)
'''
#########################
dirc = 'BAM:0066'
M = 2.7
end_wvf = 3000

sim = cdb.sim[dirc]
psi_ds = []
psii_ds = []
t_ds = []
h_ds = []

## LOW
simrun = sim.run['R03']
datah5 = simrun.data
datah5.write_strain_to_txt()
dseth5 = datah5.read_dset()
rpsi4_files = list(dseth5['rpsi4_22'].keys()) # different distances and take smallest distance
dset = dseth5['rpsi4_22'][rpsi4_files[0]] 
t3           = dset[:,0][Njunk:] #u/M
Redh3        = dset[:,1][Njunk:]
Imdh3        = dset[:,2][Njunk:]
psi43        = Redh3 + 1j*Imdh3
phi3         = np.unwrap(np.angle(psi43))
h3 = 0.194
psi_ds.append(Redh3)
psii_ds.append(Imdh3)
t_ds.append(t3)
h_ds.append(h3)

## MEDIUM
simrun = sim.run['R02']
datah5 = simrun.data
datah5.write_strain_to_txt()
dseth5 = datah5.read_dset()
rpsi4_files = list(dseth5['rpsi4_22'].keys()) # different distances and take smallest distance
dset = dseth5['rpsi4_22'][rpsi4_files[0]] 
t2           = dset[:,0][Njunk:] #u/M
Redh2        = dset[:,1][Njunk:]
Imdh2        = dset[:,2][Njunk:]
psi42        = Redh2 + 1j*Imdh2
phi2         = np.unwrap(np.angle(psi42))
h2 = 0.1455
psi_ds.append(Redh2)
psii_ds.append(Imdh2)
t_ds.append(t2)
h_ds.append(h2)

## HIGH
simrun = sim.run['R01']
datah5 = simrun.data
datah5.write_strain_to_txt()
dseth5 = datah5.read_dset()
rpsi4_files = list(dseth5['rpsi4_22'].keys()) # different distances and take smallest distance
dset = dseth5['rpsi4_22'][rpsi4_files[0]] 
t1           = dset[:,0][Njunk:] #u/M
Redh1        = dset[:,1][Njunk:]
Imdh1        = dset[:,2][Njunk:]
psi41        = Redh1 + 1j*Imdh1
phi1         = np.unwrap(np.angle(psi41))
h1 = 0.097
psi_ds.append(Redh1)
psii_ds.append(Imdh1)
t_ds.append(t1)
h_ds.append(h1)


############################
# Richardson extrapolation
############################
# interpolate all data sets on time grid of extrapolated result
ye, te = richardson_extrap_series(p=2,y=psi_ds,t=t_ds,h=h_ds)
yee, tee = richardson_extrap_series(p=2,y=psii_ds,t=t_ds,h=h_ds)
psi4_rich = ye + 1j*yee
phi_rich = np.unwrap(np.angle(psi4_rich))

dphi_h = phi1 - 2*np.pi - phi_rich # uncertainty due to truncation error
plt.plot(t1,np.log10(np.abs(dphi_h)),label='High-R(L,M,H)')
plt.legend()
plt.show()


############################
# Extrapolation w/polynomial of order K
############################
# last radius
dset = dseth5['rpsi4_22'][rpsi4_files[-1]] 
# read
t4           = dset[:,0][Njunk:] #u/M
Redh4        = dset[:,1][Njunk:]
Imdh4        = dset[:,2][Njunk:]
psi44        = Redh4 + 1j*Imdh4
phif         = np.unwrap(np.angle(psi44))


rpsi4_files = list(dseth5['rpsi4_22'].keys())
ys = []
yt = []
radii = []
for file in rpsi4_files:
    rad = get_rad(file)
    dset = dseth5['rpsi4_22'][file]
    t           = dset[:,0][Njunk:] #u/M
    Redh        = dset[:,1][Njunk:]
    Imdh        = dset[:,2][Njunk:]
    ys.append(Redh) 
    yt.append(Imdh)
    radii.append(rad)

K = 2
yinf1 = radius_extrap_polynomial(ys,radii,K)
yinf2 = radius_extrap_polynomial(yt,radii,K)
yphi = np.unwrap(np.angle(yinf1 + 1j*yinf2))

dp_r = yphi+2*np.pi-phi1#yphi+5*np.pi-phi1 # uncertainty due to finite radius
dp_rf = yphi+2*np.pi-phif#yphi-phif+4*np.pi

plt.plot(t,yphi+2*np.pi,label='r($\infty$)')
plt.plot(t,phif,label='r(1000')

plt.legend()
plt.show()

'''
############################
# Extrapolation at infinity
############################
r0 = 700 # extraction radius
m0 = 2.679263 # adm mass
inf_psi4 = radius_extrap(t,psi4,r0,m0)
inf_phi = np.unwrap(np.angle(inf_psi4))
dp_h = inf_phi-phi+4*np.pi 
'''

############################
# total error budget
############################
dphi = (dphi_h**2 + dp_rf**2)**(0.5)

t3_mrg3 = 2586.226 # took these values from the h22 script
t2_mrg2 = 2654.322
t1_mrg1 = 2707.046

fig = plt.subplots(figsize=(7,4))
plt.fill_between(x=t,y1=np.log10(dphi),y2=-10,facecolor='#b2df8a',alpha=0.2)
plt.plot(t,np.log10(np.abs(dp_r)), label=r'$R(\infty)$ - $R(700), K=2$', color='#fc8d59')
plt.plot(t,np.log10(np.abs(dp_rf)), label=r'$R(\infty)$ - $R(1000), K=2$', color='#fc8d59',linestyle='--')
plt.plot(t,np.log10(np.abs(dphi_h)), label=r'$H$ - $\mathcal{R}(L,M,H)$', color='#91bfdb') 
plt.fill_betweenx(y=np.arange(-20.,20.,0.1),x1=t2_mrg2,x2=t1_mrg1,color='gray',alpha=0.4)
plt.fill_betweenx(y=np.arange(-20.,20,0.1),x1=t3_mrg3,x2=t2_mrg2,color='gray',alpha=0.2)
#plt.plot(t,yphi+10*np.pi, label=r'$R(\infty)$ - $R(1000)$', color='k') 
#plt.plot(t,phif+6*np.pi, label=r'$R(\infty)$ - $R(1000)$', color='k',linestyle='--') 
plt.xlim([0,end_wvf])
plt.ylim([-5.8,1.39])
plt.xlabel(r'$u/M$')
plt.ylabel(r'$\log_{10}|\Delta\phi_{22}|$')
plt.legend(loc='best')
plt.tight_layout()
plt.savefig('error_budget.pdf')
plt.show()

quit()
#############################################
# Rh22
#############################################
rh_files = list(dseth5['rh_22'].keys())[0] # different distances and take smallest distance
dset = dseth5['rh_22'][rh_files] 
# read
th           = dset[:,0][Njunk:] #u/M
Redh        = dset[:,1][Njunk:]
Imdh        = dset[:,2][Njunk:]
h22        = Redh + 1j*Imdh
phih         = np.unwrap(np.angle(h22))
############################
# Extrapolation at infinity
############################
r0 = 750 # extraction radius
m0 = 2.679263 # adm mass
inf_h22 = radius_extrap(th,h22,r0,m0)
inf_phih = np.unwrap(np.angle(inf_h22))

fig = plt.subplots(figsize=(7,4))
plt.plot(th,np.log10(np.abs(inf_phih-phih)), label=r'$R(\infty)$ - $R(750)$, $Rh_{22}$', color='#fc8d59')
plt.plot(t,np.log10(np.abs(inf_phi+4*np.pi-phi)), label=r'$R(\infty)$ - $R(750)$, $R\psi^4_{22}$', color='#91bfdb') ## Add this line to figure 8 or do a total error budget plot like in Seba's paper
plt.xlim([0,end_wvf])
plt.ylim([-2.5,-0.5])
plt.xlabel(r'$u/M$')
plt.ylabel(r'$\log_{10}|\Delta\phi_{22}|$')
plt.legend()
plt.tight_layout()
#plt.savefig('rinf_ext.pdf')
plt.show()