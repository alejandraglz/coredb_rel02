#########################################
# Set of handy functions I use often
#########################################
import numpy as np
import math
import cmath
import h5py
import csv
import re
from scipy.interpolate import interp1d
import pandas as pd
from bajes.obs.gw.utils import compute_chi_eff, compute_lambda_tilde, lambda_2_kappa
from watpy.utils.ioutils import *
from watpy.coredb.coredb import *
from watpy.utils.coreh5 import *

def m1m2_to_mnu(m1, m2):
    """
    From individual masses to total mass and sym mass ratio
    """
    m = m1+m2
    nu  = m1*m2/(m*m)
    if np.any(nu>0.25):
        nu = np.minimum(nu, 0.25)
    if np.any(nu<0.0):
        nu = np.maximum(nu, 0.0)
    return m,nu

def get_rad(filename):
    name = re.match(r'R(\w+)_l(\d+)_m(\w+)_r(\w+).txt', filename)
    try:
        rad1   = rinf_str_to_float(name.group(4))
    except AttributeError:
        name2 = re.match(r'm(\d+)', name.group(4))
        rad1 = rinf_str_to_float(name2.group(1))
    return rad1

def csv_reader(filename):
    """
    Read a CSV file using csv.DictReader
    """
    data = [] 
    with open(filename) as f:
        reader = csv.DictReader(f, delimiter=',')
        for line in reader:
            data.append(line)
    return data

def nu_to_q(nu):
    if nu==0:
        sol = 0.
    else: 
        a = nu
        b = 2*nu - 1
        c = nu

        d = b**2 - 4*a*c

        if d<0:
            sol_1 = (-b + cmath.sqrt(d))/(2*a)
            sol_2 = (-b - cmath.sqrt(d))/(2*a)
        else:
            sol_1 = (-b + math.sqrt(d))/(2*a)
            sol_2 = (-b - math.sqrt(d))/(2*a)
    
        if sol_1>0:
            sol = sol_1
        else:
            sol = sol_2

    return sol


def q_to_nu(q):
    return q/((1.+q)*(1.+q))

def extract_z(array):
# extract third component of a string array with elements:
# "first, second, third"
# returns array with only the 3rd element
    zcomp = []
    for i in array:
        zcomp.append(float(i.split(',')[2]))
    return zcomp

def extract_one(array, i):
    # extract first component of a string array with elements:
    # "first, second, third"
    # returns array with only the ith element
    fcomp = []
    for element in array:
        fcomp.append(float(element.split(',')[i]))
    return fcomp

def extract_first(array):
# extract first component of a string array with elements:
# "first, second, third"
# returns array with only the 1st element
    fcomp = []
    for i in array:
        fcomp.append(float(i.split(',')[0]))
    return fcomp

def get_df(data):
    name = np.array([d['database_key'] for d in data])
    idcode = np.array([d['id_code'] for d in data])
    mass = np.array([d['id_mass'] for d in data], dtype=float)
    m1 = np.array([d['id_mass_starA'] for d in data], dtype=float)
    m2 = np.array([d['id_mass_starB'] for d in data], dtype=float)
    q = np.array([d['id_mass_ratio'] for d in data], dtype=float)
    eos = np.array([d['id_eos'] for d in data])
    ecc = np.array([d['id_eccentricity'] for d in data])
    #kap = np.array([d['id_kappa2T'] for d in data], dtype=float)
    
    # value error dealing:
    for i,ec in enumerate(ecc):
        if ec=='':
            ecc[i]=np.nan
    ecc = np.array(ecc, dtype=float)

    # 3-valued fields
    chiA = np.array([d['id_spin_starA'] for d in data]) #you want the chiz (3rd component)
    chiB = np.array([d['id_spin_starB'] for d in data])
    lamA = np.array([d['id_Lambdaell_starA'] for d in data]) #you want l=2, 1st component
    lamB = np.array([d['id_Lambdaell_starB'] for d in data])

    chiAz = extract_z(chiA)
    chiBz = extract_z(chiB)
    lamA2 = np.array(extract_first(lamA), dtype=float)
    lamB2 = np.array(extract_first(lamB), dtype=float)

    lamT = compute_lambda_tilde(m1,m2,lamA2,lamB2)
    chieff = compute_chi_eff(m1,m2,chiAz,chiBz)
    kap = lambda_2_kappa(m1,m2,lamA2,lamB2)

    ## PANDAS
    name = pd.Series(name)
    idcode = pd.Series(idcode)
    mass = pd.Series(mass)
    m1 = pd.Series(m1)
    m2 = pd.Series(m2)
    q = pd.Series(q)
    eos = pd.Series(eos)
    ecc = pd.Series(ecc)
    kap = pd.Series(kap)
    chiAz = pd.Series(chiAz)
    chiBz = pd.Series(chiBz)
    lamA2 = pd.Series(lamA2)
    lamB2 = pd.Series(lamB2)
    lamT = pd.Series(lamT)
    chieff = pd.Series(chieff)

    P = name.to_frame()
    P.columns=['name']
    idcode = idcode.to_frame()
    idcode.columns=['id_code']
    mass = mass.to_frame()
    mass.columns=['mass']
    m1 = m1.to_frame()
    m1.columns=['m1']
    m2 = m2.to_frame()
    m2.columns=['m2']
    q = q.to_frame()
    q.columns=['q']
    eos = eos.to_frame()
    eos.columns=['eos']
    ecc = ecc.to_frame()
    ecc.columns=['ecc']
    kap = kap.to_frame()
    kap.columns=['kap2t']
    chiAz = chiAz.to_frame()
    chiAz.columns=['chiAz']
    chiBz = chiBz.to_frame()
    chiBz.columns=['chiBz']
    lamA2 = lamA2.to_frame()
    lamA2.columns=['lamA2']
    lamB2 = lamB2.to_frame()
    lamB2.columns=['lamB2']
    lamT = lamT.to_frame()
    lamT.columns=['lamT']
    chieff = chieff.to_frame()
    chieff.columns=['chieff']

    P['id_code'] = idcode
    P['mass'] = mass
    P['m1'] = m1
    P['m2'] = m2
    P['q'] = q
    P['eos'] = eos
    P['ecc'] = ecc
    P['kap2t'] = kap
    P['chiAz'] = chiAz
    P['chiBz'] = chiBz
    P['lamA2'] = lamA2
    P['lamB2'] = lamB2
    P['chieff'] = chieff
    P['lamT'] = lamT

    return P
