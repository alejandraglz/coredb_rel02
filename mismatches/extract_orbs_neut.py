from cmath import nan
import matplotlib
import matplotlib.pyplot as plt
#matplotlib.use('TkAgg')
import numpy as np 
#from pycbc.types.frequencyseries import FrequencySeries 
import json
import h5py 
import pandas as pd
from matplotlib.ticker import (MultipleLocator, AutoMinorLocator)
import csv

from watpy.coredb.coredb import *
from watpy.wave.wave import *
from watpy.utils.coreh5 import *
from watpy.utils.ioutils import *

import sys
import os
sys.path.append('../')
from mismatch import *
from handy_functions import *

matplotlib.rcParams['text.usetex']= True
#matplotlib.rcParams['font.family'] = ['serif']
matplotlib.rcParams['font.serif']= 'Times New Roman'
matplotlib.rcParams['font.size']= 15 #28

P = pd.read_csv("mm_data.csv")
sim_path    = '/data/numrel/DATABASE/CoRe_DB_clone/'
rel2_path   = '/data/numrel/DATABASE/Release02/'
cdb = CoRe_db(sim_path)
idb = cdb.idb
orbs = []
neut = []
for i_dbkey, entry in enumerate(P['name']):
    print("Working with: ",entry)
    dir            = entry.replace(':','_')
    target         = dir.split('_')[0]
    dirc = dir.replace('_',':')
    splitdir = dir.split('_')
    #resos = []

    if entry in idb.dbkeys:  ## CURRENT RELEASE
        sim = cdb.sim[dirc]
        this_path = os.path.join(sim_path,dir) 
    
        if len(sim.run.keys())==0:
            continue
        else:
            for res in list(sim.run.keys()):
                simrun = sim.run[res]
                meta_dict = simrun.md.data
                if meta_dict['number_of_orbits']!='':
                    orbs.append(float(meta_dict['number_of_orbits']))
                else:
                    orbs.append('None')
                neut.append(meta_dict['neutrino_scheme'])
        #print(sim.run.keys(),orbs,neut)
                
    else:                    ## NEW RELEASE
        this_path = os.path.join(rel2_path,dir)
        metamain = this_path + '/metadata_main.txt'
        sim = CoRe_sim(this_path)
        #dict = read_txt_into_dict(metamain)
        #run_folders = [ name for name in os.listdir(thispath) if os.path.isdir(os.path.join(thispath, name)) ]
        if len(sim.run.keys())==0:
            continue
        else:
            for res in list(sim.run.keys()):
                #run_path = this_path + '/'  + res
                #h5dat = run_path + '/data.h5'
                #simrun = CoRe_run(run_path)
                simrun = sim.run[res]
                meta_dict = simrun.md.data
                if meta_dict['number_of_orbits']!='':
                    orbs.append(float(meta_dict['number_of_orbits']))
                else:
                    orbs.append('None')
                if meta_dict['neutrino_scheme']!='':
                    neut.append(meta_dict['neutrino_scheme'])
                else:
                    neut.append('None')
                mtot = float(meta_dict['id_mass'])
                f0 = float(meta_dict['id_gw_frequency_Hz'])
            #print(sim.run.keys(),orbs,neut)
    #orbs = []
    #neut = []

del P['orbits']
del P['neutrino']

orbs = pd.Series(orbs)
orbs = orbs.to_frame()
orbs.columns=['orbits']
P['orbits'] = orbs

rbs = pd.Series(neut)
rbs = rbs.to_frame()
rbs.columns=['neutrino']
P['neutrino'] = rbs

print(np.array(P['neutrino']))
