from cmath import nan
import matplotlib
import matplotlib.pyplot as plt
#matplotlib.use('TkAgg')
import numpy as np 
#from pycbc.types.frequencyseries import FrequencySeries 
import json
import h5py 
import pandas as pd
from matplotlib.ticker import (MultipleLocator, AutoMinorLocator)

#from pycbc.filter import match, overlap, optimized_match
#from pycbc.filter.matchedfilter import optimized_match
from pycbc.types.timeseries import TimeSeries
from pycbc.psd import  aLIGODesignSensitivityP1200087, AdVDesignSensitivityP1200087, EinsteinTelescopeP1600143, aLIGOZeroDetHighPower #choose your PSD
from pycbc import waveform

from bajes.obs.gw.noise import *

from watpy.coredb.coredb import *
from watpy.wave.wave import *
from watpy.wave.gwutils import match
from watpy.utils.coreh5 import *
from watpy.utils.ioutils import *

import sys
import os
sys.path.append('../')
from mismatch import *
from handy_functions import *

matplotlib.rcParams['text.usetex']= True
#matplotlib.rcParams['font.family'] = ['serif']
matplotlib.rcParams['font.serif']= 'Times New Roman'
matplotlib.rcParams['font.size']= 15 #28

Msuns  = 4.925491025543575903411922162094833998e-6
Mpc_m  = 3.085677581491367278913937957796471611e22
Msun_m = 1.476625061404649406193430731479084713e3

Njunk = 600
dT = 1./4096.

fasd_L1, asd_L1 = get_design_sensitivity('L1')
fasd_ET, asd_ET = get_design_sensitivity('ET')

psd_L1 = asd_L1*asd_L1
psd_ET = asd_ET*asd_ET


def get_rad(filename):
    name = re.match(r'R(\w+)_l(\d+)_m(\w+)_r(\w+).txt', filename)
    try:
        rad1   = rinf_str_to_float(name.group(4))
    except AttributeError:
        name2 = re.match(r'm(\d+)', name.group(4))
        rad1 = rinf_str_to_float(name2.group(1))
    return rad1

def get_mm(resos,sim):
    #print(resos)
    #fmax = np.min(resos)   # first highest resolution
    #frmax = list(sim.run.keys())[np.argmin(resos)] #its run name
    #resos.pop(np.argmin(resos))
    #smax = np.min(resos)   # second highest resolution
    #srmax = list(sim.run.keys())[np.argmin(resos)] #its run name

    fmax = smax = 1000
    for ind,val in enumerate(resos):
        if val < fmax:
            smax = fmax
            fmax = val
            frmax = list(sim.run.keys())[ind]
            srmax = list(sim.run.keys())[ind-1]
        elif (val<smax and val!=fmax):
            smax = val
            srmax = list(sim.run.keys())[ind]
    #print(frmax,fmax,srmax,smax)
    if sim.dbkey=='BAM:0066':
        frmax='R02'
        srmax='R03'
    if fmax==smax or np.unique(resos).size==1:
        sim_mm = np.nan
        radii = np.nan
    else:
        #print(frmax,fmax,srmax,smax)
        ### Now get the waveforms

        ## first highest
        simrun1 = sim.run[frmax]
        datah51 = simrun1.data
        #datah5.dump()
        dseth51 = datah51.read_dset()
        rh22_files1 = dseth51['rh_22'].keys()

        ## second highest
        simrun2 = sim.run[srmax]
        datah52 = simrun2.data
        dseth52 = datah52.read_dset()
        rh22_files2 = dseth52['rh_22'].keys()
        
        sim_mm = []
        radii = []
        for key1 in rh22_files1:
            #print(key1)
            rad1   = get_rad(key1)
            dset1 = dseth51['rh_22'][key1]
            hp1          = dset1[:,1][Njunk:]
            hc1          = dset1[:,2][Njunk:]
            t1           = dset1[:,0][Njunk:]
            ht1          = hp1 + 1j*hc1
            t1           = t1/mtot
            dt1          = t1[1]-t1[0]
            ap1 = np.abs(ht1)
            pp1 = -np.unwrap(np.angle(ht1))
            ff1 = np.abs(np.diff(pp1)/np.diff(t1))
            amrg = np.argmax(ap1)
            f1mrg = ff1[amrg]

            for key2 in rh22_files2:
                #print(key2)
                #name = re.match(r'R(\w+)_l(\d+)_m(\d+)_r(\w+).txt', key2)
                rad2   = get_rad(key2)#rinf_str_to_float(name.group(4))

                if rad2!=rad1: #or rad1==400:
                    continue
                else:
                    dset2 = dseth52['rh_22'][key2]
                    hp2          = dset2[:,1][Njunk:]
                    hc2          = dset2[:,2][Njunk:]
                    t2           = dset2[:,0][Njunk:]
                    ht2          = hp2 + 1j*hc2
                    t2           = t2/mtot
                    dt2          = t2[1]-t2[0]

                    # compare ht1 and ht2
                
                    # Time Series
                    hp_nrTS   = TimeSeries(hp1, dT)
                    hc_nrTS   = TimeSeries(hc1, dT)
                    hp_nrTS_l   = TimeSeries(hp2, dT)
                    #hc_nrTS_l   = TimeSeries(hc2, dt1)
                    f = waveform.utils.frequency_from_polarizations(hp_nrTS, hc_nrTS)
                    #fft_hp = hp_nrTS.to_frequencyseries()
                    #fft_hc = hc_nrTS.to_frequencyseries()
                    #fft_h = fft_hp + 1j*fft_hc
                    #fft_a = np.abs(fft_h)
                    #fft_f = np.array(fft_hp.sample_frequencies)
                    #f0 = fft_f[np.argmax(fft_a)]

                    alpha=0.001
                    L = hp_nrTS.duration # length of signal in seconds
                    hp_nrTS, _ = windowing(hp_nrTS, alpha/L)
                    L = hp_nrTS_l.duration
                    hp_nrTS_l, _ = windowing(hp_nrTS_l, alpha/L)
                    
                    '''
                    if sim.dbkey=='BAM:0097' or sim.dbkey=='BAM:0100':
                        print("Comparing: ",key1," and ",key2)
                        plt.plot(hp_nrTS.sample_times,hp_nrTS/Msuns,label="highest")
                        plt.plot(hp_nrTS_l.sample_times,hp_nrTS_l/Msuns,'--',label="2nd highest")
                        #plt.plot(f.sample_times,f/(2*np.pi*mtot*Msuns),label="freq")
                        plt.legend()
                        plt.show()
                    '''

                    # Resize
                    tlen = max(len(hp_nrTS), len(hp_nrTS_l))
                    hp_nrTS.resize(tlen)
                    hp_nrTS_l.resize(tlen)

                    f0_SI = f0#/(2*np.pi*mtot*Msuns) #Hz
                    orig_fm = f1mrg
                    if f1mrg/(2*np.pi*mtot*Msuns)<10000:
                        f1mrg = f1mrg/(2*np.pi*mtot*Msuns)
                    #elif f1mrg<f0_SI:
                    #    f1mrg=6000
                    else:
                        f1mrg=f1mrg
                    # Generate the aLIGO ZDHP PSD
                    delta_f = 1.0 / hp_nrTS.duration
                    flen    = tlen//2 + 1
                    psd     = aLIGOZeroDetHighPower(flen, delta_f, f0_SI)
                    #psd     = EinsteinTelescopeP1600143(flen, delta_f, max(f0_SI, flow))
                    #psd     = aLIGODesignSensitivityP1200087(flen, delta_f, f0)
                    #print("got the psd")
                    if orig_fm<1:
                        m = match(np.array(hp_nrTS.sample_times), np.array(hp_nrTS), np.array(hp_nrTS_l.sample_times), np.array(hp_nrTS_l), fpsd=fasd_L1, psd=psd_L1, fmin=f0, fmax=f1mrg, df=delta_f) #m,  _   = optimized_match(hp_nrTS, hp_nrTS_l,psd=psd,low_frequency_cutoff=f0, high_frequency_cutoff=f1mrg)
                    else:
                        m = np.nan
                    print("Mtot=",mtot, ", f0 =",f0_SI, ' fmrg= ',f1mrg,", unfaithfulness = ",1.-m)
                    
                    if (1-m)>=1.099:
                        print("Comparing: ",key1," and ",key2)
                        plt.title("Mismatch= "+str(1.-m))
                        plt.plot(hp_nrTS.sample_times,hp_nrTS,label="highest")
                        plt.plot(hp_nrTS_l.sample_times,hp_nrTS_l,'--',label="2nd highest")
                        plt.legend()
                        #plt.savefig(sim.code+sim.key+str(rad1)+".pdf")
                        plt.show()
                    
                    sim_mm.append(1.-m)
                    radii.append(str(rad1))
    return sim_mm, radii#np.mean(sim_mm)

##################################################################################################################
#sim_path    = '/home/agonzalez/Documents/git/watpy/msc/CoRe_DB_clone/'
sim_path    = '/data/numrel/DATABASE/CoRe_DB_clone/'
rel2_path   = '/data/numrel/DATABASE/Release02/'

data = csv_reader("../data/database.csv")
P = get_df(data) #ready to go pandas df
cdb = CoRe_db(sim_path)
idb = cdb.idb

fhigh = 2048
flow = 20
dT = 1./4096.

resos = []
orbs = []
neut = []
mismatches = []
all_radii = []
weird_cases = ['THC:0076']
#P = P.loc[P['name'].str.contains("THC")]
for i_dbkey, entry in enumerate(P['name']):
    print("Working with: ",entry)
    dir            = entry.replace(':','_')
    target         = dir.split('_')[0]
    dirc = dir.replace('_',':')
    splitdir = dir.split('_')
    #resos = []

    if entry in idb.dbkeys:  ## CURRENT RELEASE
        sim = cdb.sim[dirc]
        this_path = os.path.join(sim_path,dir) 
    
        if len(sim.run.keys())==1:
            mismatches.append(nan)
            all_radii.append(nan)
        else:
            for res in sim.run.keys():
                simrun = sim.run[res]
                meta_dict = simrun.md.data
                resos.append(float(meta_dict['grid_spacing_min']))
                try:
                    orbs.append(float(meta_dict['number_of_orbits']))
                except ValueError:
                    orbs.append(np.nan)
                neut.append(meta_dict['neutrino_scheme'])
                mtot = float(meta_dict['id_mass'])
                f0 = float(meta_dict['id_gw_frequency_Hz']) #Mass-rescaled initial GW frequency (c=G=Msun=1 units)
            sim_mm, radii = get_mm(resos,sim)
            mismatches.append(sim_mm)
            all_radii.append(radii)
    else:                    ## NEW RELEASE
        this_path = os.path.join(rel2_path,dir)
        metamain = this_path + '/metadata_main.txt'
        sim = CoRe_sim(this_path)
        #dict = read_txt_into_dict(metamain)
        #run_folders = [ name for name in os.listdir(thispath) if os.path.isdir(os.path.join(thispath, name)) ]
        if len(sim.run.keys())==1:
            mismatches.append(nan)
            all_radii.append(nan)
        else:
            for res in sim.run.keys():
                #run_path = this_path + '/'  + res
                #h5dat = run_path + '/data.h5'
                #simrun = CoRe_run(run_path)
                simrun = sim.run[res]
                meta_dict = simrun.md.data
                resos.append(float(meta_dict['grid_spacing_min']))
                try:
                    orbs.append(float(meta_dict['number_of_orbits']))
                except ValueError:
                    orbs.append(np.nan)
                neut.append(meta_dict['neutrino_scheme'])
                mtot = float(meta_dict['id_mass'])
                f0 = float(meta_dict['id_gw_frequency_Hz'])
            if entry in weird_cases:
                sim_mm = nan
                radii = nan
            else:
                sim_mm, radii = get_mm(resos,sim)
            print(sim.run.keys(),resos,radii)
            mismatches.append(sim_mm)
            all_radii.append(radii)
    resos = []
    orbs = []
    neut = []


morbs = pd.Series(mismatches)
morbs = morbs.to_frame()
morbs.columns=['unfaithfulness']
P['unfaithfulness'] = morbs

radd = pd.Series(all_radii)
radd = radd.to_frame()
radd.columns=['radii']
P['radii']=radd

P.dropna()
P=P.fillna(0)
pd.set_option('display.max_rows', None)
pd.set_option('display.max_columns', None)
pd.set_option('display.width', None)
pd.set_option('display.max_colwidth', None)
print(P.loc[P['name']=='BAM:0066'])
print(P.loc[P['name']=='BAM:0097'])
print(P.loc[P['name']=='BAM:0100'])
#print(P.loc[P['unfaithfulness']<0.009])
color_code = {
    'THC': 'mediumslateblue'
}

colordict = {
    '-1.0': 'blue',
    '200.0': '#002F79',#'darkmagenta',#[182,74,248],
    '250.0': 'tab:blue',#'magenta',
    '300.0': '#0070FF',#'orchid',#[211,103,242],
    '350.0': '#00E8FF',#'blue',
    '400.0' : 'lightseagreen',#'#00FA92',#'#002F79',#[0,47,121],
    '450.0' : '#46DD00',#'#0070FF',#[0,112,255],
    '500.0': 'yellowgreen',#'mediumslateblue',#[0,169,255],
    '550.0': 'seagreen',#'#00E8FF',#[0,232,255],
    '600.0': 'gold',#'#00FA92',#[0,250,146],
    '650.0': 'orange',#'lightseagreen',
    '700.0': 'tab:orange',#'#46DD00',#[70,221,0],
    '750.0': '#FF6804',#'yellowgreen',#[125,248,3],
    '800.0': 'red',#'seagreen',#[186,255,59],
    '850.0': 'hotpink',#'gold',#[236,255,15],
    '900.0': 'magenta',#'orange',
    '1000.0': 'orchid',#'#FF006A',#[255,0,106],
    '1100.0': 'darkmagenta',#'tab:blue',
    '1200.0': 'mediumslateblue',#'hotpink',#[255,0,194],
    '1300.0': 'slateblue',#'red',#[255,0,194],
    '1400.0': 'tab:purple',#'tab:orange',#[255,0,194],
    'nan': 'crimson',#[244,187,246],
    nan : 'pink',
    '0': 'white'
}

tt = 10
rad_list = []
fig, ax = plt.subplots(1,1,figsize=(10,4))
plt.subplots_adjust(right=0.6)
for i_nam,nam in enumerate(P['name']):
    cc = P.at[i_nam,'radii']
    if type(P.at[i_nam,'unfaithfulness']) is list:        
        for un,unf in enumerate(P.at[i_nam,'unfaithfulness']):
            if cc[un] in rad_list:
                ax.scatter(nam,unf,marker='.',c=colordict[cc[un]])
            else:
                ax.scatter(nam,unf,marker='.',c=colordict[cc[un]],label=cc[un])
                rad_list.append(cc[un])
    elif type(P.at[i_nam,'unfaithfulness']) is float:
        if cc in rad_list:
            ax.scatter(nam,P.at[i_nam,'unfaithfulness'],marker='.',c=colordict[cc])
        else:
            ax.scatter(nam,P.at[i_nam,'unfaithfulness'],marker='.',c=colordict[cc],label=cc)
            rad_list.append(cc)
ax.xaxis.set_major_locator(MultipleLocator(tt))
ax.xaxis.set_minor_locator(MultipleLocator(1))
ax.tick_params(axis='x',which='major', length=4, color='k', rotation=75)
ax.tick_params(axis='both',which='minor', length=1.5, color='k')
handles, labels = plt.gca().get_legend_handles_labels()
handles, labels = zip(*[ (handles[i], labels[i]) for i in sorted(range(len(handles)), key=lambda k: list(map(int,map(float,labels)))[k])] )
labels=list(labels)
for il,lab in enumerate(labels):
    if lab=='-1.0' or lab==-1.0:
        labels[il]='Infinity'
labels=tuple(labels) #bbox_to_anchor=(1.19, 1) 0.66,1
leg = plt.legend(handles,labels,borderaxespad=0,fontsize='small', bbox_to_anchor=(1.02,1),ncol=2, markerscale=2.5, frameon='True', fancybox='False', facecolor='white',framealpha=1)
leg.get_frame().set_facecolor('white')
leg.get_frame().set_linewidth(0.9)
leg.get_frame().set_edgecolor('k')
plt.ylabel(r'$\Delta x$') 
ax.set_xlim([-1,170])
#ax.set_xbound(-1,len(np.array(P['name'])))
#plt.xlim([-1,len(np.array(P['name']))])
print(ax.get_xlim())
plt.semilogy()
plt.ylabel(r"Unfaithfulness")
plt.grid(True)
plt.tight_layout()
plt.savefig('index_mm.pdf')
plt.show()

unff = np.array(P)
one_unf = []
for uu in unff:
    try:
        one_unf.append(uu[0])
    except IndexError:
        one_unf = [uu]
eso = pd.Series(one_unf)
eso = eso.to_frame()
eso.columns=['unf']
P['unf']=eso
print(P)

#P.to_csv('mm_data.csv')


