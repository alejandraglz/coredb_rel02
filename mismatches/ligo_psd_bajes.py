import os
import numpy as np
import matplotlib.pyplot as plt

from bajes import MTSUN_SI
from bajes.obs.gw.noise import *

asd_L1 = get_design_sensitivity('L1')
asd_ET = get_design_sensitivity('ET')

freq, asdl1 = asd_L1
psd = asdl1*asdl1
f0 = 20
fmax = 1000
delta_f = 1.0/4
freq_psd = np.arange(f0,fmax,delta_f)
psd = np.interp(freq_psd,freq,psd)