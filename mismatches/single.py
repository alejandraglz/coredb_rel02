from cmath import nan
import matplotlib
import matplotlib.pyplot as plt
#matplotlib.use('TkAgg')
import numpy as np 
#from pycbc.types.frequencyseries import FrequencySeries 
import json
import h5py 
import pandas as pd
from matplotlib.ticker import (MultipleLocator, AutoMinorLocator)

from pycbc.filter import match, overlap, optimized_match
#from pycbc.filter.matchedfilter import optimized_match
from pycbc.types.timeseries import TimeSeries
from pycbc.psd import  aLIGODesignSensitivityP1200087, AdVDesignSensitivityP1200087, EinsteinTelescopeP1600143, aLIGOZeroDetHighPower #choose your PSD
from pycbc import waveform

from watpy.coredb.coredb import *
from watpy.wave.wave import *
from watpy.utils.coreh5 import *
from watpy.utils.ioutils import *

import sys
import os
sys.path.append('../')
from mismatch import *
from handy_functions import *

matplotlib.rcParams['text.usetex']= True
#matplotlib.rcParams['font.family'] = ['serif']
matplotlib.rcParams['font.serif']= 'Times New Roman'
matplotlib.rcParams['font.size']= 15 #28

Msuns  = 4.925491025543575903411922162094833998e-6
Mpc_m  = 3.085677581491367278913937957796471611e22
Msun_m = 1.476625061404649406193430731479084713e3

Njunk = 600
#fhigh = 2048
#flow = 20
dT = 1./4096.
alpha = 0.001

sim_path    = '/home/agonzalez/Documents/git/watpy/msc/CoRe_DB_clone/'
#data = csv_reader("../data/database.csv")
#P = get_df(data) #ready to go pandas df
cdb = CoRe_db(sim_path)
idb = cdb.idb

def get_rad(filename):
    name = re.match(r'R(\w+)_l(\d+)_m(\w+)_r(\w+).txt', filename)
    try:
        rad1   = rinf_str_to_float(name.group(4))
    except AttributeError:
        name2 = re.match(r'm(\d+)', name.group(4))
        rad1 = rinf_str_to_float(name2.group(1))
    return rad1


dbkeys = ['BAM:0066']
for entry in dbkeys:  ## CURRENT RELEASE
    print("Working with: ",entry)
    sim = cdb.sim[entry]
    #this_path = os.path.join(sim_path,dir) 

    resos=[]
    for res in sim.run.keys():
        simrun = sim.run[res]
        meta_dict = simrun.md.data
        resos.append(float(meta_dict['grid_spacing_min']))
        mtot = float(meta_dict['id_mass'])
        f0 = float(meta_dict['id_gw_frequency_Momega22']) #Mass-rescaled initial GW frequency (c=G=Msun=1 units)
        f0_Hz = float(meta_dict['id_gw_frequency_Hz'])

    # find highest and 2nd highest
    fmax = smax = 1000
    for ind,val in enumerate(resos):
        if val < fmax:
            smax = fmax
            fmax = val
            frmax = list(sim.run.keys())[ind]
            srmax = list(sim.run.keys())[ind-1]
        elif (val<smax and val!=fmax):
            smax = val
            srmax = list(sim.run.keys())[ind]

    if fmax==smax or np.unique(resos).size==1:
        sim_mm = np.nan
        radii = np.nan
    else:
        print(frmax,fmax,srmax,smax)
        ### Now get the waveforms
        srmax='R02'
        ## first highest
        simrun1 = sim.run[frmax]
        datah51 = simrun1.data
        dseth51 = datah51.read_dset()
        rh22_files1 = dseth51['rh_22'].keys()

        ## second highest
        simrun2 = sim.run[srmax]
        datah52 = simrun2.data
        dseth52 = datah52.read_dset()
        rh22_files2 = dseth52['rh_22'].keys() 

        for key1 in rh22_files1:
            rad1   = get_rad(key1)
            dset1 = dseth51['rh_22'][key1]
            hp1          = dset1[:,1][Njunk:]
            hc1          = dset1[:,2][Njunk:]
            t1           = dset1[:,0][Njunk:]
            ht1          = hp1 + 1j*hc1
            t1           = t1/mtot
            dt1          = t1[1]-t1[0]
            ap1 = np.abs(ht1)
            pp1 = -np.unwrap(np.angle(ht1))
            ff1 = np.abs(np.diff(pp1)/np.diff(t1))
            amrg = np.argmax(ap1)
            f1mrg = ff1[amrg]/(2*np.pi*mtot*Msuns)

            for key2 in rh22_files2:
                rad2   = get_rad(key2)

                if rad2!=rad1: 
                    continue
                else:
                    dset2 = dseth52['rh_22'][key2]
                    hp2          = dset2[:,1][Njunk:]
                    hc2          = dset2[:,2][Njunk:]
                    t2           = dset2[:,0][Njunk:]
                    ht2          = hp2 + 1j*hc2
                    t2           = t2/mtot
                    dt2          = t2[1]-t2[0]


                    ####   DO THE MISMATCHING ~~~~
                    
                    hp_nrTS   = TimeSeries(hp1, dT)
                    hc_nrTS   = TimeSeries(hc1, dT)
                    hp_nrTS_l   = TimeSeries(hp2, dT)
                    hc_nrTS_l   = TimeSeries(hc2, dT)

                    print("flow ", f0_Hz, ", fhigh ", f1mrg)
                    '''
                    plt.plot(freq_nr.sample_times,freq_nr,label='freq')
                    plt.plot(amp_nr.sample_times,amp_nr,label='amp')
                    plt.legend()
                    plt.show()
                    '''
                    # Tapering
                    L = hp_nrTS.duration # length of signal in seconds
                    hp_nrTS, _ = windowing(hp_nrTS, alpha/L)
                    L = hp_nrTS_l.duration
                    hp_nrTS_l, _ = windowing(hp_nrTS_l, alpha/L)

                    # Resize
                    tlen = max(len(hp_nrTS), len(hp_nrTS_l))
                    hp_nrTS.resize(tlen)
                    hp_nrTS_l.resize(tlen)
       
                    # Generate the aLIGO ZDHP PSD
                    delta_f = 1.0 / hp_nrTS.duration
                    flen    = tlen//2 + 1
                    #psd     = aLIGODesignSensitivityP1200087(flen, delta_f, f0_Hz)
                    psd = aLIGOZeroDetHighPower(flen, delta_f, f0_Hz)

                    #f1mrg = 1000
                    m,  _   = optimized_match(hp_nrTS, hp_nrTS_l, psd=psd, low_frequency_cutoff=f0_Hz, high_frequency_cutoff=f1mrg)
                    print("Radius ",rad1, "unfaithfulness: ",1.-m)
                    '''
                    plt.title("match= "+str(m))
                    plt.plot(hp_nrTS.sample_times,hp_nrTS,label="highest")
                    plt.plot(hp_nrTS_l.sample_times,hp_nrTS_l,'--',label="2nd highest")
                    plt.legend()
                    plt.show()
                    '''
        # now second and third highest
        print('Second and lowest resolutions:')
        if entry=='BAM:0066':
            simrun3 = sim.run['R03']
            datah53 = simrun3.data
            dseth53 = datah53.read_dset()
            rh22_files3 = list(dseth53['rh_22'].keys())  

            for key2 in rh22_files2:
                rad2   = get_rad(key2)
                dset1 = dseth51['rh_22'][key2]
                hp2          = dset2[:,1][Njunk:]
                hc2          = dset2[:,2][Njunk:]
                t2           = dset2[:,0][Njunk:]
                ht2          = hp2 + 1j*hc2
                t2           = t2/mtot
                ap2 = np.abs(ht2)
                pp2 = -np.unwrap(np.angle(ht2))
                ff2 = np.abs(np.diff(pp2)/np.diff(t2))
                amrg = np.argmax(ap2)
                f2mrg = ff2[amrg]/(2*np.pi*mtot*Msuns)

                for key3 in rh22_files3:
                    rad3   = get_rad(key3)

                    if rad2!=rad3: 
                        continue
                    else:
                        dset3 = dseth53['rh_22'][key3]
                        hp3          = dset3[:,1][Njunk:]
                        hc3          = dset3[:,2][Njunk:]
                        t3           = dset3[:,0][Njunk:]
                        ht3          = hp3 + 1j*hc3
                        t3           = t3/mtot
                        
                        ####   DO THE MISMATCHING ~~~~
                    
                        hp_nrTS   = TimeSeries(hp2, dT)
                        hc_nrTS   = TimeSeries(hc2, dT)
                        hp_nrTS_l   = TimeSeries(hp3, dT)
                        hc_nrTS_l   = TimeSeries(hc3, dT)

                        print("flow ", f0_Hz, ", fhigh ", f2mrg)
                  
                        # Tapering
                        L = hp_nrTS.duration # length of signal in seconds
                        hp_nrTS, _ = windowing(hp_nrTS, alpha/L)
                        L = hp_nrTS_l.duration
                        hp_nrTS_l, _ = windowing(hp_nrTS_l, alpha/L)

                        # Resize
                        tlen = max(len(hp_nrTS), len(hp_nrTS_l))
                        hp_nrTS.resize(tlen)
                        hp_nrTS_l.resize(tlen)
       
                        # Generate the aLIGO ZDHP PSD
                        delta_f = 1.0 / hp_nrTS.duration
                        flen    = tlen//2 + 1
                        #psd     = aLIGODesignSensitivityP1200087(flen, delta_f, f0_Hz)
                        psd = aLIGOZeroDetHighPower(flen, delta_f, f0_Hz)

                        #f1mrg = 1000
                        m,  _   = optimized_match(hp_nrTS, hp_nrTS_l, psd=psd, low_frequency_cutoff=f0_Hz, high_frequency_cutoff=f2mrg)
                        print("Radius ",rad2, "unfaithfulness: ",1.-m)                   
