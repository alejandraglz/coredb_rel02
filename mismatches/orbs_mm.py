from cmath import nan
import matplotlib
import matplotlib.pyplot as plt
#matplotlib.use('TkAgg')
import numpy as np 
#from pycbc.types.frequencyseries import FrequencySeries 
import json
import h5py 
import pandas as pd
from matplotlib.ticker import (MultipleLocator, AutoMinorLocator)
import csv

from watpy.coredb.coredb import *
from watpy.wave.wave import *
from watpy.utils.coreh5 import *
from watpy.utils.ioutils import *

import sys
import os
sys.path.append('../')
from mismatch import *
from handy_functions import *

matplotlib.rcParams['text.usetex']= True
#matplotlib.rcParams['font.family'] = ['serif']
matplotlib.rcParams['font.serif']= 'Times New Roman'
matplotlib.rcParams['font.size']= 15 #28

def read_unf(cheese):
    if type(cheese) is float:
        unf = cheese
    else:
        cheese = cheese.replace('[','')
        cheese = cheese.replace(']','')
        scheese = cheese.split(', ')
        unf = []
        for cc in scheese:
            try:
                unf.append(float(cc))
            except ValueError:
                unf.append(np.nan)
    return unf


P = pd.read_csv("mm_data.csv") #get_df(data) #ready to go pandas df
print(P)

## Add info about every run as a list
json_file = "../data/data.json"
jdict_list = read_json_into_dict(json_file)

reso = []
neut = []
orbs = []
for ip, pp in enumerate(P['name']):
    ss = []
    nn = []
    oo = []
    for dict in jdict_list:
        if not dict==None:
            if dict['database_key'].startswith(pp):
                ss.append(float(dict["resolution"]))
                if dict["neutrino_scheme"]!='':
                    nn.append(dict["neutrino_scheme"])
                else:
                    nn.append('None')
                try:
                    oo.append(float(dict["number_of_orbits"]))
                except TypeError:
                    oo.append(nan)
    reso.append(ss)  
    neut.append(nn)
    orbs.append(oo)

reso = pd.Series(reso)
reso = reso.to_frame()
reso.columns=['resolution']
neut = pd.Series(neut)
neut = neut.to_frame()
neut.columns=['neutrino_scheme']
orbs = pd.Series(orbs)
orbs = orbs.to_frame()
orbs.columns=['number_of_orbits']

P['resolution'] = reso
P['neutrino_scheme'] = neut
P['number_of_orbits'] = orbs

## correct eos
for i,ee in enumerate(P['eos']):
    if ee == "sly" or ee=="SLY":
        P.at[i,'eos']="SLy"
    elif ee == "MS1B":
        P.at[i,'eos']="MS1b"
    if P.at[i,'id_code']=='Sgrid':
        P.at[i,'id_code']='SGRID'
#print(np.array(P['unfaithfulness']))
P.loc[P["neutrino_scheme"]=="None", "neutrino_scheme"] = 'Hydro'
P.loc[P["neutrino_scheme"]==None, "neutrino_scheme"] = 'Hydro'
colordict = {
    'Hydro': '#8da0cb',
    'Leakage': '#66c2a5',
    'M0': '#fc8d62',
    None: 'black'
}

marker_dict = {
    'Hydro': '.',
    'Leakage': '^',
    'M0': '*',
    None: '.'
}
tt = 10
rad_list = []
fig, ax = plt.subplots(1,1,figsize=(10,4))

for i_nam,nam in enumerate(P['name']):
    unf = read_unf(P.at[i_nam,'unfaithfulness'])
    if type(P.at[i_nam,'number_of_orbits']) is list:
        noo = np.array(P.at[i_nam,'number_of_orbits'])[0]
        neus = np.array(P.at[i_nam,'neutrino_scheme'])[0]
    else:
        noo = P.at[i_nam,'number_of_orbits']
        neus = P.at[i_nam,'neutrino_scheme']
    
    if neus=='None':
        neus='Hydro'
    mark = marker_dict[neus]
    if neus in rad_list:
        for uu in unf:
            plt.scatter(noo,uu,marker=mark,c=colordict[neus])
    else:
        for uu in unf:
            plt.scatter(noo,uu,marker=mark,c=colordict[neus],label=neus)
        rad_list.append(neus)
handles, labels = plt.gca().get_legend_handles_labels()
#leg = plt.legend(handles[5:],labels[5:],borderaxespad=0,fontsize='small', bbox_to_anchor=(1.02,1),ncol=1, markerscale=2.5, frameon='True', fancybox='False', facecolor='white',framealpha=1)
leg = plt.legend(handles[5:],labels[5:],borderaxespad=0,fontsize='small',loc = 'lower left',ncol=3, markerscale=2.5, frameon='True', fancybox='False', facecolor='white',framealpha=1)
leg.get_frame().set_facecolor('white')
leg.get_frame().set_linewidth(0.9)
leg.get_frame().set_edgecolor('k')
plt.semilogy()
plt.xlim([2,18.5])
plt.ylabel(r"Unfaithfulness")
plt.xlabel(r'Number of Orbits')
plt.grid(True)
plt.tight_layout()
plt.savefig('orbits_mm.pdf')
plt.show()



