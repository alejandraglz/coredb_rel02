import numpy as np
from handy_functions import *
from bbh_fits import *


# based on Breschi:2022
################################################
# MERGER FITS
################################################
def amrg_fits(xxx):
    '''
    Sum res. sq. = 0.11334091499329652  -> chi^2
    Rel. st. dev. = 0.026290936426882874  -> 1-sigma error %
    Rsquare: 0.9495533416536159  -> determination coefficient
    '''
    kkk,sss,qqq = xxx # kappa2t, hat{S}, Xnu= 1-4nu
    a0, a1, a2, a3, a4 = [5.57289760e-01, 5.59383085e-02, 1.00000000e-06, 1.27558282e-01, 6.79867877e-05]
    aM, b1, b2, b3, b4 = [ 5.27498424, -0.02514124, -2.  ,       11.09065224 , 9.72420669]
    a1S, b1S = [  0.31113719 -39.21145519]
    qM = 1 + aM*qqq
    p1S = a1S * (1 + b1S*qqq)
    qS = 1 + p1S * sss
    p1T = a1*(1+b1*qqq)
    p2T = a2*(1+b2*qqq)
    p3T = a3*(1+b3*qqq)
    p4T = a4*(1+b4*qqq)
    _up = (1.+ p1T * kkk + p2T * kkk**2)
    _lo = (1.+ p3T * kkk + p4T * kkk**2)
    qT = _up / _lo
    return a0 * qM * qS * qT

def fmrg_fits(xxx):
    '''
    Sum res. sq. =  0.3299352155581652
    Rel. st. dev. =  0.045742217553160235
    Rsquare: 0.9254566757205078
    '''
    kkk,sss,qqq = xxx
    a0, a1, a2, a3, a4 = [0.22754806,4.85286918e-02, 5.86329048e-06, 1.00000000e-01, 1.86774027e-04]
    aM, b1, b2, b3, b4 = [  0.8 ,         1.80784081, 599.99991982,   7.80555492,  84.76057029]
    a1S, b1S = [ 0.25332088, -1.99999622]
    qM = 1 + aM*qqq
    p1S = a1S * (1 + b1S*qqq)
    qS = 1 + p1S * sss
    p1T = a1*(1+b1*qqq)
    p2T = a2*(1+b2*qqq)
    p3T = a3*(1+b3*qqq)
    p4T = a4*(1+b4*qqq)
    _up = (1.+ p1T * kkk + p2T * kkk**2)
    _lo = (1.+ p3T * kkk + p4T * kkk**2)
    qT = _up / _lo
    return a0 * qM * qS * qT

def breschi_fmrg(xxx):
    kkk,sss,qqq = xxx
    a0, a1, a2, a3, a4 = [0.22754806, 0.03445340731627873, 5.5799962023491245e-06, 0.08404652974611324, 0.00011328992320789428]
    aM, b1, b2, b3, b4 = [0.92330138, 13.828175998146255, 517.4149218303298, 12.74661916436829, 139.76057108600236 ]
    a1S, b1S = [0.59374838, -1.99378496 ]
    qM = 1 + aM*qqq
    p1S = a1S * (1 + b1S*qqq)
    qS = 1 + p1S * sss
    p1T = a1*(1+b1*qqq)
    p2T = a2*(1+b2*qqq)
    p3T = a3*(1+b3*qqq)
    p4T = a4*(1+b4*qqq)
    _up = (1.+ p1T * kkk + p2T * kkk**2)
    _lo = (1.+ p3T * kkk + p4T * kkk**2)
    qT = _up / _lo
    return a0 * qM * qS * qT 

def lumpeak_fits(xxxxx):
    '''
    Output: Lpeak/nu

    Sum res. sq. =  2.2354881995054052
    Rel. st. dev. =  0.11965269412596767
    Rsquare: 0.9608482731933725
    '''
    m1,m2,a1,a2,lam = xxxxx # a1, a2 = dimensionless spins, lam= kappa^T_2
    nu = q_to_nu(m1/m2)
    delta   = np.sqrt(1.0 - 4.0*nu)
    m1M     = 0.5*(1.0 + delta)
    m2M     = 0.5*(1.0 - delta)
    a    = m1M*m1M*a1 + m2M*m2M*a2 #Shat

    p110,p111,p120,p121,p210,p211,p220,p221,p310,p311,p320,p321, p130,p131,p230,p231,p330,p331 = [7.985233584810662, 1.7039580852043068, -18.906726735326565, -6.219850198012944, 
    0.5896842948306272, 0.6113884548446448, -5.8264596642328685, -4.969303454433424, -3.529942383968704, 7.987389880308377, 16.736263693752324, -19.464663501324544, 
    -17.476529102390426, 4.58551692624138, 13.911379123438348, 10.10527014100211, 11.612523677909484, -29.9635878885439]

    p11 = p110*a + p111
    p12 = p120*a + p121
    p13 = p130*a + p131
    p21 = p210*a + p211
    p22 = p220*a + p221
    p23 = p230*a + p231
    p31 = p310*a + p311
    p32 = p320*a + p321
    p33 = p330*a + p331
    p1 = p11*nu + p12*nu**2 + p13*nu**3
    p2 = p21*nu + p22*nu**2 + p23*nu**3
    p3 = p31*nu + p32*nu**2 + p33*nu**3
    _up = 1 + p1*lam + p2*lam**2
    _down = (1 + p3*p3*lam)**2
    model = _up/_down
    return LpeakUIB2016(m1, m2, a1, a2)[0]*model

################################################
# POST-MERGER FITS
################################################
def Mf2_fits(xxx):
    '''
    Sum res. sq. =  0.06734350794837893
    Rel. st. dev. =  0.03633814580377815
    Rsquare:  0.958390445438881
    '''
    kkk,sss,qqq = xxx
    a0, a1, a2, a3, a4 = [8.99999991e-02, 2.94005743e-02, 3.78475217e-05, 5.75133584e-02, 2.77152754e-04]
    aM, b1, b2, b3, b4 = [31.02092983,  1.13548491, -0.99999994, 39.99838712, 27.7748322 ]
    a1S, b1S = [0.07423639, 29.99999877]
    qM = 1 + aM*qqq
    p1S = a1S * (1 + b1S*qqq)
    qS = 1 + p1S * sss
    p1T = a1*(1+b1*qqq)
    p2T = a2*(1+b2*qqq)
    p3T = a3*(1+b3*qqq)
    p4T = a4*(1+b4*qqq)
    _up = (1.+ p1T * kkk + p2T * kkk**2)
    _lo = (1.+ p3T * kkk + p4T * kkk**2)
    qT = _up / _lo
    return a0 * qM * qS * qT

##### Fits of type Mf2(Rx), Rx= Radius of a NS with mass x
def Mf2Rx(r, mass=1.4):
    '''
    R_1.4 Fits
    Sum res. sq. =  0.5541934023308421
    Rel. st. dev. =  0.059794993825368105
    Rsquare:  0.90065048887692

    R_1.8 Fits
    Sum res. sq. =  0.31162077629860563
    Rel. st. dev. =  0.044837981251298675
    Rsquare:  0.9495186447018464
    '''
    if mass==1.4:
        a = [ 0.24195131, -0.10057161,  0.01134283] # R1.4 fits
    elif mass==1.8:
        a = [ 0.23655214 ,-0.10205601,  0.01218261] # R1.8 fits
    else:
        print("Error: Fit only available for mass=1.4 or 1.8")
    return a[0] + a[1]*r + a[2]*r**2

#### Fits of type Mf2(Rx,R1.4/R1.8), Rx= Radius of a NS with mass x
def Mf2RxR48(R, mass=1.4):
    '''
    R_1.4 Fits
    Sum res. sq. =  0.3158863057067902
    Rel. st. dev. =  0.04514301913066312
    Rsquare:  0.9491644882589618

    R_1.8 Fits
    Sum res. sq. =  0.30517495128328487
    Rel. st. dev. =  0.04437164885464687
    Rsquare:  0.9523115477096047
    '''
    r, ratio = R # radius r = R/M, and ratio = R_1.4 / R_1.8
    if mass==1.4:
        a = [0.15264525, -0.11179275,  0.01385721, 0.09765477] # R1.4 fits
    elif mass==1.8:
        a = [ 0.20412987, -0.10090511,  0.01223752 , 0.02776716] # R1.8 fits
    else:
        print("Error: Fit only available for mass=1.4 or 1.8")
    return a[0] + a[1]*r + a[2]*r**2 + a[3]*ratio