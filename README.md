# README #

Scripts and fits to work with for the 2nd release of CoReDB.

### Contents ###

* check_data/ : scripts to ensure everything is in CoRe convention
* stats/ : scripts to create histograms of masses, mass ratios, etc to describe what is inside
* breschi_fits/ : adapted merger and postmerger fits for the entire DB 

### TO DO ###

* Turn the pre-release into the release: Check the metadata are complete and visually inspect the data.
* Analyse the DB data (both new and old) for: some basic stats (histograms of masses, mass ratios, etc to describe what is inside), repeat the merger and postmerger fits of Matteo (ask/adapt the scripts) for the entire DB (in his paper he used only a subset for the fits and subset for validating). We want these scripts to be ready to run on the DB data.
* Start preparing a paper with some discussion of this (CQG).
* Push the data on the core git.