import os
import numpy as np
import matplotlib.pyplot as plt
from scipy.ndimage import gaussian_filter1d
from scipy.signal import savgol_filter

from bajes import MTSUN_SI
from bajes.obs.gw.strain import fft, bandpassing
from bajes.pipe import ensure_dir
from bajes.obs.gw.utils import compute_lambda_tilde, compute_chi_eff


from watpy.coredb.coredb import *
from watpy.utils.coreh5 import *
from watpy.utils.ioutils import *
from watpy.utils.num import *

import sys
sys.path.append('../')
import csv
from handy_functions import *

lms = [(2,2),(2,1),(2,0),(3,3),(4,4)]

eos_dir = '../../tov/Sequences/Stable'
avail_eos = [eos.split('_')[0] for eos in os.listdir(eos_dir) if 'sequence' in eos]

def get_rad(filename):
    name = re.match(r'R(\w+)_l(\d+)_m(\w+)_r(\w+).txt', filename)
    try:
        rad1   = rinf_str_to_float(name.group(4))
    except AttributeError:
        name2 = re.match(r'm(\d+)', name.group(4))
        rad1 = rinf_str_to_float(name2.group(1))
    return rad1


def fixed_freq_int(signal, cutoff, dt=1):
    """
        Fixed frequency double time integration

        * signal : a np array with the target signal
        * cutoff : the cutoff frequency
        * dt     : the sampling of the signal
    """
    from scipy.fftpack import fft, ifft, fftfreq

    f = fftfreq(signal.shape[0], dt)

    idx_p = np.logical_and(f >= 0, f < cutoff)
    idx_m = np.logical_and(f <  0, f > -cutoff)

    f[idx_p] = cutoff
    f[idx_m] = -cutoff

    return ifft(-fft(signal)/(2*np.pi*f)**2)

def find_freqdomain_peaks(amp, freq, distance=None, prominence=None, width=None):
    
    from scipy.signal import find_peaks
    ipeaks, _ = find_peaks(amp, distance=int(distance), prominence=(prominence,None), width=(width,None))

    _f = freq[ipeaks]
    _a = amp[ipeaks]
    
    isort = np.argsort(_a)
    _f = _f[isort]
    _a = _a[isort]

    prim_frq = _f[-1]
    prim_amp = _a[-1]

    sec_frq = _f[-2]
    sec_amp = _a[-2]
    
    if prim_frq > sec_frq:
        return sec_frq, sec_amp, prim_frq, prim_amp
    else:
        return prim_frq, prim_amp, sec_frq, sec_amp

def find_freqdomain_peaks22(amp, freq, distance=None, prominence=None, width=None):
    
    prim_frq = freq[np.argmax(amp)]
    prim_amp = amp[np.argmax(amp)]
    
    __f = freq[:np.argmax(amp)]
    __a = amp[:np.argmax(amp)]
    __f = __f[:np.argmin(__a)]
    __a = __a[:np.argmin(__a)]

    from scipy.signal import find_peaks
    ipeaks, _ = find_peaks(__a, distance=int(distance), prominence=(prominence,None), width=(width,None))

    _f = __f[ipeaks]
    _a = __a[ipeaks]
    
    try:
    
        try :
            sec_frq = _f[np.argmax(_a)]
            sec_amp = _a[np.argmax(_a)]
        except:
            sec_frq = __f[np.argmax(__a)]
            sec_amp = __a[np.argmax(__a)]

    except:
    
        sec_frq = freq[0]
        sec_amp = amp[0]
    
    
    return prim_frq, prim_amp, sec_frq, sec_amp

def ranges(nums):
    nums = sorted(set(nums))
    gaps = [[s, e] for s, e in zip(nums, nums[1:]) if s+1 < e]
    edges = iter(nums[:1] + sum(gaps, []) + nums[-1:])
    return list(zip(edges, edges))

def get_timedomain_peaks(amp, time, threshold=5):

    damp    = np.gradient(amp, time)

    # get maxima
    imaxs   = np.array([max(ri) for ri in ranges(np.concatenate(np.where(damp>=0)))])
    # remove element within threshold
    good_i  = np.diff(imaxs)>=threshold

    # filling groups
    groups  = [[imaxs[0]]]
    ix = 1
    
    while ix < len(imaxs)-1:

        if good_i[ix]:
            groups.append([imaxs[ix]])
            ix += 1
        else:
            groups[-1].append(imaxs[ix])
            ix += 1

    y_nodes  = np.array([np.mean(amp[gi]) for gi in groups])
    x_nodes  = np.array([np.mean(time[gi]) for gi in groups])

    i_prim       = np.argmax(y_nodes)
    primary_time = x_nodes[i_prim]
    primary_peak = y_nodes[i_prim]

    x_nodes     = np.delete(x_nodes, i_prim)
    y_nodes     = np.delete(y_nodes, i_prim)

    try:
        i_sec       = np.argmax(y_nodes)
        secondary_time = x_nodes[i_sec]
        secondary_peak = y_nodes[i_sec]
    except ValueError: # no secondary peak
        secondary_time = 0
        secondary_peak = 0
    
    if secondary_time < primary_time:
        return secondary_time, secondary_peak, primary_time, primary_peak
    else:
        return primary_time, primary_peak, secondary_time, secondary_peak


def plot_time_series(h, t, out, title, tmerg, label, **kwargs):

    fig = plt.figure()
    plt.title(title)
    plt.plot(t, np.real(h), c='k')
    plt.plot(t, np.abs(h), c='orangered')
    
    if label=='PC' or label=='22':
        plt.scatter(kwargs['t_0']+tmerg,kwargs['a_0'], marker='o',color='b',label='comp', zorder=8)
        plt.scatter(tmerg,kwargs['a_merg'], marker=(5, 1),color='r',label='merg', zorder=8)
    else:
        plt.axvline(kwargs['t_0']+tmerg,color='b', label='comp', zorder=8)
        plt.axvline(tmerg,color='r',label='merg', zorder=8)

    if label=='PC':
        plt.scatter(kwargs['t_pm']+tmerg,kwargs['a_pm'], marker='v',color='g',label='pc')

    if not kwargs['PC']:
        lm = label
        plt.scatter(kwargs['t_pm']['1']+tmerg,kwargs['a_pm']['1'], marker='v',color='m',label='pm-1')
        plt.scatter(kwargs['t_pm']['2']+tmerg,kwargs['a_pm']['2'], marker='v',color='c',label='pm-2')
        plt.scatter(kwargs['t_pm']['3']+tmerg,kwargs['a_pm']['3'], marker='v',color='g',label='pm-3')
        if kwargs['AH_exists']=='True': 
            plt.axvline(kwargs['t_pm']['collapse']+tmerg,color='k',label='collapse', ls='-.')

    plt.xlim((tmerg-100, np.max(t)))
    plt.legend(loc='best')
    return fig

def plot_freq_evolut(h, t, out, title, tmerg, label, i0=None, **kwargs):

    fig = plt.figure()
    plt.title(title)
    plt.plot(t, h, c='k')
    __df__ = np.gradient(h,t)
    plt.plot(t, __df__, c='maroon')
    plt.axhline(np.mean(__df__), ls=':', c='brown', label='Mean={}'.format(np.mean(__df__)))

    plt.axvline(kwargs['t_0']+tmerg,color='b', label='comp', zorder=8)
    plt.axvline(tmerg,color='r',label='merg', zorder=8)

    if label=='PC':
        plt.scatter(kwargs['t_pm']+tmerg,kwargs['a_pm'], marker='v',color='g',label='pc')

    if not kwargs['PC']:
        lm = label
        plt.axvline(kwargs['t_pm']['1']+tmerg,color='m',label='pm-1')
        plt.axvline(kwargs['t_pm']['2']+tmerg,color='c',label='pm-2')
        plt.axvline(kwargs['t_pm']['3']+tmerg,color='g',label='pm-3')
        if kwargs['AH_exists']=='True':
            plt.axvline(kwargs['t_pm']['collapse']+tmerg,color='k',label='collapse', ls='-.')

    if not kwargs['PC']:
        
        plt.plot([kwargs['t_pm']['1']+tmerg, kwargs['t_pm']['3']+tmerg],
                 [kwargs['f_pm']+kwargs['delta_f2'], kwargs['f_pm']+kwargs['delta_f2']],
                 c='royalblue')
        plt.plot([kwargs['t_pm']['1']+tmerg, kwargs['t_pm']['3']+tmerg],
                 [kwargs['f_pm']-kwargs['delta_f2'], kwargs['f_pm']-kwargs['delta_f2']],
                 c='royalblue', label='FREQ-DELTA')
        
    if i0 != None:
        imerg = np.min(np.where(t>=tmerg))

        fr_merg = kwargs['f_merg'] + kwargs['df_merg']*(t[imerg:i0]-tmerg)
        plt.plot(t[imerg:i0], fr_merg, color='orangered',label='FREQ-MERG', ls='--')
        
        if not kwargs['PC']:
            icoll = kwargs['t_pm']['collapse']
            if icoll == None:
                icoll = -1
            else:
                icoll = np.min(np.where(t>=icoll+tmerg))
            fr_2    = kwargs['f_pm'] + kwargs['df_2']*(t[i0:icoll]-t[i0])
            plt.plot(t[i0:icoll], fr_2, color='forestgreen',label='FREQ-2', ls='--')

    plt.xlim((tmerg-100, np.max(t)))
    try:
        plt.ylim((0, np.max(h[100:-100])))
    except ValueError:
        pass
    plt.legend(loc='best')
    print("TMERG == {}".format(tmerg))
    return fig

def plot_freq_series(h, f, out, title, label, **kwargs):

    fig = plt.figure()
    plt.title(title)
    plt.loglog(f, np.abs(h), c='k')
    plt.axvline(kwargs['f_merg'], ls='--', c='c', label='merg')
    
    if label=='PC':
        plt.scatter(kwargs['f_pm'],kwargs['h_pm'], marker='v',color='g',label='pc')
        plt.xlim((kwargs['f_merg']-0.01, np.max(f)))
    
    if not kwargs['PC']:

        #plt.scatter(kwargs['f_pm']['comp'],kwargs['h_pm']['comp'], marker='s',color='m',label='compact')
        plt.scatter(kwargs['f_pm'],kwargs['h_pm'], marker='v',color='g',label='dominant')
        plt.xlim((kwargs['f_merg']-0.01, np.max(f)))
    
    plt.legend(loc='best')
    return fig

def get_info(this_path, dir, target, res, simrun, results_dir_run):

    meta_dict = simrun.md.data
    
    simname = meta_dict['simulation_name']
    figs = []
    splitdir = simname.split('_')
        
    this_info = {}
    this_info['database_key'] = meta_dict['database_key']
    m1  = float(splitdir[1])
    m2  = float(splitdir[2])
    
    if len(splitdir)> 7:
        tag = 'GRHD-'+'-'.join(splitdir[7:])
    else:
        tag = 'GRHD'

    this_info['eos']    = splitdir[0]
    this_info['code']   = target
    this_info['tag']    = tag
    this_info['resolution']   = float(meta_dict['grid_spacing_min'])
    this_info['mtot']   = float(meta_dict['id_mass'])
    this_info['q']      = float(meta_dict['id_mass_ratio'])
    this_info['mchirp'] = this_info['mtot'] * (this_info['q']/(1.+this_info['q'])**2.)**0.6

    if (':' in splitdir[3]) or (':' in splitdir[4]):
        print(" - Skipping precessing simulations")
        return None

    s1  = meta_dict['id_spin_starA'].split(',')[2]
    s2  = meta_dict['id_spin_starB'].split(',')[2]


    this_info['m1']         = float(meta_dict['id_mass_starA'])
    this_info['m2']         = float(meta_dict['id_mass_starB'])
    this_info['s1z']        = float(s1)
    this_info['s2z']        = float(s2)
    this_info['chi_eff']    = compute_chi_eff(this_info['m1'],this_info['m2'],this_info['s1z'],this_info['s2z'])

    print(" - Extracting properties")

    # set data directory
    data_dir = this_path+'/'+res

    # set repository
#    ensure_dir(os.path.join(outdir))

    # get metadata
    print(" - Reading metadata")
    this_info['m1_rest']    = float(meta_dict['id_rest_mass_starA'])
    this_info['m2_rest']    = float(meta_dict['id_rest_mass_starB'])
    this_info['lambda1']    = float(meta_dict['id_Lambdaell_starA'].split(',')[0])
    this_info['lambda2']    = float(meta_dict['id_Lambdaell_starB'].split(',')[0])
    this_info['m_adm']      = float(meta_dict['id_ADM_mass'])
    this_info['j_adm']      = float(meta_dict['id_ADM_angularmomentum'])
    this_info['omega0']     = float(meta_dict['id_gw_frequency_Momega22'])

    # check sign
    if this_info['m2_rest'] > this_info['m1_rest']:
        this_info['m2_rest'], this_info['m1_rest'] = this_info['m1_rest'], this_info['m2_rest']
        this_info['lambda1'], this_info['lambda2'] = this_info['lambda2'], this_info['lambda1']
    
    this_info['lambdat'] = compute_lambda_tilde(this_info['m1'],this_info['m2'],this_info['lambda1'],this_info['lambda2'])
    this_info['kappa2t'] = (3./16.)*this_info['lambdat']

    # get radius and max/rad-max
    if this_info['eos'] in avail_eos:
        ## rhoc M Mb R C kl lam
        m_eos, r_eos = np.genfromtxt(eos_dir+'/{}'.format(this_info['eos'])+'_sequence.txt', usecols=[1,3], unpack=True)
        
        this_info['r1']         = np.interp(this_info['m1'], m_eos, r_eos)
        this_info['r2']         = np.interp(this_info['m2'], m_eos, r_eos)
        this_info['M_max_eos']  = np.max(m_eos)
        this_info['R_max_eos']  = r_eos[np.argmax(m_eos)]

    # get extraction radius, i.e. Rh_l2_m2_r00400.txt
    listdata    = os.listdir(data_dir)
    datah5 = simrun.data
    dseth5 = datah5.read_dset()

    try:
        horizon = dseth5['horizon'].keys()
        AH_exists = True
        this_info['AH_exists'] = 'True'
    except:
        AH_exists = False
        this_info['AH_exists'] = 'False'

    energy_file = False
    # energetics
    try:
        energy_files = dseth5['energy'].keys()
        for key in energy_files:
            dset = dseth5['energy'][key]
            #read
            jorb = dset[:,0] # Jorb
            uM = dset[:,2] # u/M
            ebin = dset[:,1] # binding energy
            erad = dset[:,3] # radiated energy
            erad_max = np.max(erad)
            nan = np.nan
            if erad_max>1 or erad_max==nan:
                continue
            else:
                print("Max E_GW: ",erad_max)
                all_erad_max.append(erad_max)
            lum = np.gradient(erad,uM) #luminosity
            energy_file = True
            try:
                jrad = dset[:,4] # Jrad
            except IndexError:
                print('energetics with ony 3 columns')
    except KeyError:
        print('No energetics available')

    rh22_files = dseth5['rh_22'].keys()
    
    for key in rh22_files:
        radius = get_rad(key)
        dset = dseth5['rh_22'][key]

        # read
        hp          = dset[:,1]
        hc          = dset[:,2]
        t           = dset[:,0]
        ht          = hp + 1j*hc
        h_dot       = diff1(t,ht)
        lumi       = 1.0/(16*np.pi) * np.abs(h_dot)**2
        t           = t/this_info['mtot']
        dt          = t[1]-t[0]
        this_info['dt'] = dt

        ## tapering
        #tmerg       = t[np.argmax(np.abs(h))]
        #_w          = (tmerg-t[np.min(np.where(np.abs(h)>0.01))])
        #tfilt_shift = _w/2.
        #_tw         = 100
        #filter      = 1./(1.+np.exp(-(t-tmerg+tfilt_shift)/_tw))
        filter      = np.ones(len(hp))

        _n = len(hp)//2

        hp = np.append(np.zeros(_n),np.append(hp*filter, np.zeros(_n)))
        hc = np.append(np.zeros(_n),np.append(hc*filter, np.zeros(_n)))
        h   = (hp - 1j*hc)

        # compute FFT
        fr, hf  = fft(hp, dt)

        amp22   = np.abs(ht)
        imerg   = np.argmax(amp22)
        if amp22[imerg]>100:
            amp22 = amp22/radius
        phi22   = np.unwrap(np.angle(ht))
        #f22     = np.gradient(phi22,t)/(2.*np.pi)
        f22     = gaussian_filter1d(phi22, sigma=1, order=1, mode='wrap')/(2.*np.pi)/dt

        damp22  = np.gradient(amp22,t)
        tmerg   = t[imerg]
    
        # general properties
        this_info['a_merg'] = amp22[imerg]
        this_info['f_merg'] = f22[imerg]
        this_info['lpeak'] = lumi[imerg]
        if energy_file==True:
            imerg2 = np.where((uM<tmerg+0.1))[-1]
            print(tmerg,uM[imerg2[-1]])
            this_info['jorb_merg'] = jorb[imerg2[-1]]
            this_info['er_merg'] = erad[imerg2[-1]]
            this_info['eb_merg'] = ebin[imerg2[-1]]
            #this_info['lpeak'] = lum[imerg2[-1]]
            try:
                this_info['jr_merg'] = jrad[imerg2[-1]] 
            except:
                print('')
            

        if this_info['f_merg'] < 0 :
            this_info['f_merg'] = -this_info['f_merg']
            f22 = -f22

    #    if this_info['f_merg'] > this_info['a_merg']:
    #        this_info['f_merg'] = f22[imerg-1]
    #        if this_info['f_merg'] > this_info['a_merg']:
    #            this_info['f_merg'] = f22[imerg-2]
        i0 = -1
        for i,dai in enumerate(damp22):
            if i > imerg and dai >= 0 and amp22[i]<0.4*this_info['a_merg']:
                this_info['a_0'] = amp22[i]
                this_info['t_0'] = t[i] - tmerg
                this_info['phi_kick'] = phi22[i+1] - phi22[i]
                i0 = i
                break

        if i0<0 :
            this_info['a_0'] = None
            this_info['t_0'] = None
            this_info['phi_kick'] = None
        else:
            this_info['df_merg'] = np.mean(np.gradient(f22[imerg:(imerg+2*i0)//3],t[imerg:(imerg+2*i0)//3]))

            # check PC
            is_pc = True
            i_pm  = np.where(fr>=this_info['f_merg'])

            # if spectrum above f_merg is larger than |h(f_merg)| + 10%, then postmerger exists
            x = 0.20/this_info['q']**2
            if np.max(np.abs(hf[np.min(i_pm)+1:])) > np.abs(hf[np.min(i_pm)])*(1. + x) and np.max(amp22[i0:]) > (1./8.)*this_info['a_merg']:
                print(" - Revealed loud postmerger signal, simulation labelled as non-prompt-collapse")
                is_pc = False
            # othwerwise is prompt-collapse
            else:
                print(" - Postmerger signal is weak or absent, simulation labelled as prompt-collapse")

            this_info['PC'] = is_pc

            # include BH properties estimations?!

            if is_pc:
                
                print(" - Extracting PC properties")

                _hpc    = ht[i0:]
                _fr, _hf  = fft(np.real(_hpc), dt)
                _i_pm  = np.where(_fr>=this_info['f_merg'])
                
                this_info['a_pm']   = np.max(np.abs(_hpc))
                this_info['t_pm']   = t[i0+np.argmax(np.abs(_hpc))]-tmerg
                this_info['f_pm']   = _fr[_i_pm][np.argmax(np.abs(_hf[_i_pm]))]
                this_info['h_pm']   = np.max(np.abs(_hf[_i_pm]))

                figs.append(plot_time_series(_hpc, t[i0:], out='', title=dir, tmerg=tmerg, label='PC', **this_info))
                figs.append(plot_freq_evolut(f22[i0:], t[i0:], out='', title=dir, tmerg=tmerg, label='PC', i0=i0, **this_info))
                figs.append(plot_freq_series(_hf, _fr, out='', title=dir, label='PC', **this_info))

            else:
                
                this_info['a_pm']   = {}
                this_info['t_pm']   = {}
        #        this_info['f_pm']   = {}
        #        this_info['h_pm']   = {}

                ### freq domain
                _h22pm  = hf[i_pm]
                _fpm    = fr[i_pm]
                __f1,__a1,__f2,__a2 = find_freqdomain_peaks22(np.abs(_h22pm),_fpm, distance=2./float(this_info['resolution']))
                
                if __f1 > __f2:
                    this_info['f_pm'] = __f1
                    this_info['h_pm'] = __a1
                else:
                    this_info['f_pm'] = __f2
                    this_info['h_pm'] = __a2

                ### time domain
                _h22pm  = ht[i0:]
                _tpm    = t[i0:]

                #this_info['a_pm'][lm]['first']   = np.max(np.abs(_h22pm))
                #this_info['t_pm'][lm]['first']   = _tpm[np.argmax(np.abs(_h22pm))]-tmerg
                __t1,__a1,__t2,__a2 = get_timedomain_peaks(np.abs(_h22pm), _tpm, threshold=1./float(this_info['resolution']))
            
                this_info['a_pm']['1']   = __a1
                this_info['t_pm']['1']   = __t1 - tmerg
                this_info['a_pm']['3']   = __a2
                this_info['t_pm']['3']   = __t2 - tmerg

                _i3_ = np.where((_tpm>__t1) & (_tpm<__t2))
                ___h22pm  = _h22pm[_i3_]
                ___tpm    = _tpm[_i3_]

                this_info['a_pm']['2'] = np.min(np.abs(___h22pm))
                this_info['t_pm']['2'] = ___tpm[np.argmin(np.abs(___h22pm))] - tmerg

                # estimate frequency-modulation delta
                _fgw_pm   = f22[i0:]
                _df22     = np.diff(_fgw_pm)


                _i_amp_peaks = []
                for _i_,_df in enumerate(_df22):
                    try:
                        if np.sign(_df) !=  np.sign(_df22[_i_+1]):
                            _i_amp_peaks.append(_i_)
                        if len(_i_amp_peaks)>5:
                            #_i_delta = _i_
                            break
                    except IndexError:
                        continue

                this_info['delta_f2'] = np.max(np.abs(this_info['f_pm'] - _fgw_pm[_i_amp_peaks]))

                # estimate time of collapse
                if AH_exists:

                    # get time of collapse
                    i_weak  = np.concatenate(np.where(np.abs(_h22pm)<this_info['a_0']/4.))
                    di_weak = np.diff(i_weak)

                    set_weak = [[]]
                    for ix ,di in enumerate(di_weak):
                        if di == 1:
                            set_weak[-1].append(i_weak[ix])
                        else:
                            set_weak[-1].append(i_weak[ix])
                            set_weak.append([])

                    set_weak[-1].append(i_weak[-1])

                    this_info['t_pm']['collapse'] = _tpm[np.min(set_weak[-1])] - tmerg
                
                else:
                
                    this_info['t_pm']['collapse'] = None

                # estimate rotating frequency evolution
                __istart  = 0
                _sign_check = np.sign(f22[i0])
                if _sign_check >0 :
                    for _j_ in range(len(f22)-i0):
                        if f22[i0 + _j_] <= this_info['f_pm']:
                            __istart  = i0+_j_
                            break
                else:
                    for _j_ in range(len(f22)-i0):
                        if f22[i0 + _j_] >= this_info['f_pm']:
                            __istart  = i0+_j_
                            break

                __icoll_2 = np.max(np.where(f22>=this_info['f_pm']))
                #this_info['df_2'] = np.mean(gaussian_filter1d(f22[__istart:__icoll_2-5], sigma=1, order=1, mode='wrap'))/dt
                try:
                    __df_2__ = np.mean(np.diff(savgol_filter(f22[__istart:__icoll_2-15*int(1./this_info['resolution'])],
                                                            10*int(1./this_info['resolution'])+1, 2)))/dt
                except:
                    __df_2__ = 0.

                this_info['df_2'] = __df_2__

                if __t1==0:
                    this_info['error'] = 'ONLY_1_PEAK'

            # plots
            '''
            figs.append(plot_time_series(ht, t, out='', title=dir, tmerg=tmerg, label='22', **this_info))
            plot_dir = results_dir_run+'/time_series.pdf'
            plt.savefig(plot_dir)
            figs.append(plot_freq_evolut(f22, t, out='', title=dir, tmerg=tmerg, label='22',i0=i0, **this_info))
            plot_dir = results_dir_run+'/freq_evolut.pdf'
            plt.savefig(plot_dir)
            figs.append(plot_freq_series(hf, fr, out='', title=dir, label='22', **this_info))
            plot_dir = results_dir_run+'/freq_series.pdf'
            plt.savefig(plot_dir)
            
            #plt.show()
            '''
            print("Extracted data: ", this_info)
            
    return this_info

if __name__ == "__main__":

    targets     = ['THC', 'BAM']
    sim_path    = '/data/numrel/DATABASE/CoRe_DB_clone/'
    #sim_path    = '/home/agonzalez/Documents/git/watpy/msc/CoRe_DB_clone/'
    rel2_path   = '/data/numrel/DATABASE/Release02/'
    
    data = csv_reader("../data/database.csv")
    P = get_df(data) #ready to go pandas df
    # call database
    cdb = CoRe_db(sim_path)
    idb = cdb.idb

    dict_list =[]
    all_erad_max = []
    for i_dbkey, entry in enumerate(P['name']):
        print("Working with ", entry)

        if entry=='BAM:0097' or entry=='BAM:0100': ## Georgios' sims
            dir            = entry.replace(':','_')
            target         = dir.split('_')[0]

            dirc = dir.replace('_',':')
            splitdir = dir.split('_')

            results_dir = './results/'+dir
            if not os.path.exists(results_dir):
                os.makedirs(results_dir)

            if entry in idb.dbkeys:
                sim = cdb.sim[dirc]
                this_path = os.path.join(sim_path,dir) 

                for res in sim.run.keys():
                    results_dir_run = results_dir + '/' + res
                    if not os.path.exists(results_dir_run):
                        os.makedirs(results_dir_run)

                    simrun = sim.run[res]

                    _i = get_info(this_path, dir, target, res, simrun, results_dir_run)
                    this_infodir = _i
                    dict_list.append(this_infodir)

                this_path = os.path.join(rel2_path,dir+"_EFL")
                run_folders = os.listdir(this_path)

                for res2 in run_folders:
                    print(res2)
                    run_path = this_path + '/'  + res2
                    if os.path.isdir(run_path):
                        results_dir_run = results_dir + '/' + res2 + "_EFL"
                        if not os.path.exists(results_dir_run):
                            os.makedirs(results_dir_run)
                        h5dat = run_path + '/data.h5'
                        simrun = CoRe_run(run_path)

                        _i = get_info(this_path, dir, target, res2, simrun, results_dir_run)
                        this_infodir = _i
                        dict_list.append(this_infodir)
            
        else:
            dir            = entry.replace(':','_')
            target         = dir.split('_')[0]

            dirc = dir.replace('_',':')
            splitdir = dir.split('_')

            results_dir = './results/'+dir
            if not os.path.exists(results_dir):
                os.makedirs(results_dir)

            if entry in idb.dbkeys: ## CURRENT RELEASE
                #continue
                sim = cdb.sim[dirc]
                this_path = os.path.join(sim_path,dir) # .../CoRe_DB_clone/BAM_0010

                for res in sim.run.keys():
                    results_dir_run = results_dir + '/' + res
                    if not os.path.exists(results_dir_run):
                        os.makedirs(results_dir_run)

                    simrun = sim.run[res]

                    _i = get_info(this_path, dir, target, res, simrun, results_dir_run)
                    this_infodir = _i
                    dict_list.append(this_infodir)

            else:     ## NEW RELEASE NOT YET PUSHED
                #continue
                this_path = os.path.join(rel2_path,dir) # .../Release02/BAM_0010
                metamain = this_path + '/metadata_main.txt'
                dict = read_txt_into_dict(metamain)
                run_folders = os.listdir(this_path)

                for res in run_folders:
                    print(res)
                    run_path = this_path + '/'  + res
                    if os.path.isdir(run_path):
                        results_dir_run = results_dir + '/' + res
                        if not os.path.exists(results_dir_run):
                            os.makedirs(results_dir_run)
                        h5dat = run_path + '/data.h5'
                        simrun = CoRe_run(run_path)

                        _i = get_info(this_path, dir, target, res, simrun, results_dir_run)
                        this_infodir = _i
                        dict_list.append(this_infodir)

    #print(dict_list) 
    print(np.nanmax(all_erad_max))
    #with open('./fits_data.json', 'w') as f:         
    #    json.dump(dict_list, f, indent = 6)    
