BAM_Precession_BNS

BAM_sly_1.350_1.350_0.17_0.17_0.0341_th0
BAM_sly_1.350_1.350_0.17_0.17_0.0341_th135
BAM_sly_1.350_1.350_0.17_0.17_0.0341_th135v2
BAM_sly_1.350_1.350_0.17_0.17_0.0341_th180
BAM_sly_1.350_1.350_0.17_0.17_0.0341_th45
BAM_sly_1.350_1.350_0.17_0.17_0.0341_th90
BAM_sly_1.351_1.351_0.17_0.17_0.0341_th45v2

ALL

drwxrwxr-x 5 max.ujevic            numrel  169 Mar 29 02:36 BAM_H4_1.400_1.372_0.05_0.11_0.0373
drwxrwxr-x 5 max.ujevic            numrel  169 Mar 29 13:55 BAM_H4_1.400_1.372_0.06_0.03_0.0373
drwxrwxr-x 5 max.ujevic            numrel  169 Mar 29 14:42 BAM_H4_1.400_1.372_0.11_0.05_0.0373
drwxrwxr-x 5 max.ujevic            numrel  169 Mar 29 15:06 BAM_H4_1.447_1.372_0.04_0.11_0.0382
drwxr-xr-x 5 ananya.adhikari       numrel  269 Mar 27 18:28 BAM_MPA1_1.655_1.655_0.00_0.00_0.059
drwxr-xr-x 5 ananya.adhikari       numrel  269 Mar 27 18:38 BAM_MPA1_1.744_1.570_0.00_0.00_0.059
drwxr-xr-x 5 ananya.adhikari       numrel  269 Mar 27 18:52 BAM_MPA1_1.852_1.481_0.00_0.00_0.059
drwxr-xr-x 6 alireza.rashti        numrel  259 Mar 23 17:48 BAM_MS1B_1.400_1.370_w00430w00150_0.037
drwxr-xr-x 4 alireza.rashti        numrel  149 Apr  5 18:04 BAM_MS1B_1.400_1.370_w00560w00150_0.037
drwxr-xr-x 5 alireza.rashti        numrel  234 Mar 29 06:39 BAM_MS1B_1.450_1.150_w00150w00560_0.034
drwxr-xr-x 5 alireza.rashti        numrel  204 Mar 29 06:40 BAM_MS1B_1.450_1.370_w00150w00430_0.038
drwxrwxr-x 9 tim.dietrich          numrel 4096 Apr  8 10:55 BAM_PRECESSION_BNS
drwxr-xr-x 7 max.ujevic            numrel  280 Mar 26 21:59 BAM_SLY_1.500_1.200_0.00_0.00_0.0322
drwxrwxr-x 7 max.ujevic            numrel  280 Mar 28 20:04 BAM_SLY_1.620_1.080_0.00_0.00_0.0322
drwxrwxr-x 7 max.ujevic            numrel  280 Mar 28 20:25 BAM_SLY_1.718_0.982_0.00_0.00_0.0322
drwxrwxr-x 7 max.ujevic            numrel  280 Mar 29 02:19 BAM_SLY_1.800_0.900_0.00_0.00_0.0322
drwxrwxr-x 5 surendra.padamata     numrel  222 Mar 24 22:24 THC_BHBlp_1.364_1.364_0.00_0.00_0.048_M0_LK
drwxrwxr-x 3 surendra.padamata     numrel  100 Mar 21 01:05 THC_BHBlp_1.493_1.248_0.00_0.00_0.048_M0_LK
drwxrwsr-x 4 surendra.padamata     numrel  151 Mar 23 23:28 THC_BLh_1.300_1.300_0.00_0.00_0.053_M0
drwxrwsr-x 4 surendra.padamata     numrel  151 Mar 21 04:13 THC_BLh_1.333_1.333_0.00_0.00_0.054_M0
drwxrwxr-x 5 surendra.padamata     numrel  207 Mar 21 04:14 THC_BLh_1.364_1.364_0.00_0.00_0.048_M0
drwxrwxr-x 5 surendra.padamata     numrel  216 Mar 21 01:05 THC_BLh_1.364_1.364_0.00_0.00_0.048_M0_LK
drwxrwsr-x 3 surendra.padamata     numrel   95 Mar 21 04:15 THC_BLh_1.400_1.200_0.00_0.00_0.045_M0
drwxrwsr-x 4 surendra.padamata     numrel  151 Mar 21 04:15 THC_BLh_1.400_1.400_0.00_0.00_0.058_M0
drwxrwxr-x 4 alessandro.camilletti numrel  151 Mar 20 12:40 THC_BLh_1.437_1.914_0.00_0.00_0.063_M0
drwxrwsr-x 4 surendra.padamata     numrel  151 Mar 21 04:16 THC_BLh_1.450_1.450_0.00_0.00_0.061_M0
drwxrwsr-x 4 surendra.padamata     numrel  151 Mar 21 04:16 THC_BLh_1.475_1.475_0.00_0.00_0.063_M0
drwxrwsr-x 4 surendra.padamata     numrel  151 Mar 21 04:17 THC_BLh_1.482_1.259_0.00_0.00_0.048_M0
drwxrwxr-x 3 surendra.padamata     numrel   98 Mar 21 01:05 THC_BLh_1.482_1.259_0.00_0.00_0.048_M0_LK
drwxrwsr-x 4 surendra.padamata     numrel  151 Mar 21 04:17 THC_BLh_1.500_1.500_0.00_0.00_0.064_M0
drwxrwxr-x 4 alessandro.camilletti numrel  151 Mar 20 11:04 THC_BLh_1.527_1.795_0.00_0.00_0.063_M0
drwxrwxr-x 4 alessandro.camilletti numrel  151 Mar 20 11:04 THC_BLh_1.557_1.750_0.00_0.00_0.062_M0
drwxrwxr-x 5 surendra.padamata     numrel  216 Mar 21 01:05 THC_BLh_1.581_1.118_0.00_0.00_0.047_M0_LK
drwxrwxr-x 5 surendra.padamata     numrel  207 Mar 21 01:05 THC_BLh_1.581_1.184_0.00_0.00_0.049_M0
drwxrwsr-x 4 surendra.padamata     numrel  151 Mar 21 04:18 THC_BLh_1.600_1.600_0.00_0.00_0.070_M0
drwxrwxr-x 4 surendra.padamata     numrel  157 Mar 21 01:05 THC_BLh_1.635_1.146_0.00_0.00_0.049_M0_LK
drwxrwxr-x 4 alessandro.camilletti numrel  151 Mar 20 11:04 THC_BLh_1.654_1.654_0.00_0.00_0.062_M0
drwxrwxr-x 5 surendra.padamata     numrel  207 Mar 21 01:05 THC_BLh_1.699_1.104_0.00_0.00_0.050_M0
drwxrwxr-x 3 surendra.padamata     numrel   98 Mar 21 01:05 THC_BLh_1.699_1.104_0.00_0.00_0.050_M0_LK
drwxrwxr-x 5 surendra.padamata     numrel  216 Mar 21 01:05 THC_BLh_1.772_1.065_0.00_0.00_0.050_M0_LK
drwxrwxr-x 5 surendra.padamata     numrel  207 Mar 21 01:05 THC_BLh_1.856_1.020_0.00_0.00_0.051_M0
drwxrwxr-x 5 surendra.padamata     numrel  216 Mar 21 01:05 THC_BLh_1.856_1.020_0.00_0.00_0.051_M0_LK
drwxrwsr-x 4 surendra.padamata     numrel  151 Mar 29 01:38 THC_BLQ_1.300_1.300_0.00_0.00_0.053_M0
drwxrwsr-x 4 surendra.padamata     numrel  151 Mar 29 01:45 THC_BLQ_1.333_1.333_0.00_0.00_0.054_M0
drwxrwsr-x 4 surendra.padamata     numrel  151 Mar 29 01:24 THC_BLQ_1.364_1.364_0.00_0.00_0.048_M0
drwxrwsr-x 4 surendra.padamata     numrel  151 Mar 29 01:25 THC_BLQ_1.400_1.400_0.00_0.00_0.058_M0
drwxrwsr-x 4 surendra.padamata     numrel  151 Mar 29 01:25 THC_BLQ_1.450_1.450_0.00_0.00_0.061_M0
drwxrwsr-x 4 surendra.padamata     numrel  151 Mar 29 01:25 THC_BLQ_1.475_1.475_0.00_0.00_0.063_M0
drwxrwsr-x 4 surendra.padamata     numrel  151 Mar 29 01:26 THC_BLQ_1.482_1.259_0.00_0.00_0.048_M0
drwxrwsr-x 4 surendra.padamata     numrel  151 Mar 29 01:26 THC_BLQ_1.500_1.500_0.00_0.00_0.064_M0
drwxrwsr-x 4 surendra.padamata     numrel  151 Mar 29 01:26 THC_BLQ_1.600_1.600_0.00_0.00_0.070_M0
drwxrwsr-x 4 surendra.padamata     numrel  151 Mar 29 01:27 THC_BLQ_1.856_1.020_0.00_0.00_0.051_M0
drwxrwxr-x 4 alessandro.camilletti numrel  151 Mar 20 11:04 THC_DD2_1.289_2.149_0.00_0.00_0.066_M0
drwxrwxr-x 5 surendra.padamata     numrel  207 Mar 21 01:05 THC_DD2_1.364_1.364_0.00_0.00_0.048_M0
drwxrwxr-x 5 surendra.padamata     numrel  216 Mar 21 01:05 THC_DD2_1.364_1.364_0.00_0.00_0.048_M0_LK
drwxrwxr-x 3 surendra.padamata     numrel   95 Mar 21 01:05 THC_DD2_1.432_1.300_0.00_0.00_0.048_M0
drwxrwxr-x 3 surendra.padamata     numrel   95 Mar 21 01:05 THC_DD2_1.435_1.298_0.00_0.00_0.048_M0
drwxrwxr-x 4 alessandro.camilletti numrel  151 Mar 20 11:04 THC_DD2_1.437_1.914_0.00_0.00_0.063_M0
drwxrwxr-x 4 surendra.padamata     numrel  151 Mar 21 01:05 THC_DD2_1.486_1.254_0.00_0.00_0.024_M0
drwxrwxr-x 5 surendra.padamata     numrel  207 Mar 21 01:07 THC_DD2_1.497_1.245_0.00_0.00_0.024_M0
drwxrwxr-x 5 surendra.padamata     numrel  216 Mar 21 01:07 THC_DD2_1.509_1.235_0.00_0.00_0.024_M0_LK
drwxrwxr-x 4 alessandro.camilletti numrel  151 Mar 20 11:04 THC_DD2_1.527_1.795_0.00_0.00_0.063_M0
drwxrwxr-x 4 surendra.padamata     numrel  157 Mar 21 01:07 THC_DD2_1.635_1.146_0.00_0.00_0.025_M0_LK
drwxrwxr-x 4 alessandro.camilletti numrel  151 Mar 20 11:04 THC_DD2_1.654_1.654_0.00_0.00_0.062_M0
drwxrwxr-x 3 surendra.padamata     numrel   94 Mar 21 01:07 THC_LS220_1.364_1.364_0.00_0.00_0.048
drwxrwxr-x 3 surendra.padamata     numrel   97 Mar 21 01:07 THC_LS220_1.364_1.364_0.00_0.00_0.048_LK
drwxrwxr-x 4 surendra.padamata     numrel  161 Mar 21 01:07 THC_LS220_1.364_1.364_0.00_0.00_0.048_M0_LK
drwxrwxr-x 4 surendra.padamata     numrel  155 Mar 21 01:07 THC_LS220_1.400_1.330_0.00_0.00_0.024_M0
drwxrwxr-x 3 surendra.padamata     numrel  100 Mar 21 01:07 THC_LS220_1.635_1.114_0.00_0.00_0.048_M0_LK
drwxrwxr-x 4 surendra.padamata     numrel  155 Mar 21 01:07 THC_LS220_1.772_1.065_0.00_0.00_0.050_M0
drwxrwxr-x 4 surendra.padamata     numrel  161 Mar 21 01:07 THC_LS220_1.772_1.065_0.00_0.00_0.050_M0_LK
drwxrwxr-x 4 surendra.padamata     numrel  159 Mar 21 01:07 THC_SFHo_1.065_1.772_0.00_0.00_0.050_M0_LK
drwxrwxr-x 3 surendra.padamata     numrel   99 Mar 21 01:07 THC_SFHo_1.146_1.635_0.00_0.00_0.049_M0_LK
drwxrwxr-x 5 surendra.padamata     numrel  210 Mar 21 01:07 THC_SFHo_1.364_1.364_0.00_0.00_0.048_M0
drwxrwxr-x 5 surendra.padamata     numrel  219 Mar 21 01:07 THC_SFHo_1.364_1.364_0.00_0.00_0.048_M0_LK
drwxrwxr-x 4 alessandro.camilletti numrel  153 Mar 20 11:04 THC_SFHo_1.437_1.914_0.00_0.00_0.063_M0
drwxrwxr-x 5 surendra.padamata     numrel  210 Mar 21 01:07 THC_SFHo_1.452_1.283_0.00_0.00_0.024_M0
drwxrwxr-x 5 surendra.padamata     numrel  219 Mar 21 01:07 THC_SFHo_1.452_1.283_0.00_0.00_0.024_M0_LK
drwxrwxr-x 4 alessandro.camilletti numrel  153 Mar 20 11:04 THC_SFHo_1.527_1.795_0.00_0.00_0.063_M0
drwxrwxr-x 3 surendra.padamata     numrel   99 Mar 21 01:07 THC_SFHo_1.635_1.146_0.00_0.00_0.049_M0_LK
drwxrwxr-x 4 alessandro.camilletti numrel  153 Mar 20 11:04 THC_SFHo_1.654_1.654_0.00_0.00_0.062_M0
drwxrwxr-x 4 alessandro.camilletti numrel  151 Mar 20 11:04 THC_SLy_1.437_1.914_0.00_0.00_0.063_M0
drwxrwxr-x 4 alessandro.camilletti numrel  151 Mar 20 11:04 THC_SLy_1.527_1.795_0.00_0.00_0.063_M0
drwxrwxr-x 4 alessandro.camilletti numrel  151 Mar 20 11:04 THC_SLy_1.654_1.654_0.00_0.00_0.062_M0
drwxrwxr-x 4 surendra.padamata     numrel  153 Mar 21 01:07 THC_SLy4_1.364_1.364_0.00_0.00_0.024_M0
drwxrwxr-x 4 surendra.padamata     numrel  159 Mar 21 01:07 THC_SLy4_1.364_1.364_0.00_0.00_0.048_M0_LK
drwxrwxr-x 4 surendra.padamata     numrel  153 Mar 21 01:07 THC_SLy4_1.452_1.283_0.00_0.00_0.048_M0
drwxrwxr-x 3 surendra.padamata     numrel   99 Mar 21 01:07 THC_SLy4_1.635_1.146_0.00_0.00_0.049_M0_LK
drwxrwxr-x 4 surendra.padamata     numrel  159 Mar 21 01:07 THC_SLy4_1.772_1.065_0.00_0.00_0.050_M0_LK
drwxrwxr-x 3 surendra.padamata     numrel   99 Mar 21 01:07 THC_SLy4_1.856_1.020_0.00_0.00_0.051_M0_LK
