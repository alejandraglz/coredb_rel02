 ## Writes CoRe DB in a csv file
from watpy.coredb.coredb import *
import os # mkdirs
import csv
import natsort

db_path = '/data/numrel/DATABASE/CoRe_DB_clone/'
#os.makedirs(db_path, exist_ok=True)

cdb = CoRe_db(db_path)
idb = cdb.idb

csv_header = []
for i in idb.index:
    for k, v in i.data.items():
        if k=="database_key" or k=="simulation_name" or k.startswith("id_"):
            csv_header.append(k)
    break

dict_list = []
####
#  ADD BAM FIRST
####
bam_md = [i for i in idb.index if i.data['database_key'].startswith("BAM")]

pop_keys = []
for i in bam_md:
    for k, v in i.data.items():
        if k in csv_header:
            continue
        else:
            pop_keys.append(k)
    for pk in pop_keys:
        try:
            i.data.pop(pk)
        except:
            continue
    dict_list.append(i.data)

## Add new release data
base = '/data/numrel/DATABASE/Release02/'
sim_folders = natsort.natsorted(os.listdir(base))

pop_keys = []
for sim_dir in sim_folders:
    if sim_dir=="Sim" or sim_dir=="README" or sim_dir.startswith("THC_") or sim_dir=="BAM_0097_EFL" or sim_dir=="BAM_0100_EFL":
        continue
    else:
        metamain = base + sim_dir + "/metadata_main.txt"
        dict = read_txt_into_dict(metamain)
        for k, v in dict.items():
            if k in csv_header:
                continue
            else:
                pop_keys.append(k)
        for pk in pop_keys:
            try:
                dict.pop(pk)
            except:
                continue
        dict_list.append(dict)


####
#  NOW THC
####
thc_md = [i for i in idb.index if i.data['database_key'].startswith("THC")]

pop_keys = []
for i in thc_md:
    for k, v in i.data.items():
        if k in csv_header:
            continue
        else:
            pop_keys.append(k)
    for pk in pop_keys:
        try:
            i.data.pop(pk)
        except:
            continue
    dict_list.append(i.data)

## Add new release data
pop_keys = []
for sim_dir in sim_folders:
    if sim_dir=="Sim" or sim_dir=="README" or sim_dir.startswith("BAM_"):
        continue
    else:
        print(sim_dir)
        metamain = base + sim_dir + "/metadata_main.txt"
        dict = read_txt_into_dict(metamain)
        for k, v in dict.items():
            if k in csv_header:
                continue
            else:
                pop_keys.append(k)
        for pk in pop_keys:
            try:
                dict.pop(pk)
            except:
                continue
        dict_list.append(dict)


#print(dict_list)
## Write everything in csv file
with open('database.csv','w') as file:
    writer = csv.DictWriter(file,fieldnames=csv_header)
    writer.writeheader()
    writer.writerows(dict_list)
