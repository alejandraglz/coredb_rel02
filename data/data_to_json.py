import os
import numpy as np
import matplotlib.pyplot as plt
from scipy.ndimage import gaussian_filter1d
from scipy.signal import savgol_filter

from bajes import MTSUN_SI
from bajes.obs.gw.strain import fft, bandpassing
from bajes.pipe import ensure_dir
from bajes.obs.gw.utils import compute_lambda_tilde, compute_chi_eff


from watpy.coredb.coredb import *
from watpy.utils.coreh5 import *
from watpy.utils.ioutils import *

import sys
sys.path.append('../')
import csv
from handy_functions import *

def build_json(this_path, dir, target, res, simrun, results_dir_run):
    meta_dict = simrun.md.data
    
    simname = meta_dict['simulation_name']
        
    this_info = {}
    this_info['database_key'] = meta_dict['database_key']
    this_info['sim_name'] = meta_dict['simulation_name']
    this_info['eos']    = splitdir[0]
    this_info['code']   = target
    this_info['resolution']   = float(meta_dict['grid_spacing_min'])
    this_info['mtot']   = float(meta_dict['id_mass'])
    this_info['q']      = float(meta_dict['id_mass_ratio'])
    this_info['mchirp'] = this_info['mtot'] * (this_info['q']/(1.+this_info['q'])**2.)**0.6
    s1  = meta_dict['id_spin_starA'].split(',')[2]
    s2  = meta_dict['id_spin_starB'].split(',')[2]
    this_info['m1']         = float(meta_dict['id_mass_starA'])
    this_info['m2']         = float(meta_dict['id_mass_starB'])
    this_info['s1z']        = float(s1)
    this_info['s2z']        = float(s2)
    this_info['chi_eff']    = compute_chi_eff(this_info['m1'],this_info['m2'],this_info['s1z'],this_info['s2z'])
    this_info['m1_rest']    = float(meta_dict['id_rest_mass_starA'])
    this_info['m2_rest']    = float(meta_dict['id_rest_mass_starB'])
    this_info['lambda1']    = float(meta_dict['id_Lambdaell_starA'].split(',')[0])
    this_info['lambda2']    = float(meta_dict['id_Lambdaell_starB'].split(',')[0])
    this_info['m_adm']      = float(meta_dict['id_ADM_mass'])
    this_info['j_adm']      = float(meta_dict['id_ADM_angularmomentum'])
    this_info['omega0']     = float(meta_dict['id_gw_frequency_Momega22'])
    this_info['subgrid_model']     = meta_dict['subgrid_model']
    this_info['neutrino_scheme']     = meta_dict['neutrino_scheme']
    try:
        this_info['number_of_orbits']     = float(meta_dict['number_of_orbits'])
    except ValueError:
        this_info['number_of_orbits'] = None
    this_info['hydro_reconstruction']     = meta_dict['hydro_reconstruction']

    return this_info

if __name__ == "__main__":

    targets     = ['THC', 'BAM']
    sim_path    = '/data/numrel/DATABASE/CoRe_DB_clone/'
    #sim_path    = '/home/agonzalez/Documents/git/watpy/msc/CoRe_DB_clone/'
    rel2_path   = '/data/numrel/DATABASE/Release02/'
    
    data = csv_reader("../data/database.csv")
    P = get_df(data) #ready to go pandas df
    # call database
    cdb = CoRe_db(sim_path)
    idb = cdb.idb

    dict_list =[]
    
    for i_dbkey, entry in enumerate(P['name']):
        print("Working with ", entry)

        if entry=='BAM:0097' or entry=='BAM:0100': ## Georgios' sims
            dir            = entry.replace(':','_')
            target         = dir.split('_')[0]

            dirc = dir.replace('_',':')
            splitdir = dir.split('_')

            results_dir = './results/'+dir
            if not os.path.exists(results_dir):
                os.makedirs(results_dir)

            if entry in idb.dbkeys:
                sim = cdb.sim[dirc]
                this_path = os.path.join(sim_path,dir) 

                for res in sim.run.keys():
                    results_dir_run = results_dir + '/' + res
                    if not os.path.exists(results_dir_run):
                        os.makedirs(results_dir_run)

                    simrun = sim.run[res]

                    _i = build_json(this_path, dir, target, res, simrun, results_dir_run)
                    this_infodir = _i
                    dict_list.append(this_infodir)

                this_path = os.path.join(rel2_path,dir+"_EFL")
                run_folders = os.listdir(this_path)

                for res2 in run_folders:
                    print(res2)
                    run_path = this_path + '/'  + res2
                    if os.path.isdir(run_path):
                        results_dir_run = results_dir + '/' + res2 + "_EFL"
                        if not os.path.exists(results_dir_run):
                            os.makedirs(results_dir_run)
                        h5dat = run_path + '/data.h5'
                        simrun = CoRe_run(run_path)

                        _i = build_json(this_path, dir, target, res2, simrun, results_dir_run)
                        this_infodir = _i
                        dict_list.append(this_infodir)
            
        else:
            dir            = entry.replace(':','_')
            target         = dir.split('_')[0]

            dirc = dir.replace('_',':')
            splitdir = dir.split('_')

            results_dir = './results/'+dir
            if not os.path.exists(results_dir):
                os.makedirs(results_dir)

            if entry in idb.dbkeys: ## CURRENT RELEASE
                #continue
                sim = cdb.sim[dirc]
                this_path = os.path.join(sim_path,dir) # .../CoRe_DB_clone/BAM_0010

                for res in sim.run.keys():
                    results_dir_run = results_dir + '/' + res
                    if not os.path.exists(results_dir_run):
                        os.makedirs(results_dir_run)

                    simrun = sim.run[res]

                    _i = build_json(this_path, dir, target, res, simrun, results_dir_run)
                    this_infodir = _i
                    dict_list.append(this_infodir)

            else:     ## NEW RELEASE NOT YET PUSHED
                #continue
                this_path = os.path.join(rel2_path,dir) # .../Release02/BAM_0010
                metamain = this_path + '/metadata_main.txt'
                dict = read_txt_into_dict(metamain)
                run_folders = os.listdir(this_path)

                for res in run_folders:
                    print(res)
                    run_path = this_path + '/'  + res
                    if os.path.isdir(run_path):
                        results_dir_run = results_dir + '/' + res
                        if not os.path.exists(results_dir_run):
                            os.makedirs(results_dir_run)
                        h5dat = run_path + '/data.h5'
                        simrun = CoRe_run(run_path)

                        _i = build_json(this_path, dir, target, res, simrun, results_dir_run)
                        this_infodir = _i
                        dict_list.append(this_infodir)

    #print(dict_list)        
    with open('./data.json', 'w') as f:         
        json.dump(dict_list, f, indent = 6)    
