import os
import numpy as np
from watpy.utils.ioutils import *
import sys
sys.path.append('../')

def to_string(num):
    num = str(num)
    if len(num)==2:
        new_str = '00'+num
    else:
        new_str = '0'+num
    return new_str

old_json = 'fits_results.json'
new_json = 'fits_data.json'

old_dict = read_json_into_dict(old_json)
new_dict = read_json_into_dict(new_json)

remove_sims = ['BAM:0134:R03','THC:0042:R02','THC:0098:R02','THC:0054:R01','THC:0068:R01','THC:0068:R02','THC:0079:R03','THC:0052:R01','THC:0056:R01','THC:0056:R02','THC:0056:R03','THC:0069:R01']
# remove
new_dict = [odict for odict in new_dict if (odict!=None)]
old_dict = [odict for odict in old_dict if (odict!=None)]
old_dict = [odict for odict in old_dict if (odict['database_key'] not in remove_sims)]

## name changing 
for i, old_d in enumerate(old_dict):  
    try:
        if old_d['database_key'].startswith('THC'):
            #print(old_d['database_key'])
            sim_num = int(old_d['database_key'].split(':')[1])
            run = old_d['database_key'].split(':')[2]
            if (sim_num>52) and (sim_num<56): # remove 56 and 52
                sim_num_new = sim_num - 1
                old_d['database_key'] = "THC:"+to_string(sim_num_new)+":"+run
                #print(old_d['database_key'])
            elif (sim_num>56) and (sim_num<69): # remove 69
                sim_num_new = sim_num - 2
                old_d['database_key'] = "THC:"+to_string(sim_num_new)+":"+run
            elif (sim_num>69): 
                sim_num_new = sim_num - 3
                old_d['database_key'] = "THC:"+to_string(sim_num_new)+":"+run   
            
            #print(old_d['database_key']) # corrected
        else:
            #print(old_d['database_key'])
            continue
    except TypeError:
        continue

## correct data
remove_keys = ['a_pm','t_pm','h_pm','delta_f2','df_2']
for i, new_d in enumerate(new_dict):  
    try:
        print(new_d['database_key'], old_dict[i]['database_key'])
        if new_d['database_key'].startswith('BAM') or new_d['database_key'].startswith('THC'):
            try:
                try:
                    new_d.pop('error', None) 
                except:
                    continue

                if new_d['PC'] == old_dict[i]['PC']:
                    if new_d['PC'] == 'True' or new_d['PC'] == 'true' or new_d['PC'] == True:
                        new_d.pop('f_pm', None)
                        for key in remove_keys:
                            new_d.pop(key, None)
                    elif new_d['f_pm'] == old_dict[i]['f_pm']:
                        print("OK")
                    else:
                        new_d['f_pm'] = old_dict[i]['f_pm']

                else: 
                    new_d['PC'] = old_dict[i]['PC']
                    for key in remove_keys:
                        new_d.pop(key, None)

                    if new_d['PC'] == 'False' or new_d['PC'] == 'false' or new_d['PC'] == False:
                        new_d['PC'] = 'False'
                        new_d['f_pm'] = old_dict[i]['f_pm']
                    elif new_d['PC'] == 'True' or new_d['PC'] == 'true' or new_d['PC'] == True:
                        new_d.pop('f_pm', None)
                        new_d['PC'] = 'True'
                        
                    print("New: ",new_d, " \n Old: ",old_dict[i])
            except:
                continue
    except:
        continue


# Compare with Matteo's data



# Write new file
#with open('./final_data.json', 'w') as f:         
#    json.dump(new_dict, f, indent = 6)  